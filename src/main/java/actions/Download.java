package actions;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.io.FileUtils;
import org.gs4tr.qa.util.FileUtil;
import org.gs4tr.qa.utility.BrowserFactory;


public class Download {
	private static Download add_objects;
	
	String SubmissionName = "HYB_2626";
	String filePath = BrowserFactory.SystemEngine().properties_sourceDir;
	String projectPath = "";
	String home = System.getProperty("user.home");
	File path = new File(home+"/Downloads/"); 
	
	public static synchronized Download action(){
		try{
			if(add_objects.equals(null))
			{
				add_objects = new Download();
			}
		}
		catch(Exception NOSYSTEM){
			add_objects = new Download();
		}
		return add_objects;
	}
	
	public boolean  manageZipFile (String path,String FileName, String cData) throws Exception {
		boolean result = false;
	String foldertoUnZip = common.action().getFolderName(path, FileName);
//	FileUtil.deleteDir(projectPath);
	System.out.println("Source------------->:  "+path + "/"+ foldertoUnZip);
	System.out.println("Target------------->:  "+filePath + "/target/");	
	String xmlFilePath = Manage_ZipFile.action().extractFolder(path + "/"+ foldertoUnZip,path +"/target/");
	System.out.println("\n File_UnpackKit()");
	String charData = Manage_ZipFile.action().xmlContent(xmlFilePath);
	Thread.sleep(1000);
	if(charData.equalsIgnoreCase(cData))
	{
		System.out.println(charData + " matches " + cData);
		result = true;
	}
	return result;
}
	
	public void deleteFiles_From_Download() throws Exception {
		projectPath = path.getPath();
		FileUtils.deleteDirectory(new File(projectPath));
	}
	
	public boolean verify_XML_Node_Data(String label1, String label2) throws Exception {
		boolean result = false;
		projectPath = path.getPath();
		Thread.sleep(1000);
		String fileName =common.action().getZipFile_Name();
		Thread.sleep(1000);
		String foldertoUnZip = common.action().getFolderName(projectPath, fileName);
		Thread.sleep(1000);
		System.out.println("Source------------->:  "+path + "/"+ foldertoUnZip);
		String xmlFilePath = Manage_ZipFile.action().extractFolder(projectPath + "/"+ foldertoUnZip,projectPath +"/target/");
		System.out.println("\n File_UnpackKit()");
		Thread.sleep(2000);
		ArrayList<String> str = Manage_ZipFile.action().get_XML_Node_Values(xmlFilePath);
		Thread.sleep(2000);
		if(str.contains(label1) && str.contains(label2))
		{
			System.out.println("Product feature label present in the XML.");
			result = true;
		}
		return result;
}
	
	public boolean verify_XML_Node_Data_Not_Exist(String label1) throws Exception {
		boolean result = false;
		projectPath = path.getPath();
		Thread.sleep(1000);
		String fileName =common.action().getZipFile_Name();
		Thread.sleep(1000);
		String foldertoUnZip = common.action().getFolderName(projectPath, fileName);
		System.out.println("Source------------->:  "+path + "/"+ foldertoUnZip);
		Thread.sleep(1000);
		String xmlFilePath = Manage_ZipFile.action().extractFolder(projectPath + "/"+ foldertoUnZip,projectPath +"/target/");
		System.out.println("\n File_UnpackKit()");
		Thread.sleep(2000);
		ArrayList<String> str = Manage_ZipFile.action().get_XML_Node_Values(xmlFilePath);
		Thread.sleep(2000);
		if(!str.contains(label1))
		{
			System.out.println("Product feature label is not present in XML.");
			result = true;
		}
		return result;
}


	public boolean verify_XML_Data(String data ) throws Exception {
	
		 String fileName =common.action().getZipFile_Name();
			projectPath = path.getPath();
			boolean result = manageZipFile(projectPath, fileName, data);
			return result;
	}
}