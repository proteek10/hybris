package actions;

import java.util.ArrayList;

import org.gs4tr.qa.utility.BrowserFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import locators.Hybris_dashboard;

public class General {

	private static General general;
	String job_ID;

	/**
	 * Method used to self-instantiate the class. Will make sure one object, and
	 * one object only is created in memory for this class. The purpose is to be
	 * able to call this object dynamically from any JAVA class that includes
	 * this as an import.
	 * 
	 * @return Returns the object instantiated from the class.
	 */
	public static synchronized General action() {
		try {
			if (general.equals(null)) {
				general = new General();
			}
		} catch (Exception NOSYSTEM) {
			general = new General();
		}
		return general;
	}

	
	
	/**
	 * Opens the web-site in the browser from the URL address specified in the
	 * tdc_test.properties file.
	 * @throws Exception 
	 * 
	 * 
	 */
	public void openURL() throws Exception {
		String passURL = BrowserFactory.SystemEngine().properties.getProperty("url");
		System.out.println(passURL);
		BrowserFactory.SystemEngine();
		BrowserFactory.driver.get(passURL);
	
	}

	
	/*
	*//**
	 * Verifies an element is present in the current page loaded in the browser.
	 * 
	 * 
	 * @param tagName
	 *            name of element to verify if present.
	 * 
	 * @throws Exception
	 *             used by Thread.sleep, which requires an exception handler.
	 *//*
	public void isPresent(String tagName) throws Exception {
		for (int second = 0;; second++) {
			if (second >= 60)
				fail("timeout");
			try {
				if (BrowserFactory.SystemEngine().findElement(tagName))
					break;
			} catch (Exception e) {
			}
		}
		Thread.sleep(2000);
	}
*/
	
	
	public void mouseOver(String locator) throws Exception{
		Thread.sleep(500);
		BrowserFactory.SystemEngine();
		Actions act=new Actions(BrowserFactory.driver);
		act.moveToElement(BrowserFactory.SystemEngine().findElement(locator));
		act.build().perform();
		Thread.sleep(2000);
		
		System.out.println("INSIDE MOUSEOVER METHOD**********");
		
	}
	
	
public void mouseOver_x(String locator) throws Exception{
		
		Thread.sleep(500);

		Actions actions = new Actions(BrowserFactory.driver);
	    //Retrieve WebElemnt 'slider' to perform mouse hover 
		WebElement element =BrowserFactory.SystemEngine().findElement(locator);
	    //Move mouse to x offset 50 i.e. in horizontal direction
		actions.moveToElement(element,10,0).perform();
		
	}
public void mouseOver_x(String locator,int pixcel) throws Exception{
	
	Thread.sleep(500);

	Actions actions = new Actions(BrowserFactory.driver);
    //Retrieve WebElemnt 'slider' to perform mouse hover 
	WebElement element =BrowserFactory.SystemEngine().findElement(locator);
    //Move mouse to x offset 50 i.e. in horizontal direction
	actions.moveToElement(element,pixcel,0).perform();
	
}
	
	
	
	public void doubleClick(String locator) throws Exception{
		Thread.sleep(500);
		BrowserFactory.SystemEngine();
		Actions act=new Actions(BrowserFactory.driver);
		act.doubleClick(BrowserFactory.SystemEngine().findElement(locator));
		act.build().perform();
		Thread.sleep(2000);
		
	}
	
	public void rightClick(String locator) throws Exception{
		Thread.sleep(500);
		BrowserFactory.SystemEngine();
		Actions act=new Actions(BrowserFactory.driver);
		act.contextClick(BrowserFactory.SystemEngine().findElement(locator));
		act.build().perform();
		Thread.sleep(2000);
		
	}
	
	/**
	 * This method use to scroll down in a page. First user has to click in scrollable area then only this will work.
	 * @param no - pass no of steps to go down
	 * @throws Exception
	 */
	public void scrollDown(int no) throws Exception{
		BrowserFactory.SystemEngine();
		Actions act=new Actions(BrowserFactory.driver);
		for(int i=1;i<=no;i++)
		{
			act.sendKeys(Keys.PAGE_DOWN);
			act.build().perform();
			Thread.sleep(500);
		}	
	}
	
	/**
	 * This method use to scroll up in a page. First user has to click in scrollable area then only this will work.
	 * @param no - pass no of steps to go down
	 * @throws Exception
	 */
	public void scrollUp(int no) throws Exception{
		BrowserFactory.SystemEngine();
		Actions act=new Actions(BrowserFactory.driver);
		for(int i=1;i<=no;i++)
		{
			act.sendKeys(Keys.PAGE_UP);
			act.build().perform();
			Thread.sleep(500);
		}	
	}
	
	
	/**
	 * 
	 *  This method use to scroll down in a page. First user has to click in one first row then only this will work.
	 * 
	 * @author Proteek
	 * 
	 * @param locator
	 * @param no - number to scroll down
	 * @throws Exception
	 */
	public void scrollDown_X(String locator,int no) throws Exception{
		
		BrowserFactory.SystemEngine();
		((JavascriptExecutor)BrowserFactory.driver).executeScript(
				"arguments[0].scrollIntoView();",BrowserFactory.SystemEngine().findElement(locator));
		Thread.sleep(2000);
		
		BrowserFactory.SystemEngine().findElement(locator).click();
		Thread.sleep(2000);
		BrowserFactory.SystemEngine();
		Actions act=new Actions(BrowserFactory.driver);
		for(int i=1;i<=no;i++)
		{
			act.sendKeys(Keys.PAGE_DOWN);
			act.build().perform();
			Thread.sleep(2000);
		}	
		Thread.sleep(2000);
	}
	
	/**
	 * JavaScript Click method
	 * 
	 * @author Praveen
	 * @param locator
	 */
	public void jsClick(String locator) {
		
		BrowserFactory.SystemEngine();
		((JavascriptExecutor)BrowserFactory.driver).executeScript(
				"arguments[0].click();",BrowserFactory.SystemEngine().findElement(locator));
	}
	
	/**
	 * JavaScript Scroll method
	 * 
	 * @author Praveen
	 * @param locator
	 */
	public void jsScroll(String locator) {
		
		BrowserFactory.SystemEngine();
		((JavascriptExecutor)BrowserFactory.driver).executeScript(
				"arguments[0].scrollIntoView();",BrowserFactory.SystemEngine().findElement(locator));
	}
	
	/**
	 * This method is used to click on split button(Eg: Buttons having arrows for drop-down)
	 * 
	 * @author Proteek
	 * @param xOffset
	 * @throws Exception
	 */
	public void splitButtonClick(WebElement locator,int xOffset) throws Exception{
		Thread.sleep(500);
		BrowserFactory.SystemEngine();
		Actions act=new Actions(BrowserFactory.driver);
		act.moveToElement(locator, xOffset, 0);
		act.click().build().perform();
		Thread.sleep(2000);
		
	}
	
	public Integer getXpathCount(String locator) throws Exception{
		int count;
		BrowserFactory.SystemEngine();
		count=BrowserFactory.driver.findElements(By.xpath(locator)).size();
		return count;
		
	}

	/*	
		
		Login to application
		
	*/	
	
	public void login() {
		try {
			
			if(Verify.action().verifyElementPresent(Hybris_dashboard.exts().adavancedbutton, 5))
			{
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().adavancedbutton).click();
				Thread.sleep(2000);
			}
			
			if(Verify.action().verifyElementPresent(Hybris_dashboard.exts().proceed_button, 5))
			{
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().proceed_button).click();
				Thread.sleep(2000);
			}
			
			Thread.sleep(3000);
			Verify.action().verifyElementPresent(Hybris_dashboard.exts().Login_email, 5);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Login_email).sendKeys(Hybris_Common_Properties_Cred.username);
			Thread.sleep(1000);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Login_password).sendKeys(Hybris_Common_Properties_Cred.password);
			Thread.sleep(1000);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Login_button).click();
			Thread.sleep(6000);
			
			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
	}
	
	
	//Select
	/**
	 * This method is used to to select the mulitple value
	 * 
	 * @author Amarata M
	 * @param listItems - list of select tag element
	 * @throws Exception
	 */
	public static void selectMulti(String[] listItems,String Tagname)
	{
	    Select select = new Select(BrowserFactory.SystemEngine().findElement(Tagname));//GlobalLink_WizardSetting_Existing_Content_Type

	    if (listItems.length > 1 && !select.isMultiple())
	    {
	       System.out.println("The SELECT is not a multiple select but the list provided is larger than 1.");
	        return;
	    }

	    for (String listItem : listItems)
	    {
	        try
	        {
	            select.selectByVisibleText(listItem);
	           
	        }
	        catch (Exception e)
	        {
	        	System.out.println(listItem + " was not an available option");
	            e.printStackTrace();
	            return;
	        }
	    }
	}


	public static void selectMultiLocal(String[] listItems,String Tagname)
	{
	    Select select = new Select(BrowserFactory.SystemEngine().findElement(Tagname));

	    if (listItems.length > 1 && !select.isMultiple())
	    {
	       System.out.println("The SELECT is not a  select but the list provided is larger than 1.");
	        return;
	    }

	    for (String listItem : listItems)
	    {
	        try
	        {
	            select.selectByVisibleText(listItem);
	           
	        }
	        catch (Exception e)
	        {
	        	System.out.println(listItem + " was not an available option");
	            e.printStackTrace();
	            return;
	        }
	    }
	}
	//DeSelect
	/**
	 * This method is used to to select the mulitple value
	 * 
	 * @author Amarata M
	 * @param listItems - list of select tag element
	 * @throws Exception
	 */
	public static void deSelectMulti(String[] listItems,String Tagname)
	{
	    Select select = new Select(BrowserFactory.SystemEngine().findElement(Tagname));//locators.exts().GlobalLink_WizardSetting_Existing_Content_Type)

	    if (listItems.length > 1 && !select.isMultiple())
	    {
	       System.out.println("The SELECT is not a multiple select but the list provided is larger than 1.");
	        return;
	    }

	    for (String listItem : listItems)
	    {
	        try
	        {
	            select.deselectByVisibleText(listItem);
	           
	        }
	        catch (Exception e)
	        {
	        	System.out.println(listItem + " was not an available option");
	            e.printStackTrace();
	            return;
	        }
	    }
	}
	
	
	/**
	 * @author Proteek
	 * 
	 * This method is to switch to Pop-window
	 * 
	 * @param windowIndex
	 * @throws Exception
	 */
	public void switchToPopupWindow(int windowIndex) throws Exception{
		System.out.println("In switchToPopupWindow()");
		BrowserFactory.SystemEngine();
		int noOfWindows=BrowserFactory.driver.getWindowHandles().toArray().length;
		System.out.println("No of Windows - "+noOfWindows);
		for(int i=0;i<noOfWindows;i++)
		{
			BrowserFactory.SystemEngine();
			System.out.println("Printing window handles - "+BrowserFactory.driver.getWindowHandles().toArray()[i]);
		}
		
		BrowserFactory.SystemEngine();
		BrowserFactory.SystemEngine();
		BrowserFactory.driver.switchTo().window(BrowserFactory.driver.getWindowHandles().toArray()[windowIndex].toString());
		Thread.sleep(2000);
		  System.out.println("EOM - switchToPopupWindow().");
	}
	
	public void createEmployee(String user_title, String employees_title, String language, String currency, 
		  String groupname1, String groupname_1, ArrayList<String> list) throws Exception {
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(user_title)).click();
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(employees_title)).click();
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_user_emp_newEmp).click();
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().create_new_employee_general("ID")).sendKeys(Hybris_Common_Properties_Cred.autouserid);
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().create_new_employee_general("Name")).sendKeys(Hybris_Common_Properties_Cred.autousername);
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().create_user_nextbutton).click();
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().create_new_employee_locale_information_values("Standard Language:")).sendKeys(language);
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().create_new_employee_locale_information_selectLanguage(language)).click();
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().create_new_employee_locale_information_values("Standard Currency:")).sendKeys(currency);
		Thread.sleep(2000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().create_new_employee_locale_information_selectCurrency(currency)).click();
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().create_user_nextbutton).click();
		Thread.sleep(2000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().create_user_selectGroup_CountryInput).sendKeys(groupname1);
		Thread.sleep(2000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().create_new_employee_locale_information_selectGroup(groupname_1)).click();
		Thread.sleep(1000);
		for(int i=0; i<list.size();i++)
		{
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().create_user_selectGroup_CountryInput).sendKeys(list.get(i));
			Thread.sleep(2000);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().create_new_employee_locale_information_selectGroup(list.get(i))).click();
			Thread.sleep(1000);
		}
	  	BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().create_user_donebutton).click();
		Thread.sleep(5000);
	}
	
	public void setPasswordForNewEmployee(String userID, String Password) throws Exception {
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().search_input).sendKeys(userID);
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().search_Button).click();
		Thread.sleep(2000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().searched_Result(userID)).click();
		Thread.sleep(2000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().title_Password).click();
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().new_Password).sendKeys(Password);
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().confirm_Password).sendKeys(Password);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().search_input).click();
		Thread.sleep(500);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().save_Password).click();
			
		}
	
	public void createPage(String title, String subtitle) throws Exception {
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(title)).click();
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(subtitle)).click();
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_user_emp_newEmp).click();
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(Hybris_Common_Properties_Cred.newPage_PageType)).click();
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().New_Page_Approval_Status_DropDown_Btn).click();
		Thread.sleep(500);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().searched_Result(Hybris_Common_Properties_Cred.newPage_ApprovalStatus)).click();
		Thread.sleep(500);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().New_page_Catalog_Version_InputField).click();
		Thread.sleep(500);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().New_page_Catalog_Version_InputField).sendKeys(Hybris_Common_Properties_Cred.Page_ApparelUK_Content_Cat_Staged);
		Thread.sleep(500);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_Attribute_Value(Hybris_Common_Properties_Cred.Page_ApparelUK_Content_Cat_Staged)).click();
		Thread.sleep(500);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().New_Page_pageTemplate_InputField).click();
		Thread.sleep(500);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().New_Page_pageTemplate_InputField).sendKeys("landing");
		Thread.sleep(500);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_Attribute_Value(Hybris_Common_Properties_Cred.newPage_PageTemplate_LandingPage2)).click();
		Thread.sleep(500);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().New_Page_ID_InputField).sendKeys(Hybris_Common_Properties_Cred.newPage_ID);
		Thread.sleep(500);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().done_Button).click();
		Thread.sleep(8000);
	}
	
	public void createItem(String title, String subtitle, String number, String version, String status) throws Exception {
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(title)).click();
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(subtitle)).click();
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_user_emp_newEmp).click();
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().createNewProduct_ArticleNumber_Input).sendKeys(number);
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().createNewProduct_Approval_DropDown_Arrow).click();
		Thread.sleep(500);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().searched_Result(status)).click();
		Thread.sleep(500);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().createNewProduct_CatalogVersion).click();
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().createNewProduct_CatalogVersion).sendKeys(version);
		Thread.sleep(2000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_Attribute_Value(version)).click();
		Thread.sleep(500);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().create_user_nextbutton).click();
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().createNewProduct_Identifier_Input).sendKeys("This product is created via Automation");
		Thread.sleep(500);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().done_Button).click();
		Thread.sleep(8000);
	}
	

 }

