package actions;

public class GlobalProductCockpit_Common_Prop_Cred {
	
	//TODO BELOW DETAILS SHOULD BE REQUIRED AT EVERY NEW BUILD.
	
	//Global Product Cockpit Details
		public final static String gpc_URL = "https://10.10.222.59:9002/globalproductcockpit";
		public final static String gpc_UserName = "productmanager";
		public final static String gpc_Password = "1234";
		
		
	//Global Product CMS Cockpit Details
			public final static String gpc_cms_URL = "https://10.10.222.59:9002/globalcmscockpit";
			public final static String gpc_cms_UserName = "cmsmanager";
			public final static String gpc_cms_Password = "1234";
		
	//Target Language Name
		public final static String German = "German";
		public final static String French = "French";
		public final static String selectALL = "Select All";
		
	//Create Submission Page Buttons
		public final static String translate_Btn = "Translate";
		public final static String close_Btn = "Close";	
		public final static String next_Btn = "Next";
		
	//Menu's
		public final static String menu_Product = "Product";
		public final static String menu_Catalog = "Catalog";
		public final static String menu_GlobalLink = "GlobalLink";
		public final static String menu_Navigation = "Navigation";
		
	//Product Names
		public final static String product_DC_Car_Battery_Adaptor = "DC Car Battery Adapter";
		public final static String product_Helmet = "Helmet Women TSG Lotus Graphic Designs wms";
		
	//Page names
		public final static String page_HomePage = "Homepage";
		public final static String page_HomePage_ItemName = "homepage";
	
	//Catalog Products
		public final static String apparel_Prod_Catalog_Staged = "Apparel Product Catalog Staged";
		
	//Select Project Name for SUbmission 
		public final static String ProjectName_Hybris = "Hybris";
		public final static String ProjectName_Inriver = "Inriver";
		
	//Select Profile
		public final static String profileName_Default = "Default";
		
	//Job Status
		public final static String Job_Status_inProgress = "In Progress";
		public final static String Job_Status_Complete = "Complete";
		public final static String Job_Status_Queued = "Queued";
		
	//Advanced Searche Fields
		public final static String id = "ID";
		public final static String name = "Name";
		public final static String timeCreated = "Time created";
		
	//Job Config Fields
		public final static String configFld_ID = "ID: ";
		public final static String configFld_DueDate = "Due Date: ";
		
		
	//CMS Menu's
		public final static String cms_menu_Product = "Product";
		public final static String cms_menu_Catalog_Staged = "Electronics Content Catalog / Staged";
		public final static String cms_menu_Apparel_UK_Catalog_Staged = "Apparel UK Content Catalog / Staged";
		public final static String csm_menu_WCMS = "WCMS Page View";
		public final static String csm_Electronic_Site = "Electronics Site";

		public final static String csm_Apparel_Site_UK = "Apparel Site UK";
		public final static String cms_Apparel_UK_Content_Catalog_Staged = "Apparel UK Content Catalog / Staged";
		
	//Product Names
		public final static String product_cms_Terms_Conditions = "Terms and Conditions Page";	
}
