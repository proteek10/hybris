package actions;

public class Hybris_Common_Properties_Cred {

	//TODO BELOW DETAILS SHOULD BE REQUIRED AT EVERY NEW BUILD.
	public  final static String  username ="admin";   
	public  final static String  password ="nimda";   
	
	public final static String autouserid = "autoadmin";
	public final static String autousername = "admin";
	public final static String autouserpwd = "nimda";
	
	
	//Select Project Name for SUbmission 
	public final static String ProjectName_Hybris = "Hybris";
	public final static String ProjectName_Inriver = "Inriver";
	
	//Select Profile
	public final static String profileName_Default = "Default";
	
	//Target Languages 
	public final static String TargetLanguage_German ="German";
	public final static String TargetLanguage_French ="French";
	
	//Mapped Languages
	public final static String english = "English - en-US";
	public final static String german = "German - de-DE";
	public final static String french = "French - fr-FR";
	
	//Notification Recipients
	public final static String recipient = "demo123@gmail.com";
	
	//Source & Target Languages
	public final static String SourceLang_English = "English [en]";
	public final static String TargetLang_German = "German [de]";
	public final static String multi_Target_Lang = "German [de], French [fr]";
	
	//Job Items > Targets Status
	public final static String TargetStatus_German_InProgress = "German [IN_PROGRESS]";
	public final static String TargetStatus_German_Complete = "German [COMPLETE]";
	
	//Job Items
	public final static String Job_Items_Category_Caps = "Caps";
	public final static String Job_Items_Item_Caps = "Caps [caps] - Apparel Product Catalog : Staged";
	
	
	//Dashboard: Title and Subtitle names
	public final static String Title_wcms = "WCMS";
	public final static String Subtitle_page = "Page";
	public final static String Subtitle_navigation_node = "Navigation Node";
	public final static String Title_catalog = "Catalog";
	public final static String Subtitle_products = "Products";
	public final static String Title_globalLink = "GlobalLink";
	public final static String subtitle_jobs = "Jobs";
	public final static String subtitle_categories = "Categories";
	public final static String subtitle_SubmitQuedJobs = "Submit Queued Jobs";
	public final static String subtitle_FetchTranslations = "Fetch Translations";
	public final static String subtitle_Job_Items = "Job Items"; 
	public final static String subtitle_Change_Detection = "Change Detection";
	public final static String subtitle_Config = "Configuration";
	public final static String Job_Status_inProgress = "IN_PROGRESS";
	public final static String Job_Status_Complete = "COMPLETE";
	public final static String Job_Status_Queued = "QUEUED";
	public final static String Job_Status_Warning = "WARNING";
	public final static String subtitle_Jobs_Requestor = "Administrator [admin]";
	
	//Dashborad > Catalog > Products: article number & catalog version
	public final static String Product_Label_Attributes = "Attributes";
	public final static String Product_Label_Attributes_Processor_Model_Value = "DIGIC III";
	public final static String Product_Article_Num_1 = "29532";
	public final static String Product_Catalog_Staged_Version_1 = "Apparel Product Catalog : Staged";
	public final static String electronics_Product_Catalog_Staged = "Electronics Product Catalog : Staged";
	public final static String Product_Article_Num_2 = "3557133";
	public final static String Product_Article_Num_3 = "1382080";
	public final static String Product_Article_Num_4 = "473996";
	public final static String Product_Article_Num_5 = "300051511";
	public final static String Product_Article_Num_6 = "1325806";
	public final static String Product_Identifier_Rotolog = "The Rotolog";
	
	//Dashborad > WCMS > Page: Attribute names and values
	public final static String Page_Attribute_Catalog_Version= "Catalog"; //Catalog Version = Catalog
	public final static String Page_ApparelUK_Content_Cat_Staged = "Apparel UK Content Catalog : Staged";
	public final static String Electronics_Content_Catalog_Staged = "Electronics Content Catalog : Staged";
	public final static String Page_Attribute_PageName = "Name"; //Page Name = Name
	public final static String Page_Attribute_PageID = "Page ID";
	public final static String Page_Attribute_PageName_HomePage = "Homepage";
	public final static String Page_Attribute_PageName_Cameras = "Cameras";
	public final static String Page_Attribute_PageName_Search = "Search Results Page";
	public final static String Page_Attribute_PageName_FAQ = "Frequently Asked Questions FAQ Page";
	public final static String NavigationNode_Page_Attribute_PageName_Cameras = "Cameras Flashes Link";
	public final static String Page_Attribute_PageName_TermsConditions = "Terms and Conditions Page";
	public final static String NavigationNode_Page_Attribute_PageName_Brand = "Brand Links";

	public final static String NavigationNode_Page_Attribute_PageName_ProductGrid = "Product Grid";

	
	
	//Dashborad > WCMS > New Page
	public final static String newPage_PageType = "Content Page";
	public final static String newPage_ApprovalStatus = "approved";
	public final static String newPage_PageTemplate_LandingPage2 = "Landing Page 2 Template";
	public final static String newPage_ID = "Auto_Page_01";

	
	//Dashborad > WCMS > Navigation_Node: Attribute names and values
	public final static String Navigation_Node_Attribute_Catlog = "Catalog";
	public final static String Navigation_Node_App_UK_Content_Cat_Staged = "Apparel UK Content Catalog : Staged";
	public final static String Navigation_Node_Apparel_Product_Catalog_Staged = "Apparel Product Catalog : Staged";
	public final static String Navigation_Node_Electronic_Content_Catalog_Staged = "Electronics Content Catalog : Staged";
	public final static String Navigation_Node_Name = "Name";
	public final static String Navigation_Node_ID = "ID";
	public final static String Navigation_Node_Name_Value = "root";
	public final static String Navigation_Node_Name_Value_DigCamera = "DigitalCamera";

	//Dashborad > Catalog > Categories: Attribute names and values
	public final static String Catalog_categories_Catalog_Version = "Catalog";//Catalog Version = Catalog
	public final static String Catalog_categories_Apparel_Product_Catalog_Staged= "Apparel Product Catalog : Staged";
	public final static String Catalog_categories_Attribute_Identifier = "Identifier";
	public final static String Catalog_categories_Attribute_NameValue_Caps = "Caps";
	public final static String Catalog_Categories_Attribute_Name_ski_Poles = "Ski Poles";

	public final static String Catalog_Categories_Attribute_Name_The_Rotolog = "The Rotolog";

	public final static String Catalog_Categories_Attribute_Chargers = "Chargers";
	public final static String Catalog_categries_Electronics_Product_Catalog_Staged = "Electronics Product Catalog : Staged";
	public final static String Catalog_Categories_Attribute_Binoculars = "Binoculars";
	public final static String Catalog_Categories_Attribute_PowerSupplies = "Power Supplies";
	public final static String Catalog_Categories_Attribute_DigitalCameras = "Digital Cameras";
		
	//Dashborad > Catalog > Products: Attribute names and values
	public final static String Catalog_products_Catalog_Version = "Catalog";//Catalog Version = Catalog
	public final static String Catalog_products_Apparel_Product_Catalog_Staged= "Apparel Product Catalog : Staged";
	public final static String Catalog_products_Attribute_Article_Number = "Article";
	public final static String Catalog_products_Attribute_Article_Number_Value = "";
	
	//Dashboard > Administration Cockpit 
	public final static String subtitle_Administration_Cockpit = "Administration Cockpit";
	public final static String subtitle_adaptive_Search = "Adaptive Search";
	public final static String subtitle_Product_Cockpit = "Product Cockpit";
	public final static String subtitle_Integration_UI_Tool = "Integration UI Tool";
	
	//Dashborad > GlobalLink > GL_Config_Labels
	public final static String pd_URL_Label = "Project Director URL:";
	public final static String username_Label = "Username:";
	public final static String password_Label = "Password:";
	public final static String shortCode_Label = "Default Project Short Code:";
	public final static String feature_Translation_Tab = "Feature Translation";
	public final static String addNewAssignment_ClassifyngCat = "Technical details [622] - Electronics Classification : 1.0";
	public final static String addNewAssignment_FeatureDescriptor = "Print technology[Print technology, 78]";
	public final static String addNewAssignment_FeatureDescriptor_AutoExposure = "Auto exposure[Auto exposure, 3672]";
	public final static String addNewAssignment_FeatureDescriptor_ShutterPriority = "Shutter priority AE[Shutter priority AE, 3674]";
	
	//PD Configuration
	public final static String PD_URL = "https://techqa8.translations.com/PD/";
	public final static String PD_Config_URL = "https://techqa8.translations.com/PD/services";

	public final static String PD_Invalid_Config_URL = "https://techqa8.translations.com/PD/services/invalidURL";

	public final static String invalid_PD_Config_URL = "//span[contains(text(),'Unable to create: Unexpected error')]";

	public final static String PD_Config_UserName = "hybpm";
	public final static String PD_Config_Password = "password1#";
	public final static String PD_Config_ShortCode = "HYB000008";
	
	//Product Cockpit > Title and Subtitle names
	public final static String PC_title_ApparelProductCatalog = "Apparel Product Catalog";
	public final static String PC_subtitle_Staged = "Staged";
	public final static String PC_product_Name = "Cap Blue Tomato BT Snow Trucker Cap black [300441142] - Apparel Product Catalog : Staged";
	
	//Job Items > Edit Item > Properties 
	public final static String editItem_Identifier = "name";
	public final static String editItem_Page_Title = "title";
	public final static String editItem_Description = "Description";
	public final static String editItem_Summary = "summary";
	
	//Dashborad > WCMS > Navigation_Node: Compartor Values
		public final static String Navigation_Node_Comparator_Value_StartsWith = "Starts";
		public final static String Navigation_Node_Comparator_Value_Equals = "Equals";
		
	//Dashborad > WCMS > Navigation_Node: Compartor Values
		public final static String Page_Comparator_Value_StartsWith = "Starts";
		public final static String Page_Comparator_Value_Equals = "Equals";
		
		
		
	//GL>Config>featuretab
		public final static String Classifying_Category = "Classifying";
		public final static String Feature_Description = "Feature";
		public final static String Environmentalconditions_1196 = "Environmental conditions [1196] - Electronics Classification : 1.0";
	
	//Create New Job Page Header Title
	public final static String create_New_Job_Header_Title = "Create New GlobalLink Job";
	
	//Create new job > Select_Project/Job_Config/ > ALL Labels
	public final static String jobConfig_Label_Item = "Items:";
	public final static String jobConfig_Label_Name = "Name:";
	public final static String jobConfig_Label_Source = "Source Language:";
	public final static String selectProject_Label_Project = "Project:";
	
	//Create Job > Job Name > Edit name 
	public final static String test_Job_name = "Test_ABC123";
		
		//Version (Update this with version you are testing)
		public  final static String Hybris_Version="7.3.2";
		
		public  final static String Hybris_Version_TestRail_comment="Passed on GLSAPCC 7.4.1 RC1 & PD 6.3.1";//"GLSAPCC7.4.0_SAPCC2011 & PD 5.21.0";
}
 	