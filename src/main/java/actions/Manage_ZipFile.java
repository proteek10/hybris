package actions;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Manage_ZipFile {
	private static Manage_ZipFile mz;
	String xmlPath="";
	
	public static synchronized Manage_ZipFile action(){
		try{
			if(mz.equals(null))
			{
				mz = new Manage_ZipFile();
			}
		}
		catch(Exception NOSYSTEM){
			mz = new Manage_ZipFile();
		}
		return mz;
	}
	
	public String extractFolder(String zipFile,String destPath) throws ZipException, IOException 
	{
	    //System.out.println(zipFile);
	    int BUFFER = 2048;
	    String newPath="";
	    File file = new File(zipFile);

	    ZipFile zip = new ZipFile(file);
	    
	     newPath = zipFile.substring(zipFile.lastIndexOf("/"), zipFile.length() - 4);
	    
	   // System.out.println(zipFile.length());
	    File destfile=new File(destPath);
	    //System.out.println(newPath);
	    destPath=destPath+"/"+newPath;

	    new File(destPath).mkdir();
	    //destfile
	    Enumeration zipFileEntries = zip.entries();

	    // Process each entry
	    while (zipFileEntries.hasMoreElements())
	    {
	        // grab a zip file entry
	        ZipEntry entry = (ZipEntry) zipFileEntries.nextElement();
	        String currentEntry = entry.getName();
	   //     xmlPath = currentEntry.toString();
	        File destFile = new File(destPath, currentEntry);
	        xmlPath = destFile.getPath();
	        //destFile = new File(newPath, destFile.getName());
	        File destinationParent = destFile.getParentFile();

	        // create the parent directory structure if needed
	        destinationParent.mkdirs();

	        if (!entry.isDirectory())
	        {
	            BufferedInputStream is = new BufferedInputStream(zip
	            .getInputStream(entry));
	            int currentByte;
	            // establish buffer for writing file
	            byte data[] = new byte[BUFFER];

	            // write the current file to disk
	            FileOutputStream fos = new FileOutputStream(destFile);
	            BufferedOutputStream dest = new BufferedOutputStream(fos,
	            BUFFER);

	            // read and write until last byte is encountered
	            while ((currentByte = is.read(data, 0, BUFFER)) != -1) {
	                dest.write(data, 0, currentByte);
	            }
	            
	            dest.flush();
	            dest.close();
	            is.close();
	            fos.close();
	            zip.close();
	            
	        }

	        if (currentEntry.endsWith(".zip"))
	        {
	             //found a zip file, try to open
	            //extractFolder();
	        }
	    }
	    System.out.println("Unzip Complete");
	    return xmlPath;
	}
	
	public String xmlContent(String xmlPath) throws Exception {
		//    File fXmlFile = new File(xmlPath);
			String cData="";
		    File file = new File(xmlPath);
		    DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		    Document doc = builder.parse(file);

		    NodeList nodes = doc.getElementsByTagName("item");
		    for (int i = 0; i < nodes.getLength(); i++) {
		      Element element = (Element) nodes.item(i);
		      NodeList title = element.getElementsByTagName("attribute");
		      Element line = (Element) title.item(0);
		      cData = getCharacterDataFromElement(line);
		      System.out.println("cData is : " + cData);
		    }
		    return cData;
		}
		    	
	public static String getCharacterDataFromElement(Element e) {
	    Node child = e.getFirstChild();
	    if (child instanceof CharacterData) {
	      CharacterData cd = (CharacterData) child;
	      return cd.getData();
	    }
	    return "";
	  }
	
	public ArrayList<String> get_XML_Node_Values(String xmlPath) throws Exception {
		DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document doc = builder.parse(xmlPath);
		XPathFactory xPathfactory = XPathFactory.newInstance();
		XPath xpath = xPathfactory.newXPath();
		XPathExpression expr = xpath.compile("//hybris-export/item[@feature]");
		NodeList nl = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
		ArrayList<String> list = new ArrayList<String>();
		for (int i = 0; i < nl.getLength(); i++)
		{
		    Node currentItem = nl.item(i);
		    String key = currentItem.getAttributes().getNamedItem("feature").getNodeValue();
		    System.out.println(key);
		    Thread.sleep(500);
		    list.add(key);
		    Thread.sleep(500);
		}
		System.out.println(list);
		return list;
	}
}
	

