package actions;



import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import org.gs4tr.qa.utility.BrowserFactory;

import com.opencsv.CSVReader;

import locators.PD4_filters_panel_Locators;
import locators.PD4_main_Locators;
import locators.PD4_main_client_dashboard_Locators;

public class Pmuser_client {
	private static Pmuser_client client_objects;

	/**
	 * Method used to self-instantiate the class. Will make sure one object, and
	 * one object only is created in memory for this class. The purpose is to be
	 * able to call this object dynamically from any JAVA class that includes
	 * this as an import.
	 * 
	 * @return Returns the object instantiated from the class.
	 */
	public static Pmuser_client action() {
		try {
			if (client_objects.equals(null)) {
				client_objects = new Pmuser_client();
			}
		} catch (Exception NOSYSTEM) {
			client_objects = new Pmuser_client();
		}
		return client_objects;
	}


	
/**
 * Go to specified folder, filter out submission and refresh until Status(if specified) is same as expected
 * 
 * @param submissionName name of the submission to be processed
 * 
 * @param folderName name of the folder to process the submission in<br>
 * (On Hold,Active,Completed)<br>(leave blank if filtering should be done in current folder)
 * 
 * @param status Expected status<br>(leave blank if verification not needed)
 * 
 * @param  second Time in seconds to wait the specified status (ignored if status is blank)
 * 
 * @throws InterruptedException 
 * 
 * @throws Exception used by Thread.sleep, which requires an exception handler.
 */
	public boolean filterAndWaitForStatus(String submissionName, String folderName, String status, Integer second) throws Exception {
		int folder=0;	
		System.out.println("Inside Method   of filter And Wait For Status");
		
		if(folderName.equalsIgnoreCase("On Hold"))
		{
		Verify.action().verifyElementPresent(PD4_main_client_dashboard_Locators.exts().workflows_onHoldSubmissionsButton,10);
		BrowserFactory.SystemEngine().findElement(PD4_main_client_dashboard_Locators.exts().workflows_onHoldSubmissionsButton).click();
		Thread.sleep(1000);
		folder=1;
		}
		
		else if(folderName.equalsIgnoreCase("Active"))
		{
		Verify.action().verifyElementPresent(PD4_main_client_dashboard_Locators.exts().workflows_activeSubmissionsButton,10);
		BrowserFactory.SystemEngine().findElement(PD4_main_client_dashboard_Locators.exts().workflows_activeSubmissionsButton).click();
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(PD4_main_client_dashboard_Locators.exts().submitTab).click();
		Thread.sleep(1000);
		folder=2;
		}
		
		else if(folderName.equalsIgnoreCase("Completed"))
		{
		Verify.action().verifyElementPresent(PD4_main_client_dashboard_Locators.exts().workflows_completedSubmissionsButton,10);
		BrowserFactory.SystemEngine().findElement(PD4_main_client_dashboard_Locators.exts().workflows_completedSubmissionsButton).click();
		Thread.sleep(1000);
		folder=3;
		}
		
		else if (folderName.equalsIgnoreCase("Archive"))
		{
			Verify.action().verifyElementPresent(PD4_main_client_dashboard_Locators.exts().workflows_archiveSubmissionsButton,10);
			BrowserFactory.SystemEngine().findElement(PD4_main_client_dashboard_Locators.exts().workflows_archiveSubmissionsButton).click();
			Thread.sleep(1000);
			folder = 4;
		}
		else if(folderName.equalsIgnoreCase("Gate"))
		{
			Verify.action().verifyElementPresent(PD4_main_client_dashboard_Locators.exts().workflows_gateSubmissionsButton,10);
			BrowserFactory.SystemEngine().findElement(PD4_main_client_dashboard_Locators.exts().workflows_gateSubmissionsButton).click();
			Thread.sleep(1000);
			folder = 5;
		}
		
		else if(folderName.equalsIgnoreCase("Templates"))
		{
			Verify.action().verifyElementPresent(PD4_main_client_dashboard_Locators.exts().workflows_templatesSubmissionsButton,10);
			BrowserFactory.SystemEngine().findElement(PD4_main_client_dashboard_Locators.exts().workflows_templatesSubmissionsButton).click();
			Thread.sleep(1000);
			folder = 6;
		}
		
		Thread.sleep(1000);
		Pmuser_client.action().filterByFirstFilterType(submissionName);
		Thread.sleep(1000);
		Verify.action().verifyElementPresent(PD4_main_client_dashboard_Locators.exts().firstDataRowName,10);
		Verify.action().verifyElementTextPresent(PD4_main_client_dashboard_Locators.exts().firstDataRowName,submissionName , second); 
		
		if(false==status.isEmpty())
			for (int _second = 0;; _second=_second+2) 
			{
				if (_second >= second)
				{
					
					System.out.print("Verification failed. Status for "+submissionName+" is not: "+status+".");
					return false;
				}
				try {
					switch(folder){
					case 1:
					if (BrowserFactory.SystemEngine().findElement(PD4_main_client_dashboard_Locators.exts().firstDataRowStatus_OnHoldFolder).getText().contains(status))
												
					{
						Verify.action().verifyElementPresent(PD4_main_client_dashboard_Locators.exts().firstDataRowName,10);
						BrowserFactory.SystemEngine().findElement(PD4_main_client_dashboard_Locators.exts().firstDataRowName).click();
						Thread.sleep(1000);
						return true;
						} break;
						
					case 2:
						if (BrowserFactory.SystemEngine().findElement(PD4_main_client_dashboard_Locators.exts().firstDataRowStatus_ActiveFolder).getText().contains(status))
						{
							Verify.action().verifyElementPresent(PD4_main_client_dashboard_Locators.exts().firstDataRowName,10);
						BrowserFactory.SystemEngine().findElement(PD4_main_client_dashboard_Locators.exts().firstDataRowName).click();
						Thread.sleep(1000);
						return true;
						} break;
						
					case 3:
						if (BrowserFactory.SystemEngine().findElement(PD4_main_client_dashboard_Locators.exts().firstDataRowStatus_CompletedFolder).getText().contains(status))
						{
							Verify.action().verifyElementPresent(PD4_main_client_dashboard_Locators.exts().firstDataRowName,10);
						BrowserFactory.SystemEngine().findElement(PD4_main_client_dashboard_Locators.exts().firstDataRowName).click();
						Thread.sleep(1000);
						return true;
						} break;
						
					case 4:
					{
						Verify.action().verifyElementPresent(PD4_main_client_dashboard_Locators.exts().firstDataRowName,10);
						BrowserFactory.SystemEngine().findElement(PD4_main_client_dashboard_Locators.exts().firstDataRowName).click();
						Thread.sleep(1000);
					} break;
					
					default: 
						if (BrowserFactory.SystemEngine().findElement(PD4_main_client_dashboard_Locators.exts().firstDataRowStatus).getText().contains(status))
						{
							Verify.action().verifyElementPresent(PD4_main_client_dashboard_Locators.exts().firstDataRowName,10);
						BrowserFactory.SystemEngine().findElement(PD4_main_client_dashboard_Locators.exts().firstDataRowName).click();
						Thread.sleep(1000);
						return true;
						} break;
					}
				} catch (Exception e) {
				}
				Thread.sleep(2000);
				BrowserFactory.SystemEngine().findElement(PD4_main_Locators.exts().refreshButton).click();
				Thread.sleep(1000);
			}
		Verify.action().verifyElementPresent(PD4_main_Locators.exts().firstDataRow,10);
		BrowserFactory.SystemEngine().findElement(PD4_main_Locators.exts().firstDataRow).click();
		Thread.sleep(1000);
		System.out.println("EOM - filterAndWaitForStatus()\n");
		return true;
		
	}
	/**Method to drill down row in Preview table for countering 'more...' Link
	 * Works for byLanguage=true only()!!!
	 * Works only for one language pair in submission
	 * 
	 * @param languagePair
	 * @param batch
	 * @param byLanguage
	 * @throws Exception
	 */
	public final void drillDown_mainPreview_forMore_Link(String languagePair, String batch, Boolean byLanguage) throws Exception {

		
		System.out.println("Inside Method  of drillDown_mainPreview_forMore_Link.");
				Integer i=1;
				Verify.action().verifyElementPresent(PD4_main_client_dashboard_Locators.exts().previewMain_rowX(i),10);
				StringBuffer id =new StringBuffer();
				StringBuffer first =new StringBuffer();
				StringBuffer second =new StringBuffer();

				//By Language
				if(byLanguage)
				{ 
					id.append("\u21D2");
					first.append(languagePair);
					second.append(batch);
				}
				else
				{
					id.append("gla_fileformat");
					first.append(batch);
					second.append(languagePair);
					
				}
				System.out.println("drill down method");
				//Iterate through all rows in a table with passed language pair
				while (Verify.action().verifyElementPresent(PD4_main_client_dashboard_Locators.exts().previewMain_rowX(i),10))
				{			
					//if correct first value found - open it
					if(Verify.action().verifyElementPresent(PD4_main_client_dashboard_Locators.exts().previewMain_rowWithTextX(i,first.toString()),10))
					{	
						Pmuser_client.action().openRowX(i);
						i++;
		
						//Iterate following rows until next language pair met or out of rows	
						while (Verify.action().verifyElementPresent(PD4_main_client_dashboard_Locators.exts().previewMain_rowX(i),10))
						{		
							{	
								Pmuser_client.action().openRowX(i);
								i++;}
							break;
							}
						}break;
					}
				System.out.println("End Method of drillDown_mainPreview_forMore_Link.");
				Thread.sleep(1000);
	}
	
	
	/**
	 * This Method is used to open rowX in preview pane
	 * 
	 * @return true if done without errors
	 * 
	 * @throws Exception
	 */
	public boolean openRowX(Integer row) throws Exception
	{
	
		if(Verify.action().verifyElementPresent(PD4_main_client_dashboard_Locators.exts().previewMain_plus_commonX(row), 10))
		{
			BrowserFactory.SystemEngine().findElement(PD4_main_client_dashboard_Locators.exts().previewMain_plus_commonX(row)).click();
			Thread.sleep(3000);
			return true;
		}
		
		if(Verify.action().verifyElementPresent(PD4_main_client_dashboard_Locators.exts().previewMain_plus_lastRowX(row), 10))
		{
			BrowserFactory.SystemEngine().findElement(PD4_main_client_dashboard_Locators.exts().previewMain_plus_lastRowX(row)).click();
			Thread.sleep(3000);
			return true;
		}
			
		return false;
	
	}

	/**
	 * chooses the first filter type in the dropdown (usually name) and filters
	 * 
	 * @param filter
	 *          input string to filter
	 *          
	 * 
	 * @throws Exception
	 */
		
	public final void filterByFirstFilterType(String filter) throws Exception 
		{
			Thread.sleep(1000);
			System.out.println("Inside Method  of filterByFirstFilterType.");
			BrowserFactory.SystemEngine().findElement(PD4_filters_panel_Locators.exts().filters_clearButton).click();
			Thread.sleep(10000);
			Verify.action().verifyElementPresent(PD4_filters_panel_Locators.exts().filters_chooseFilter_trigger,5);
			BrowserFactory.SystemEngine().findElement(PD4_filters_panel_Locators.exts().filters_chooseFilter_trigger).click();
			Thread.sleep(2000);
			Verify.action().verifyElementPresent(PD4_filters_panel_Locators.exts().filters_chooseFilter_valueFirstFilter("Submission Name"),5);
			Thread.sleep(1000);
			BrowserFactory.SystemEngine().findElement(PD4_filters_panel_Locators.exts().filters_chooseFilter_valueFirstFilter("Submission Name")).click();
			Thread.sleep(2000);
			Verify.action().verifyElementPresent(PD4_filters_panel_Locators.exts().filters_firstInput,10);
			Thread.sleep(1000);
			BrowserFactory.SystemEngine().findElement(PD4_filters_panel_Locators.exts().filters_firstInput).click();
			Thread.sleep(1000);
		    BrowserFactory.SystemEngine().findElement(PD4_filters_panel_Locators.exts().filters_firstInput).sendKeys(filter);
		    Thread.sleep(1000);
		    BrowserFactory.SystemEngine().findElement(PD4_filters_panel_Locators.exts().filters_findButton).click();
		    Thread.sleep(1000);
			System.out.println("End Method - filterByFirstFilterType().");
	}
	

	
	 public static List<String> readCSVFileDataLineByLine(String filePath) 
		{ 
			List<String> List = new ArrayList<String>();
		  
		    try { 
		  
		        // Create an object of filereader 
		        // class with CSV file as a parameter.	    	
		        FileReader filereader = new FileReader(filePath); //C:\\Users\\PCName\\Downloads\\ActivityReport.csv
		        
		        
		        // create csvReader object passing 
		        // file reader as a parameter 
		      CSVReader csvReader = new CSVReader(filereader); 
		        String[] nextRecord; 
		  
		        // we are going to read data line by line 
		        while ((nextRecord = csvReader.readNext()) != null) { 
		            for (String cell : nextRecord) { 
		            	
		            	  List.add(cell);	
		                
		            } 
		           
		        }    
		      
		    } 
		    catch (Exception e) { 
		        e.printStackTrace(); 
		    }
		    return List;
		} 
	 
	
	
	
 }
