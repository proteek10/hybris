package actions;

public class SE_Common_Prop_Cred {


	//Smart Edit Site Details
	public final static String SmartEdit_URL = "https://10.10.222.59:9002/smartedit";
	public final static String SmartEdit_UserName = "smartuser";
	public final static String SmartEdit_Password = "1234";
	
	//Target Language Name
	public final static String German = "German";
	public final static String French = "French";
	public final static String Spanish = "Spanish";
	
	//Select Site 
	public final static String Apparel_Site_DE = "Apparel Site DE";
	public final static String Apparel_Site_UK = "Apparel Site UK";
	public final static String Electronic_Site = "Electronics Site";
	
	//Catalog Type
	public final static String catalog_staged = "Staged";
	public final static String catalog_online = "Online";
	
	
	//Page Tyoe
	public final static String catalog_page_homepage = "Homepage";
	public final static String catalog_page_pages = "Pages";
	public final static String catalog_page_navigation_mgt = "Navigation Management";
	
	
	//Page Name
	public final static String catalog_page_addedit_Address_page = "Add Edit Address Page";
	
	//Edit
	public final static String basic_Edit_Mode = "Basic Edit";
	public final static String Advanced_Edit_Mode = "Advanced Edit";
	
	//Select Profile
	public final static String profileName_Default = "Hybris";
	
	//Source & Target Languages
	public final static String SourceLang_English = "English";
	public final static String TargetLang_German = "German";
	public final static String multi_Target_Lang = "German, French";
	
}
 	