package actions;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.gs4tr.qa.utility.BrowserFactory;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import com.opencsv.CSVReader;

import locators.Hybris_dashboard;
import locators.PD4_main_Locators;

public class common{
	private static common add_objects;
	
	
	/**
	 * Method used to self-instantiate the class.  Will make sure one object,
	 * and one object only is created in memory for this class.  The purpose is
	 * to be able to call this object dynamically from any JAVA class that 
	 * includes this as an import.
	 * 
	 * 
	 * @return Returns the object instantiated from the class.
	 */
	public static synchronized common action(){
		try{
			if(add_objects.equals(null))
			{
				add_objects = new common();
			}
		}
		catch(Exception NOSYSTEM){
			add_objects = new common();
		}
		return add_objects;
	}
	
	
	public static String GetCurrentDate() throws Exception 	
	{		
		Calendar cal = Calendar.getInstance();		
		DateFormat dateFormat = new SimpleDateFormat("MMMM d, yyyy", Locale.getDefault());				
		return (dateFormat.format(cal.getTime()));
		
	}
	
	public void search_Product(String title, String subtitle, String value, String catalog) throws Exception {
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(title)).click();
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(subtitle)).click();
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().advanceSearchIcon).click();
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().articleNumber_Value_input).sendKeys(value);
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().catalogVersion_Value_input).click();
		Thread.sleep(2000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().catalogVersion_Value_input).sendKeys(catalog);
		Thread.sleep(1000); 
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_CatalogVersion(catalog)).click();
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().search_Button).click();
		Thread.sleep(5000);
	}
	

	
	public void search_Product_CatalogAndIdentifier(String title, String subtitle, boolean catalog,boolean identifier,String attributeCatalog ,String attributeName) throws Exception {
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(title)).click();
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(subtitle)).click();
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().advanceSearchIcon).click();
		Thread.sleep(1000);

		if(catalog) {
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().catalogVersion_Value_input).sendKeys(attributeCatalog);
			Thread.sleep(1000);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_CatalogVersion(attributeCatalog)).click();
			Thread.sleep(2000);
		}
		if(identifier) {
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().catalog_identified_value_input).click();
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().catalog_identified_value_input).clear();
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().catalog_identified_value_input).sendKeys(attributeName);
			Thread.sleep(2000);
		}
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().search_Button).click();
		Thread.sleep(5000);
}

		

	public void search_Multiple_Products(String title, String subtitle, boolean articleNum, String value, boolean version, String catalog) throws Exception {

		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(title)).click();
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(subtitle)).click();
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().advanceSearchIcon).click();
		Thread.sleep(1000);
		if(articleNum) {
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().articleNumber_Value_input).sendKeys(value);
			Thread.sleep(1000);
		}
		if(version) {
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().catalogVersion_Value_input).click();
			Thread.sleep(2000);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().catalogVersion_Value_input).sendKeys(catalog);
			Thread.sleep(3000); 
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_CatalogVersion(catalog)).click();
			Thread.sleep(1000);
		}
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().search_Button).click();
		Thread.sleep(5000);
	}
	

	public void search_Category(String title, String subtitle, boolean name, boolean catalog, String attributeName, String attributeCatalog) throws Exception {
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(title)).click();
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(subtitle)).click();
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().advanceSearchIcon).click();
		Thread.sleep(1000);
		if(name) {
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().catagories_Name_Value_input).sendKeys(attributeName);
		}
		if(catalog) {
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().catalogVersion_Value_input).sendKeys(attributeCatalog);
		}
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().search_Button).click();
		Thread.sleep(5000);
	}

  	public void create_Job(String ProjectName,String SubmissionName,String LanguageName,boolean submit_onlyNew,boolean trans_prod_feature) throws Exception {
  		Thread.sleep(2000);
  		//Select search result 1st row
  		Verify.action().verifyElementPresent(Hybris_dashboard.exts().select_search_firstrow,5);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_search_firstrow).click();
		Thread.sleep(2000);
		//Translate Icon
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().translate_icon).click();
		Thread.sleep(2000);
		//Select Project
		common.action().SelectProject_CreateJob(ProjectName);
		Thread.sleep(1000);
		common.action().select_Profile_CreateJob(Hybris_Common_Properties_Cred.profileName_Default);
		Thread.sleep(1000);
		Verify.action().verifyElementPresent(Hybris_dashboard.exts().gl_Job_ID,5);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Job_ID).clear();
		Thread.sleep(500);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Job_ID).sendKeys(SubmissionName);
		Thread.sleep(2000);
		//Select Language
		Verify.action().verifyElementPresent(Hybris_dashboard.exts().select_Language(LanguageName),5);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_Language(LanguageName)).click();
		Thread.sleep(1000);
		
		//Select check box for Submit Only new changed content and translate prod feature
		if(submit_onlyNew){
			if(BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().checkbox_submitonly_newcontent).isSelected()==true)
			{
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().checkbox_submitonly_newcontent_uncheck).click();
				Thread.sleep(2000);
				System.out.println("--------->Submit Only new changed content");
			}
		}
		if(trans_prod_feature){
			if(BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().checkbox_translate_prod_feature).isSelected()==true)
			{
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().checkbox_translate_prod_feature_uncheck).click();
				Thread.sleep(2000);
				System.out.println("--------->translate prod feature");
			}
		}
			
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Job_Nxt_Btn).click();
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Job_Create_Btn).click();
		Thread.sleep(500);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Job_Done_Btn).click();
		Thread.sleep(500);
	
	} 
  	
  	public void create_Job_multipleLanguage(String ProjectName,String SubmissionName,boolean submit_onlyNew,boolean trans_prod_feature) throws Exception {
  		//Select search result 1st row
  		Verify.action().verifyElementPresent(Hybris_dashboard.exts().select_search_firstrow,5);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_search_firstrow).click();
		Thread.sleep(1000);
		//Translate Icon
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().translate_icon).click();
		Thread.sleep(1000);
		//Select Project
		common.action().SelectProject_CreateJob(ProjectName);
		Thread.sleep(1000);
		common.action().select_Profile_CreateJob(Hybris_Common_Properties_Cred.profileName_Default);
		Thread.sleep(1000);
		Verify.action().verifyElementPresent(Hybris_dashboard.exts().gl_Job_ID,5);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Job_ID).clear();
		Thread.sleep(500);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Job_ID).sendKeys(SubmissionName);
		Thread.sleep(2000);
		//Select Multiple Language
		Verify.action().verifyElementPresent(Hybris_dashboard.exts().clickOn_Lang_CheckBox,5);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().clickOn_Lang_CheckBox).click();
		Thread.sleep(1000);
		
		//Select check box for Submit Only new changed content and translate prod feature
		if(submit_onlyNew){
			if(BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().checkbox_submitonly_newcontent).isSelected()==true)
			{
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().checkbox_submitonly_newcontent_uncheck).click();
				Thread.sleep(2000);
				System.out.println("--------->Submit Only new changed content");
			}
		}
		if(trans_prod_feature){
			if(BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().checkbox_translate_prod_feature).isSelected()==true)
			{
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().checkbox_translate_prod_feature_uncheck).click();
				Thread.sleep(2000);
				System.out.println("--------->translate prod feature");
			}
		}
			
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Job_Nxt_Btn).click();
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Job_Create_Btn).click();
		Thread.sleep(500);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Job_Done_Btn).click();
		Thread.sleep(500);
	
	} 
  	
  	public void create_Job_Multiple_Categories(boolean selectItem,int num,String ProjectName,String SubmissionName,boolean lang,String LanguageName,boolean submit_onlyNew,
  			boolean trans_prod_feature,int items) throws Exception {
		//Translate Icon
  		if(selectItem) {
  			Verify.action().verifyElementPresent(Hybris_dashboard.exts().select_search_firstrow,5);
  			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_search_firstrow).click();
  			Thread.sleep(1000);
  			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().translate_icon).click();
  			Thread.sleep(1000);
  		}
  		else {
  			List<WebElement> els = BrowserFactory.SystemEngine().findElements(Hybris_dashboard.exts().select_Multiple_Products);
  			Thread.sleep(1000);
  			for(int i=0;i<num;i++)
  			{
  				els.get(i).click();
  				Thread.sleep(500);
  			}
  			Thread.sleep(1000);
  			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().WithoutSelectItem_translate_icon).click();
  			Thread.sleep(1000);
  		}
		//Select Project
		common.action().SelectProject_CreateJob(ProjectName);
		//Select Profile
		common.action().select_Profile_CreateJob(Hybris_Common_Properties_Cred.profileName_Default);
		Thread.sleep(1000);
		Verify.action().verifyElementPresent(Hybris_dashboard.exts().gl_Job_ID,5);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Job_ID).clear();
		Thread.sleep(500);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Job_ID).sendKeys(SubmissionName);
		Thread.sleep(2000);
		//Select Language
		if(lang) {
			Verify.action().verifyElementPresent(Hybris_dashboard.exts().select_Language(LanguageName),5);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_Language(LanguageName)).click();
			Thread.sleep(1000);
		}
		else {
			//Select Multiple Language
			Verify.action().verifyElementPresent(Hybris_dashboard.exts().clickOn_Lang_CheckBox,5);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().clickOn_Lang_CheckBox).click();
			Thread.sleep(1000);
		}
		//Select check box for Submit Only new changed content and translate prod feature
		if(submit_onlyNew){
			if(BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().checkbox_submitonly_newcontent).isSelected()==true)
			{
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().checkbox_submitonly_newcontent_uncheck).click();
				Thread.sleep(2000);
				System.out.println("--------->Submit Only new changed content");
			}
		}
		if(trans_prod_feature){
			if(BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().checkbox_translate_prod_feature).isSelected()==true)
			{
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().checkbox_translate_prod_feature_uncheck).click();
				Thread.sleep(2000);
				System.out.println("--------->translate prod feature");
			}
		}
			
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Job_Nxt_Btn).click();
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Job_Create_Btn).click();
		Thread.sleep(5000);
		
		Verify.action().verifyTextPresent(items+"item(s) queued for translation.", 5);
		System.out.println("Queued Job Values---------->"+items);
		
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Job_Done_Btn).click();
		Thread.sleep(500);
	
	} 
  	

	public void selectMulti_Products(int num) throws Exception {
		List<WebElement> els = BrowserFactory.SystemEngine().findElements(Hybris_dashboard.exts().select_Multiple_Products);
			Thread.sleep(1000);
			for(int i=0;i<num;i++)
			{
				els.get(i).click();
				Thread.sleep(500);
			}
			Thread.sleep(1000);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().WithoutSelectItem_translate_icon).click();
			Thread.sleep(1000);
	}

  	
  	public void verify_Globe_Icon_Fields(boolean selectItem, int num, boolean edit, String str) throws Exception {
  		if(selectItem) {
  			Verify.action().verifyElementPresent(Hybris_dashboard.exts().select_search_firstrow,5);
  			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_search_firstrow).click();
  			Thread.sleep(1000);
  			List<WebElement> els = BrowserFactory.SystemEngine().findElements(Hybris_dashboard.exts().globe_Icon_Fields);
  			for(int i=0;i<num;i++)
  			{
  				String name = els.get(i).getText();
  				Thread.sleep(500);
  				System.out.println("Globe Icon Field is : " + name);
  				Thread.sleep(500);
  			}
  		}
  		if(edit) {
  			List<WebElement> els = BrowserFactory.SystemEngine().findElements(Hybris_dashboard.exts().globe_Icon_Fields_Input);
  			for(int i=0;i<els.size();i++)
  			{
  				els.get(i).clear();
  				Thread.sleep(500);
  				els.get(i).sendKeys(str);
  				Thread.sleep(500);
  			}
  			Thread.sleep(500);
  			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().editItem_Save_Button).click();
  			Thread.sleep(1000);
		}
  		Thread.sleep(500);
  		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().downArrow_Icon).click();
  	}
  	
  	
  	public void search_Job(String title, String subtitle,String SubmissionName) throws Exception {
  		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(title)).click();
  		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(subtitle)).click();
		Thread.sleep(3000);
		
		//Advanced Search Job Name
		if(Verify.action().verifyElementNotPresent(Hybris_dashboard.exts().gl_jobs_attributeText, 5))
		{
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().advanceSearchIcon).click();
			Thread.sleep(2000);
		}
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Jobs_Attribute_Name).clear();
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Jobs_Attribute_Name).sendKeys(SubmissionName);
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().search_Button).click();
		Thread.sleep(1000);
		if(Verify.action().verifyElementPresent(Hybris_dashboard.exts().searched_Result(SubmissionName),  5));
		{
			System.out.println(SubmissionName + ": is found");
		}
  	}
	
  	
  	
	public void search_Page(String title, String subtitle, String attributeName1, String val1, String attributeName2, 
			String val2) throws Exception {
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(title)).click();
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(subtitle)).click();
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().advanceSearchIcon).click();
		Thread.sleep(1000);
		boolean result=Verify.action().verifyElementPresent(Hybris_dashboard.exts().pageAttriubteNames(attributeName1),  5);
		if(result!=true)
		{
			System.out.println("inside if");
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().clickOn_Attribute_dropDown).click();
			Thread.sleep(1000);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_PageAttributeName(attributeName1)).click();
			Thread.sleep(500);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().clickOn_Value_dropDown).click();
			Thread.sleep(1000);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().clickOn_Value_dropDown).sendKeys(val1);
			Thread.sleep(1000);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_Attribute_Value(val1)).click();
			Thread.sleep(1000);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().add_Button).click();
			Thread.sleep(500);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().search_Button).click();
		}
		else {
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().clickOn_Attribute_dropDown).click();
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_PageAttributeName(attributeName1)).click();
				Thread.sleep(500);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().clickOn_Value_dropDown).click();
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().clickOn_Value_dropDown).sendKeys(val1);
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_Attribute_Value(val1)).click();
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().add_Button).click();
				Thread.sleep(500);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().search_Button).click();
			}
		
		boolean result1=Verify.action().verifyElementPresent(Hybris_dashboard.exts().pageAttriubteNames(attributeName2),  5);
		if(result1!=true)
		{
			System.out.println("inside if");
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().clickOn_Attribute_dropDown).click();
			Thread.sleep(1000);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_PageAttributeName(attributeName2)).click();
			Thread.sleep(500);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().clickOn_Value_dropDown).sendKeys(val2);
			Thread.sleep(1000);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().add_Button).click();
			Thread.sleep(500);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().search_Button).click();
			Thread.sleep(500);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().clickOn_SearchedPage(val2)).click();
		}
		else {
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().clickOn_Attribute_dropDown).click();
			Thread.sleep(1000);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_PageAttributeName(attributeName2)).click();
			Thread.sleep(500);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().clickOn_Value_dropDown).sendKeys(val2);
			Thread.sleep(1000);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().add_Button).click();
			Thread.sleep(500);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().search_Button).click();
			Thread.sleep(1000);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().clickOn_SearchedPage(val2)).click();
			Thread.sleep(2000);
			}			
	 }	
		
	
	
	public void search_Page_ProductComparator(String title, String subtitle, String attributeName1,String comparator_value, String val1, String attributeName2, String comparator_value2,
			String val2) throws Exception {
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(title)).click();
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(subtitle)).click();
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().advanceSearchIcon).click();
		Thread.sleep(1000);
		boolean result=Verify.action().verifyElementPresent(Hybris_dashboard.exts().pageAttriubteNames(attributeName1),  5);
		if(result!=true)
		{
			System.out.println("inside if");
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().clickOn_Attribute_dropDown).click();
			Thread.sleep(1000);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_PageAttributeName(attributeName1)).click();
			Thread.sleep(500);		
			//Comparator
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().clickOn_comparator_dropDown).click();
			Thread.sleep(500);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_comparatorName(comparator_value)).click();
			Thread.sleep(500);			
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().clickOn_Value_dropDown).click();
			Thread.sleep(1000);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().clickOn_Value_dropDown).sendKeys(val1);
			Thread.sleep(1000);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_Attribute_Value(val1)).click();
			Thread.sleep(1000);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().add_Button).click();
			Thread.sleep(500);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().search_Button).click();
		}
		else {
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().clickOn_Attribute_dropDown).click();
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_PageAttributeName(attributeName1)).click();
				Thread.sleep(500);
				//Comparator
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().clickOn_comparator_dropDown).click();
				Thread.sleep(500);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_comparatorName(comparator_value)).click();
				Thread.sleep(500);			
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().clickOn_Value_dropDown).click();
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().clickOn_Value_dropDown).sendKeys(val1);
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_Attribute_Value(val1)).click();
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().add_Button).click();
				Thread.sleep(500);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().search_Button).click();
			}
		
		boolean result1=Verify.action().verifyElementPresent(Hybris_dashboard.exts().pageAttriubteNames(attributeName2),  5);
		if(result1!=true)
		{
			System.out.println("inside if");
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().clickOn_Attribute_dropDown).click();
			Thread.sleep(1000);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_PageAttributeName(attributeName2)).click();
			Thread.sleep(500);
			//Comparator
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().clickOn_comparator_dropDown).click();
			Thread.sleep(500);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_comparatorName(comparator_value2)).click();
			Thread.sleep(500);			
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().clickOn_Value_dropDown).sendKeys(val2);
			Thread.sleep(1000);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().add_Button).click();
			Thread.sleep(500);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().search_Button).click();
			Thread.sleep(500);
	//		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().clickOn_SearchedPage(val2)).click();
		}
		else {
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().clickOn_Attribute_dropDown).click();
			Thread.sleep(1000);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_PageAttributeName(attributeName2)).click();
			Thread.sleep(500);
			//Comparator
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().clickOn_comparator_dropDown).click();
			Thread.sleep(500);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_comparatorName(comparator_value2)).click();
			Thread.sleep(500);			
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().clickOn_Value_dropDown).sendKeys(val2);
			Thread.sleep(1000);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().add_Button).click();
			Thread.sleep(500);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().search_Button).click();
			Thread.sleep(1000);
	//		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().clickOn_SearchedPage(val2)).click();
			Thread.sleep(2000);
			}			
	 }
	
	
	  /**
	   * This method is used to get the folder names in directory using Name of user who downloads translation kit
	   * @param basePath Directory with folders
	   * @param vendoruserName Name of user who downloads translation kit from PD.
	   * @return
	   */
	  public  String getFolderName(String basePath,String FileName) {
		  String [] children=null;
		  String folderName =null;
		  
		  File folder = new File(basePath);
		    if (folder.isDirectory())
		    {
		    	children=folder.list();
		    	 for(int i=0;i<children.length;i++)
		    	 {
		 	    	folderName=children[i];
		 	    	if(folderName.contains(FileName))
		 	    		break;
		 	    }
		    }
	    		return folderName;
	}
	
	  
	  
	  /**
		 * @author Mandar
		 * 
		 * This method is used to verify if by giving PARTIAL file name, file is present in Download folder
		 * @param basePath
		 * @param fileName
		 * @return
		 * @throws IOException
		 */
		public boolean getfileNamePresent(String basePath, String fileName)
				throws IOException {

			boolean filePresent = false;
			File dir = new File(basePath);
			List<File> files = (List<File>) FileUtils.listFiles(dir,
					TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
			for (File file : files) {
				System.out.println("File Name-->"+file.getName());
				if (file.getName().contains(fileName)) {
					filePresent = true;
					System.out.println("File is present");
					break;
				}
			}
			return filePresent;
		}

	public void serach_Multi_Pages(String title, String subtitle,boolean catalog, String attributeName1, String val1,boolean page, String attributeName2, 
			String val2) throws Exception {
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(title)).click();
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(subtitle)).click();
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().advanceSearchIcon).click();
		Thread.sleep(1000);
		if(catalog) {
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().clickOn_Attribute_dropDown).click();
			Thread.sleep(1000);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_PageAttributeName(attributeName1)).click();
			Thread.sleep(500);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().clickOn_Value_dropDown).click();
			Thread.sleep(1000);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().clickOn_Value_dropDown).sendKeys(val1);
			Thread.sleep(1000);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_Attribute_Value(val1)).click();
			Thread.sleep(1000);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().add_Button).click();
			Thread.sleep(500);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().search_Button).click();
			Thread.sleep(1000);
		}
		if(page) {
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().clickOn_Attribute_dropDown).click();
			Thread.sleep(1000);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_PageAttributeName(attributeName2)).click();
			Thread.sleep(500);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().clickOn_Value_dropDown).sendKeys(val2);
			Thread.sleep(1000);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().add_Button).click();
			Thread.sleep(500);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().search_Button).click();
			Thread.sleep(1000);
		//	BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().clickOn_SearchedPage(val2)).click();
		}
	}
	
	
	public void Go_toFeatureTab_Search(String title,String subtitle,boolean includeFeature,boolean Defaultsearch,String attributeName1,String comparator_value,String val1) throws Exception {
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(title)).click();
		Thread.sleep(2000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(subtitle)).click();
		Thread.sleep(2000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().pd_URL).click();
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Config_Labels(Hybris_Common_Properties_Cred.feature_Translation_Tab)).click();
		Thread.sleep(1000);
		if(includeFeature) {
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Config_FeatureTrans_IncludeFeatures).click();
			Thread.sleep(500);
		}
		else {
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Config_FeatureTrans_ExcludeFeatures).click();
			Thread.sleep(500);
		}
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Config_Add_Button).click();
		Thread.sleep(1000);

	//	boolean result=Verify.action().verifyElementPresent(Hybris_dashboard.exts().pageAttriubteNames(attributeName1),  5);
		if(Defaultsearch==true)
		{
			System.out.println("inside if Add Default Filter");
			//Comparator
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().featureTabdefault_clickOn_comparator_dropDown).click();
			Thread.sleep(1000);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_comparatorName(comparator_value)).click();
			Thread.sleep(1000);			
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().featureTab_default_clickOn_Value_dropDown).click();
			Thread.sleep(1000);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().featureTab_default_clickOn_Value_dropDown).sendKeys(val1);
			Thread.sleep(1000);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_Attribute_Value(val1)).click();
			Thread.sleep(1000);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().addNewAssignment_addBtn).click();
			Thread.sleep(1000);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().addNewAssignment_SearchBtn).click();
		}
		if(Defaultsearch==false) {
			System.out.println("inside if Not Add Default Filter");
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().addNewAssignment_SearchBtn).click();
			}
	}
	
	
	
	
	public Boolean verify_Job_Status(String subtitle1, String subtitle2, String status, String SubmissionName) throws Exception {
		
		if(status!="QUEUED")
		{
			if(subtitle1.equalsIgnoreCase("Submit Queued Jobs"))
			{
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(subtitle1)).click();
				if(Verify.action().verifyElementPresent(Hybris_dashboard.exts().Cronjob_Submit_Message,  5));
				{
					System.out.println("Cronjob submitted message has been displayed successfully");
				}
				Thread.sleep(10000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Jobs_Attribute_Name).clear();
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Jobs_Attribute_Name).sendKeys(SubmissionName);
				Thread.sleep(10000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().search_Button).click();
				Thread.sleep(1000);
				if(Verify.action().verifyElementPresent(Hybris_dashboard.exts().verify_Job_Status(status),  5));
				{
					System.out.println("Status of job is: "+ status);
				}
			}
		if(subtitle2.equalsIgnoreCase("Fetch Translations"))
		{
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(subtitle2)).click();
			if(Verify.action().verifyElementPresent(Hybris_dashboard.exts().Cronjob_Fetch_Message,  5));
			{
				System.out.println("Cronjob fetch message has been displayed successfully");
			}
			Thread.sleep(1000);
			
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Jobs_Attribute_Name).clear();
			Thread.sleep(1000);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Jobs_Attribute_Name).sendKeys(SubmissionName);
			Thread.sleep(30000);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().search_Button).click();
			Thread.sleep(30000);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().search_Button).click();
			Thread.sleep(30000);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().search_Button).click();
			Thread.sleep(2000);
			if(Verify.action().verifyElementPresent(Hybris_dashboard.exts().verify_Job_Status(status),  5));
			{
				System.out.println("Status of job is: "+ status);
			}
		}
	}
		else {
			if(Verify.action().verifyElementPresent(Hybris_dashboard.exts().verify_Job_Status(status),  5));
			{
				System.out.println("Status of job is: "+ status);
			}
		}
		return true;	
	}
	
	public void switchTo_Cockpit_Mode(String value) throws Exception {
		
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().title_Administration_Cockpit).click();
		Thread.sleep(500);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_Cockpit_From_dropDown(value)).click();
		Thread.sleep(2000);
	}
	
public void SelectProject_CreateJob(String ProjectName) throws Exception {
		
	if(Verify.action().verifyElementPresent(Hybris_dashboard.exts().selectProject_tab, 5)){
		//Select Project
		Verify.action().verifyElementPresent(Hybris_dashboard.exts().select_Project(ProjectName),5);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_Project(ProjectName)).click();
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Job_Nxt_Btn).click();
		Thread.sleep(1000);
	}
	}

	public void select_Profile_CreateJob(String profileName) throws Exception {
	
	if(Verify.action().verifyElementPresent(Hybris_dashboard.exts().selectProfile_Tab, 5)){
		//Select Profile
		Verify.action().verifyElementPresent(Hybris_dashboard.exts().select_Project(profileName),5);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_Project(profileName)).click();
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Job_Nxt_Btn).click();
		Thread.sleep(1000);
	}
	}
	
	public void WaitUntil_Progress() throws Exception {
		
		if(Verify.action().verifyElementPresent(Hybris_dashboard.exts().progress_bar, 5)){
			Verify.action().WaitUntilElementPresent(Hybris_dashboard.exts().pd_config_invalidURL_warning, 50);
			Thread.sleep(2000);
			}
	}
	
	
public Boolean Verify_WaitUntil_JobStatus(String JobName,String Status) throws Exception {
		
		if(Verify.action().verifyElementPresent(Hybris_dashboard.exts().verify_JobName_WithStatus(JobName,Status), 5)){
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().search_Button).click();
			Verify.action().WaitUntilElementPresent(Hybris_dashboard.exts().verify_JobName_WithStatus(JobName,Status), 50);
			Thread.sleep(2000);
			}
		return true;
	}
	
	public boolean verify_Naming_Convention(String[] str) throws Exception {
		
		for (String eachString : str) {
				eachString= eachString.trim();
				Thread.sleep(500);
		        System.out.println( "Does \"" + eachString + "\" contain whitespace? " + 
		                             containsWhitespace(eachString));
		}
		return true;
	}
	
	public boolean containsWhitespace(String str) {
		    return str.matches(".*\\s.*");
		}
	
	public boolean verify_Targets_Under_JobItems(String subtitle1, String submissionName,  String target) throws Exception {
		boolean check = false;
		Thread.sleep(10000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(subtitle1)).click();
		Thread.sleep(10000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().jobItems_search_Button).click();
		Thread.sleep(1000);
		if(Verify.action().verifyElementPresent(Hybris_dashboard.exts().verify_Status(submissionName),  5));
		{
			System.out.println(submissionName + ": is found");
		}
		Thread.sleep(1000);
		if(Verify.action().verifyElementPresent(Hybris_dashboard.exts().verify_Status(target),  5));
		{
			System.out.println(target + ": is found");
			check = true;
		}
	
		return check;	
	}
	
	public boolean verify_Jobs_Under_JobItems(String subtitle1, String submissionName, String name, String item) throws Exception {
		boolean check = false;
		Thread.sleep(10000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(subtitle1)).click();
		Thread.sleep(10000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().jobItems_search_Button).click();
		Thread.sleep(1000);
		if(Verify.action().verifyElementPresent(Hybris_dashboard.exts().verify_Status(submissionName),  5));
		{
			System.out.println(submissionName + ": found");
			
		}
		if(Verify.action().verifyElementPresent(Hybris_dashboard.exts().verify_Status(name),  5));
		{
			System.out.println(name + ": found");
		
		}
		if(Verify.action().verifyElementPresent(Hybris_dashboard.exts().verify_Status(item),  5));
		{
			System.out.println(item + ": found");
			check = true;
		}
	
		return check;	
	}
	
	public void add_New_Fields_Under_Technical_Features(String label, String[] str ) throws Exception {
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().catalog_product_Labels(label)).click();
		Thread.sleep(3000);
		General.action().jsScroll(Hybris_dashboard.exts().products_Attributes_TechnicalFeatures);
		Thread.sleep(1000);
		for(int i=0; i<str.length; i++) {
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().products_Attributes_TechnicalFeatures_AddIcon).click();
			Thread.sleep(500);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().products_Attributes_TechnicalFeatures_InputField).sendKeys(str[i]);
			Thread.sleep(500);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().products_Attributes_TechnicalFeatures_AddBtn).click();
			Thread.sleep(500);
		}
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().save_Password).click();
		Thread.sleep(1000);
		
	}

	//***************************PD METHODS*********************************************//
	
	
		/**
		 * Login to the Project Director  web-site.
		 * 
		 * 
		 * @param user
		 *            user name to login as in Project Director .
		 * 
		 * @param passw
		 *            password to use when login into Project Director.
		 * @param locator
		 * 
		 * @throws Exception
		 *             used by isPresent, in case element is not present.
		 */
		public void PDlogIn(String user, String passw) throws Exception {
			System.out.println("In Method-  pd4_logIn() \n");
			//isPresent(PD4_main_Locators.exts().inputLoginUsername);
			
	//		if(Verify.action().verifyElementPresent(PD4_main_Locators.exts().inputLoginwithSSOButton,  5));
	//		{
	//			BrowserFactory.SystemEngine().findElement(PD4_main_Locators.exts().inputLoginwithLocalButton).click();
	//		}
			
			Verify.action().verifyElementPresent(PD4_main_Locators.exts().inputLoginUsername, 80);
			BrowserFactory.SystemEngine().findElement(PD4_main_Locators.exts().inputLoginUsername).sendKeys(user);
			Thread.sleep(1000);
			BrowserFactory.SystemEngine().findElement(PD4_main_Locators.exts().inputLoginPassword).sendKeys(passw);
			Thread.sleep(1000);
			BrowserFactory.SystemEngine().findElement(PD4_main_Locators.exts().loginButton).click();
			Thread.sleep(20000);

			Verify.action().verifyElementPresent(PD4_main_Locators.exts().admin_accountButton, 10);
			System.out.println("\nLogin by -> "+user+"\n");
			Thread.sleep(1000);	
		}

		/**
		 * Logout of the Project Director web-site.
		 * 
		 * 
		 * @throws Exception
		 *             used by isPresent, in case element is not present.
		 */
		public void PDlogOut() throws Exception {
			System.out.println("In Method-  pd4_logOut()-- \n");
			Thread.sleep(1000);	
			Verify.action().verifyElementPresent(PD4_main_Locators.exts().admin_accountButton,60);
			BrowserFactory.SystemEngine().findElement(PD4_main_Locators.exts().admin_accountButton).click();
			Thread.sleep(1000);	
			Verify.action().verifyElementPresent(PD4_main_Locators.exts().logoutButton,10);
			BrowserFactory.SystemEngine().findElement(PD4_main_Locators.exts().logoutButton).click();
			Thread.sleep(1000);	
			BrowserFactory.SystemEngine().findElement(PD4_main_Locators.exts().logOut_yesButton).click();
			Thread.sleep(3000);
			System.out.println("Logged Out...");
		}
		
		/**
		 * Filter_Submission
		 * @author Proteek
		 * 
		 * @param URL
		 *           Enter URL of PD and CRXDE
		 * @throws Exception
		 *             used by isPresent, in case element is not present.
		 */
		public void Open_New_Instance_URL(String URL) throws Exception {

			System.out.println("******INSIDE METHOD Open_PD_Instance_URL() ************");


			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_T);
			Thread.sleep(4000);
			robot.keyRelease(KeyEvent.VK_T);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			Thread.sleep(3000);

			BrowserFactory.SystemEngine();
			ArrayList<String> tabs = new ArrayList<String> (BrowserFactory.driver.getWindowHandles());
			Thread.sleep(3000);
			BrowserFactory.SystemEngine();
			BrowserFactory.driver.switchTo().window(tabs.get(1));
			Thread.sleep(3000);
			BrowserFactory.SystemEngine();
			BrowserFactory.driver.get(URL);
			Thread.sleep(4000);


			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			Thread.sleep(8000);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			Thread.sleep(3000);

			System.out.println("**** EOM Open_PD_Instance_URL() **********");

			}
	
		/**
		 *  To open new tab
		 * @author Praveen
		 * 
		 * @param URL
		 *           Enter URL of PD
		 *
		 *@throws Exception
		 *             used by isPresent, in case element is not present.
		 */
		
	public void open_New_Instance(String URL) throws Exception {
		((JavascriptExecutor) BrowserFactory.driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(BrowserFactory.driver.getWindowHandles());
		BrowserFactory.driver.switchTo().window(tabs.get(1));
		Thread.sleep(3000);
		BrowserFactory.SystemEngine();
		BrowserFactory.driver.get(URL);
		Thread.sleep(8000);
	}
		
		
		/**
		 * Close_ActiveTab
		 * @author Proteek
		 * 
		 * @param originalHandle
		 *           switch back to original Handle window
		 * @throws Exception
		 *             used by isPresent, in case element is not present.
		 */

	public void Close_ActiveTab(String originalHandle) throws Exception {

		System.out.println("switchToPopupWindow()");
		System.out.println("originalHandle--->"+originalHandle);

	    BrowserFactory.SystemEngine();
		for(String handle : BrowserFactory.driver.getWindowHandles()) {
	        if (!handle.equals(originalHandle)) {
	        	System.out.println("handle--->"+handle);
	        	BrowserFactory.SystemEngine();
				BrowserFactory.driver.switchTo().window(handle);
	        	BrowserFactory.SystemEngine();
				BrowserFactory.driver.close();
	        }
		}
    }
	
	/**
	 * Close_ActiveTab
	 * @author Praveen
	 * 
	 * @param winHandle
	 *           switches back to Current window
	 */
	public void switchTo_CurrentWindow() {
	//	String winHandleBefore = BrowserFactory.driver.getWindowHandle();

        for(String winHandle : BrowserFactory.driver.getWindowHandles()){
        	BrowserFactory.driver.switchTo().window(winHandle);
        }
	}
		
		
		
		//**********************************************************************************//
	public boolean verify_Translated_Content(String subtitle1, String submissionName, String name) throws Exception {
		boolean check = false;
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(subtitle1)).click();
		Thread.sleep(1000);
		if(Verify.action().verifyElementPresent(Hybris_dashboard.exts().verify_Status(submissionName),  5));
		{
			System.out.println(submissionName + ": is found");
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().verify_Status(submissionName)).click();
		}
		Thread.sleep(2000);
		General.action().doubleClick(Hybris_dashboard.exts().jobItems_Details_Targets_label);
		Thread.sleep(2000);
		General.action().doubleClick(Hybris_dashboard.exts().editItem_JobItem);
		Thread.sleep(2000);
		General.action().doubleClick(Hybris_dashboard.exts().editItem_Item);
		Thread.sleep(2000);
		if(name.contains("name"))
		{
			System.out.println("Title Identifier is found");
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().editItem_TranslateIcon(Hybris_Common_Properties_Cred.editItem_Identifier)).click();
		}
		Thread.sleep(1000);
		if(name.contains("title"))
		{
			System.out.println("Page Title is found");
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().editItem_TranslateIcon(Hybris_Common_Properties_Cred.editItem_Page_Title)).click();
		}
	    Thread.sleep(2000);
	    String str1 = BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().editItem_Lang_English).getAttribute("value");
	    Thread.sleep(2000);
	    String str2 = BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().editItem_Lang_German).getAttribute("value");
	    Thread.sleep(2000);
	    if(str1!=str2)
	    {
	    	System.out.println("All contents are Translated successfully");
	    	check = true;
	    }
	    return check;
		
	}
	
	public boolean verify_Content_In_JobItems(String subtitle1, String submissionName,boolean item,String titleIdentifier,String titleSummary) throws Exception {
		boolean check = false;
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(subtitle1)).click();
		Thread.sleep(1000);
		if(Verify.action().verifyElementPresent(Hybris_dashboard.exts().verify_Status(submissionName),  5));
		{
			System.out.println(submissionName + ": is found");
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().verify_Status(submissionName)).click();
		}
		Thread.sleep(2000);
		if(item) {
			General.action().doubleClick(Hybris_dashboard.exts().jobItems_Click_On_Item);
			Thread.sleep(2000);
			if(titleIdentifier.contains("name"))
			{
				System.out.println("Title Identifier is found");
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().editItem_TranslateIcon(Hybris_Common_Properties_Cred.editItem_Identifier)).click();
				Thread.sleep(1000);
				String str1 = BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().jobItem_Item_Identifier_Lang_English).getAttribute("value");
			    Thread.sleep(2000);
			    String str2 = BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().jobItem_Item_Identifier_Lang_German).getAttribute("value");
			    Thread.sleep(2000);
			    if(str1!=str2)
			    {
			    	System.out.println("All contents are Translated successfully from IDENTIFIER");
			    }
			}
		    Thread.sleep(2000);
	/*	    if(titleSummary.contains("summary"))
			{
				System.out.println("Summary Title is found");
				Thread.sleep(1000);
				General.action().jsScroll(Hybris_dashboard.exts().editItem_TranslateIcon(Hybris_Common_Properties_Cred.editItem_Summary));
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().editItem_TranslateIcon(Hybris_Common_Properties_Cred.editItem_Summary)).click();
				Thread.sleep(1000);
				General.action().jsScroll(Hybris_dashboard.exts().jobItem_Item_Summary_Lang_English);
				Thread.sleep(1000);
				String str1 = BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().jobItem_Item_Summary_Lang_English).getAttribute("value");
			    Thread.sleep(2000);
			    General.action().jsScroll(Hybris_dashboard.exts().jobItem_Item_Summary_Lang_German);
			    Thread.sleep(1000);
			    String str2 = BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().jobItem_Item_Summary_Lang_German).getAttribute("value");
			    Thread.sleep(2000);
			    if(str1!=str2)
			    {
			    	System.out.println("All contents are Translated successfully from SUMMARY");
			    }
			}
		    Thread.sleep(2000); */
		    check=true;
		}
	    return check;
	}
	
	public boolean verify_Content_In_Page(boolean pageTitle,String subtitle1, boolean chckSpecialChar) throws Exception {
		boolean check = false;
		if(pageTitle) {
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(subtitle1)).click();
			Thread.sleep(2000);
			Verify.action().verifyElementPresent(Hybris_dashboard.exts().select_search_firstrow,5);
  			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_search_firstrow).click();
  			Thread.sleep(2000);
  			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().globe_Icon_Page_Title).click();
  			Thread.sleep(2000);
  		    String str1 = BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().editItem_Lang_English).getAttribute("value");
  		    System.out.println("Data in English is: " +str1);
  		    Thread.sleep(2000);
  		    String str2 = BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().editItem_Lang_German).getAttribute("value");
  		    System.out.println("Data in German is: " +str2);
  		    Thread.sleep(2000);
  		    if(str1!=str2)
  		    {
  		    	System.out.println("All contents are Translated successfully");
  		    	check = true;
  		    }
  		    if(chckSpecialChar) {
  		    	 Pattern p = Pattern.compile("[^A-Za-z0-9]");
  		       Matcher m = p.matcher(str1);
  		      // boolean b = m.matches();
  		       boolean b = m.find();
  		       if (b)
  		          System.out.println("There is a special character in my string ");
  		       else
  		           System.out.println("There is no special char.");
  		    }
		}
		return check;
	}
	
	public boolean verify_Content_In_Catalog(boolean category, boolean product,String subtitle1,String subtitle2) throws Exception {
		boolean check = false;
		if(category) {
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(subtitle1)).click();
			Thread.sleep(2000);
			Verify.action().verifyElementPresent(Hybris_dashboard.exts().select_search_firstrow,5);
  			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_search_firstrow).click();
  			Thread.sleep(2000);
  			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().globe_Icon_Name).click();
  			Thread.sleep(2000);
  		    String str1 = BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().catagories_Name_Lang_English).getAttribute("value");
  		    System.out.println("Name value in English is: " +str1);
  		    Thread.sleep(2000);
  		    String str2 = BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().catagories_Name_Lang_German).getAttribute("value");
  		    System.out.println("Name value in German is: " +str2);
  		    Thread.sleep(2000);
  		    if(str1!=str2)
  		    {
  		    	System.out.println("All contents are Translated successfully");
  		    	check = true;
  		    }
		}
		if(product) {
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(subtitle2)).click();
			Thread.sleep(2000);
			Verify.action().verifyElementPresent(Hybris_dashboard.exts().select_search_firstrow,5);
  			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_search_firstrow).click();
  			Thread.sleep(2000);
  			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().globe_Icon_Identifier).click();
			Thread.sleep(1000);
			String str1 = BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().products_Identifier_Lang_English).getAttribute("value");
			System.out.println("Identifier value in English is: " +str1);
		    Thread.sleep(2000);
		    String str2 = BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().products_Identifier_Lang_German).getAttribute("value");
		    System.out.println("Identifier value in German is: " +str2);
		    Thread.sleep(2000);
		    if(str1!=str2)
		    {
		    	System.out.println("All contents are Translated successfully from IDENTIFIER");
		    	check=true;
		    }
		}	
		return check;
	}
	
	public boolean verify_Catalog_Product_Attribute_Values(boolean translated,String label, String value,boolean notTrans) throws Exception {
		boolean check = false;
		if(translated) {
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().catalog_product_Labels(label)).click();
			Thread.sleep(3000);
			General.action().jsScroll(Hybris_dashboard.exts().products_Attributes_ProcessorModel);
			Thread.sleep(500);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().globe_Icon_ProcessorModel).click();
			Thread.sleep(1000);
			String str1 = BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().products_Attributes_PM_Lang_English).getAttribute("value");
		    Thread.sleep(2000);
		    System.out.println("Attribute value in English is: " +str1);
		    String str2 = BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().products_Attributes_PM_Lang_German).getAttribute("value");
		    Thread.sleep(2000);
		    System.out.println("Attribute value in German is: " +str2);
		    if(str1!=str2)
		    {
		    	System.out.println("All contents are Translated successfully from Processor Model");
		    	check=true;
		    }
		}
		if(notTrans) {
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().catalog_product_Labels(label)).click();
			Thread.sleep(3000);
			General.action().jsScroll(Hybris_dashboard.exts().products_Attributes_ProcessorModel);
			Thread.sleep(500);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().globe_Icon_ProcessorModel).click();
			Thread.sleep(500);
			String str1 = BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().products_Attributes_PM_Lang_English).getAttribute("value");
			System.out.println("ProcessorModel value in English is: " +str1);
		    Thread.sleep(2000);
		 // Check whether input field is blank
		    if(str1.isEmpty())
		    {
		       System.out.println("Product features are not Translated");
		       check=true;
		    }
		}
		
	    return check;
	}	
	
	
	public void create_Submission_From_ProductCockpit(String ProjectName,String SubmissionName,String LanguageName,boolean submit_onlyNew,boolean trans_prod_feature) throws Exception {
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().productCockpit_Icon_Products).click();
		Thread.sleep(2000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().productCockpit_Titles(Hybris_Common_Properties_Cred.PC_title_ApparelProductCatalog)).click();
		Thread.sleep(500);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().productCockpit_Titles(Hybris_Common_Properties_Cred.PC_subtitle_Staged)).click();
		Thread.sleep(3000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().productCockpit_SelectProduct(Hybris_Common_Properties_Cred.PC_product_Name)).click();
		Thread.sleep(500);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().productCockpit_TranslateIcon).click();
		Thread.sleep(500);
		//Select Project
		common.action().SelectProject_CreateJob(ProjectName);
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Job_Nxt_Btn).click();
				Thread.sleep(500);
				Verify.action().verifyElementPresent(Hybris_dashboard.exts().gl_Job_ID,5);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Job_ID).clear();
				Thread.sleep(500);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Job_ID).sendKeys(SubmissionName);
				Thread.sleep(2000);
				//Select Language
				Verify.action().verifyElementPresent(Hybris_dashboard.exts().select_Language(LanguageName),5);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_Language(LanguageName)).click();
				Thread.sleep(1000);
				
				//Select check box for Submit Only new changed content and translate prod feature
				if(submit_onlyNew){
					if(BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().checkbox_submitonly_newcontent).isSelected()==true)
					{
						BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().checkbox_submitonly_newcontent_uncheck).click();
						Thread.sleep(2000);
						System.out.println("--------->Submit Only new changed content");
					}
				}
				if(trans_prod_feature){
					if(BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().checkbox_translate_prod_feature).isSelected()==true)
					{
						BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().checkbox_translate_prod_feature_uncheck).click();
						Thread.sleep(2000);
						System.out.println("--------->translate prod feature");
					}
				}
				
				Thread.sleep(20000);
						
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Job_Nxt_Btn).click();
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Job_Create_Btn).click();
				Thread.sleep(500);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Job_Done_Btn).click();
				Thread.sleep(500);
	}
	
	public void switchBackTo_AdministrationCockpit() throws Exception {
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().clickON_Cockpit_Dropdown(Hybris_Common_Properties_Cred.subtitle_Product_Cockpit)).click();
		Thread.sleep(500);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_Cockpit_From_dropDown(Hybris_Common_Properties_Cred.subtitle_Administration_Cockpit)).click();
		Thread.sleep(1000);	
	}

	//**********************TRANSLATION WIZARD***************************//
	
	public void click_On_TranlationIcon() throws Exception {
		//Select search result 1st row
  		Verify.action().verifyElementPresent(Hybris_dashboard.exts().select_search_firstrow,5);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_search_firstrow).click();
		Thread.sleep(1000);
		//Translate Icon
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().translate_icon).click();
		Thread.sleep(1000);
	}
	
	public String editDueDate() throws Exception {
		SimpleDateFormat df = new SimpleDateFormat("dd");
		Date dt = new Date();
		Calendar cl = Calendar.getInstance();
		cl.setTime(dt);;
		cl.add(Calendar.DAY_OF_YEAR, 2);
		dt=cl.getTime();
		String str = df.format(dt);
		    System.out.println("the date today is " + str);

		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_Due_Date(str)).click();
		Thread.sleep(1000);
		String editedDate = BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().job_Config_DueDate_input).getAttribute("value");
		return editedDate;
	}
	
	//***************************ZIP FILE****************************//
	public String getZipFile_Name() throws FileNotFoundException{
		String name="";
		String home = System.getProperty("user.home");
		File path = new File(home+"/Downloads/"); 
		File[] listFiles = path.listFiles(new FileFilter() {

		    @Override
		    public boolean accept(File file) {

		        String name = file.getName();
		        return name.startsWith("source_");

		    }

		});
		if (listFiles.length == 0) {

		    // File does not exit
			System.out.println("File not exist");

		} else {

			File file = listFiles[0];
			name=file.getName();
			  
		}
		return name;

	}
	
	//************************CSV FILE*************************//
	
	public String getFile_Name(String fname) throws Exception {
		String absPath="";
		String home = System.getProperty("user.home");
		File path = new File(home+"/Downloads/"); 
		File[] listFiles = path.listFiles(new FileFilter() {

		    @Override
		    public boolean accept(File file) {

		        String name = file.getName();
		        return name.startsWith(fname);

		    }

		});
		if (listFiles.length == 0) {

		    // File does not exit
			System.out.println("File not exist");

		} else {

			File file = listFiles[0];
			absPath = file.getAbsolutePath();
			  
		}
		return absPath;
	}
	
	public boolean read_Data_From_CSV_File(String fName , int count) throws Exception {
		
		boolean result = false;
		String name = getFile_Name(fName);
		CSVReader reader = new CSVReader(new FileReader(name));

		  List<String[]> list=reader.readAll();
		  System.out.println("Total rows which we have is "+list.size());
		  
		  if((list.size()-1)==count) {
			  
			  result = true;
			  System.out.println("All Jobs are exported in CSV file");
		  }
		  else {
			  System.out.println("All Jobs are not exported correctly");
		  }
		            
		 // create Iterator reference
		  Iterator<String[]>iterator= list.iterator();
		    
		 // Iterate all values 
		 while(iterator.hasNext()){
		     
		 String[] str=iterator.next();
		   
		 System.out.print(" Values are ");

		 for(int i=0;i<str.length;i++)
		{

		   System.out.print(" "+str[i]);

		}
		   System.out.println("   ");
		 }
		 Thread.sleep(1000);
		 return result;
	}
}
