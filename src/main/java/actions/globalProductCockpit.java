package actions;

import java.util.ArrayList;
import java.util.List;

import org.gs4tr.qa.utility.BrowserFactory;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import locators.Hybris_GlobalProductCockpit;
import locators.Hybris_dashboard;

public class globalProductCockpit{
	private static globalProductCockpit add_objects;
	
	
	/**
	 * Method used to self-instantiate the class.  Will make sure one object,
	 * and one object only is created in memory for this class.  The purpose is
	 * to be able to call this object dynamically from any JAVA class that 
	 * includes this as an import.
	 * 
	 * 
	 * @return Returns the object instantiated from the class.
	 */
	public static synchronized globalProductCockpit action(){
		try{
			if(add_objects.equals(null))
			{
				add_objects = new globalProductCockpit();
			}
		}
		catch(Exception NOSYSTEM){
			add_objects = new globalProductCockpit();
		}
		return add_objects;
	}
	
	
	public void open_New_Instance_globalProduct_Cockpit(String URL) throws Exception {
		((JavascriptExecutor) BrowserFactory.driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(BrowserFactory.driver.getWindowHandles());
		BrowserFactory.driver.switchTo().window(tabs.get(1));
		Thread.sleep(3000);
		BrowserFactory.SystemEngine();
		BrowserFactory.driver.get(URL);
		Thread.sleep(10000);
	}
	
	public void globalProduct_Cockpit_login() {
		try {
			
			Thread.sleep(3000);
			Verify.action().verifyElementPresent(Hybris_dashboard.exts().Login_email, 5);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Login_email).sendKeys(GlobalProductCockpit_Common_Prop_Cred.gpc_UserName);
			Thread.sleep(1000);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Login_password).sendKeys(GlobalProductCockpit_Common_Prop_Cred.gpc_Password);
			Thread.sleep(1000);
			BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().login_Btn).click();
			Thread.sleep(6000);
			
			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	public void searchProduct(String productName) throws Exception {
		BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().searchProduct_Input).sendKeys(productName);
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().searchProduct_Icon).click();
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().searchProduct_Icon).click();
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().clickOn_Product(productName)).click();
		Thread.sleep(3000);
	}
	
	public void search_Created_Job(String menu, String jobName) throws Exception {
		BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().selectMenu(menu)).click();
		Thread.sleep(1000);
		
		BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().search_Job_Input).sendKeys(jobName);
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().search_Job_Icon).click();
		Thread.sleep(3000);
	}
	
	
	public void create_Job_From_Product(boolean singleProduct,int num,String projectName,String profile,String SubmissionName,boolean lang,String TargetLanguageName,
			boolean submit_onlyNew, boolean translate_Product, int items)
			throws Exception {
		
		//Click on Translate Icon
		if(singleProduct) {
		Verify.action().verifyElementPresent(Hybris_GlobalProductCockpit.exts().translate_Icon,5);
		BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().translate_Icon).click();
		Thread.sleep(2000);
		}else {
			List<WebElement> els = BrowserFactory.SystemEngine().findElements(Hybris_GlobalProductCockpit.exts().selectMulti_Products);
  			Thread.sleep(1000);
  			Actions act=new Actions(BrowserFactory.driver);
  			for(int i=0;i<num;i++)
  			{
  				act.keyDown(Keys.CONTROL).click(els.get(i)).build().perform();
  				Thread.sleep(500);
  			}
  			Thread.sleep(1000);
  			BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().translate_Icon).click();
  			Thread.sleep(1000);
		}
		//Select Project
		globalProductCockpit.action().SelectProject_CreateJob(projectName);
		//Select Profile
		globalProductCockpit.action().select_Profile_CreateJob(profile);
		Thread.sleep(3000);
		//Enter JOB NAME
		BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().jobName_Input).clear();
		Thread.sleep(2000);
		/*WebElement ele = BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().jobName_Input);
		Thread.sleep(1000);
		 JavascriptExecutor jse = (JavascriptExecutor)BrowserFactory.driver;
		 jse.executeScript("arguments[0].value='"+SubmissionName+"';", ele);*/
		BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().jobName_Input).sendKeys(SubmissionName);
		Thread.sleep(2000);
		 Thread.sleep(2000);
		/* BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().jobName_Label).click();
		 Thread.sleep(2000);*/
		//Select Language
		if(lang) 
			{
				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().selectTarget_Lang(TargetLanguageName)).click();
				Thread.sleep(2000);
				System.out.println(TargetLanguageName+"--------->Target Language Selected");
			}
		else {
			//Select Multiple Language
			BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().selectTarget_Lang(GlobalProductCockpit_Common_Prop_Cred.selectALL)).click();
			Thread.sleep(2000);
		}
		//Select check box for Submit Only new changed content and translate prod feature
		if(submit_onlyNew)
			{
				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().checkbox_submitonly_newcontent).click();
				Thread.sleep(2000);
				System.out.println("--------->Submit Only new changed content");
			}
		if(translate_Product)
		{
			BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().checkbox_translate_prod_feature).click();
			Thread.sleep(2000);
			System.out.println("--------->Translate Product Features");
		}
			
		BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().click_On_Button(GlobalProductCockpit_Common_Prop_Cred.translate_Btn)).click();
		Thread.sleep(3000);
		Verify.action().verifyTextPresent(items+" item(s) queued for translation.", 5);
		System.out.println("Queued Job Values---------->"+items);
		Thread.sleep(2000);
		BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().click_On_Button(GlobalProductCockpit_Common_Prop_Cred.close_Btn)).click();
		Thread.sleep(3000);
	} 
	
	
	public String create_Job_MultipleProd_From_Product(boolean singleProduct,int num,String projectName,String profile,boolean lang,String TargetLanguageName,
			boolean submit_onlyNew, boolean translate_Product, int items,String menu)
			throws Exception {
		
		//Click on Translate Icon
		if(singleProduct) {
		Verify.action().verifyElementPresent(Hybris_GlobalProductCockpit.exts().translate_Icon,5);
		BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().translate_Icon).click();
		Thread.sleep(2000);
		}else {
			List<WebElement> els = BrowserFactory.SystemEngine().findElements(Hybris_GlobalProductCockpit.exts().selectMulti_Products);
  			Thread.sleep(1000);
  			Actions act=new Actions(BrowserFactory.driver);
  			for(int i=0;i<num;i++)
  			{
  				act.keyDown(Keys.CONTROL).click(els.get(i)).build().perform();
  				Thread.sleep(500);
  			}
  			Thread.sleep(1000);
  			BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().translate_Icon).click();
  			Thread.sleep(1000);
		}
		//Select Project
		globalProductCockpit.action().SelectProject_CreateJob(projectName);
		//Select Profile
		globalProductCockpit.action().select_Profile_CreateJob(profile);
		Thread.sleep(3000);
		//Enter JOB NAME
	/*	BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().jobName_Input).clear();
		Thread.sleep(2000);
		WebElement ele = BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().jobName_Input);
		Thread.sleep(1000);*/
		
		String JobName = BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().jobName_Input).getAttribute("value");
		System.out.println("Job Name to Search---->"+JobName);
		
/*		 JavascriptExecutor jse = (JavascriptExecutor)BrowserFactory.driver;
		 jse.executeScript("arguments[0].value='"+SubmissionName+"';", ele);
		 BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().jobName_Input).sendKeys(SubmissionName);
		Thread.sleep(2000);*/
		 Thread.sleep(2000);
		 BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().jobName_Label).click();
		 Thread.sleep(2000);
		//Select Language
		if(lang) 
			{
				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().selectTarget_Lang(TargetLanguageName)).click();
				Thread.sleep(2000);
				System.out.println(TargetLanguageName+"--------->Target Language Selected");
			}
		else {
			//Select Multiple Language
			BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().selectTarget_Lang(GlobalProductCockpit_Common_Prop_Cred.selectALL)).click();
			Thread.sleep(2000);
		}
		//Select check box for Submit Only new changed content and translate prod feature
		if(submit_onlyNew)
			{
				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().checkbox_submitonly_newcontent).click();
				Thread.sleep(2000);
				System.out.println("--------->Submit Only new changed content");
			}
		if(translate_Product)
		{
			BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().checkbox_translate_prod_feature).click();
			Thread.sleep(2000);
			System.out.println("--------->Translate Product Features");
		}
			
		BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().click_On_Button(GlobalProductCockpit_Common_Prop_Cred.translate_Btn)).click();
		Thread.sleep(3000);
		Verify.action().verifyTextPresent(items+" item(s) queued for translation.", 5);
		System.out.println("Queued Job Values---------->"+items);
		Thread.sleep(2000);
		BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().click_On_Button(GlobalProductCockpit_Common_Prop_Cred.close_Btn)).click();
		Thread.sleep(3000);
		
		BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().selectMenu(menu)).click();
		Thread.sleep(3000);
		
		BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().search_Job_Input).sendKeys(JobName);
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().search_Job_Icon).click();
		Thread.sleep(3000);
		return JobName;
	} 
	
	
	
	public void create_Job_SiteRoot_GlobalCMS_Product(boolean singleProduct,int num,String projectName,String profile,boolean lang,String TargetLanguageName,
			boolean submit_onlyNew, boolean translate_Product, int items,String menu)
			throws Exception {
	
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().translate_Icon).click();
		//Select Project
		globalProductCockpit.action().SelectProject_CreateJob(projectName);
		//Select Profile
		globalProductCockpit.action().select_Profile_CreateJob(profile);
		Thread.sleep(3000);
		String JobName = BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().SiteRoot_JobName_Input).getAttribute("value");
		System.out.println("Job Name to Search---->"+JobName);

		 Thread.sleep(2000);
		 BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().jobName_Label).click();
		 Thread.sleep(2000);
		//Select Language
		if(lang) 
			{
				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().selectTarget_Lang(TargetLanguageName)).click();
				Thread.sleep(2000);
				System.out.println(TargetLanguageName+"--------->Target Language Selected");
			}
		else {
			//Select Multiple Language
			BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().selectTarget_Lang(GlobalProductCockpit_Common_Prop_Cred.selectALL)).click();
			Thread.sleep(2000);
		}
		//Select check box for Submit Only new changed content and translate prod feature
		if(submit_onlyNew)
			{
				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().checkbox_submitonly_newcontent).click();
				Thread.sleep(2000);
				System.out.println("--------->Submit Only new changed content");
			}
		if(translate_Product)
		{
			BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().checkbox_translate_prod_feature).click();
			Thread.sleep(2000);
			System.out.println("--------->Translate Product Features");
		}
			
		BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().click_On_Button(GlobalProductCockpit_Common_Prop_Cred.translate_Btn)).click();
		Thread.sleep(3000);
		Verify.action().verifyTextPresent(items+" item(s) queued for translation.", 5);
		System.out.println("Queued Job Values---------->"+items);
		Thread.sleep(2000);
		BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().click_On_Button(GlobalProductCockpit_Common_Prop_Cred.close_Btn)).click();
		Thread.sleep(3000);
		
		BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().SiteRoot_selectMenu(menu)).click();
		Thread.sleep(3000);
		
		BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().globalCMS_search_Job_Input).sendKeys(JobName);
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().globalCMS_search_Job_Icon).click();
		Thread.sleep(3000);
	} 
	
	
	public String create_Job_SiteRoot_ApparelUKSite(String RootName,boolean singleProduct,int num,String projectName,String profile,boolean lang,String TargetLanguageName,
			boolean submit_onlyNew, boolean translate_Product, int items,String menu)
			throws Exception {
	
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().translateIcon_SiteRoot_Name(RootName)).click();
		//Select Project
		globalProductCockpit.action().SelectProject_CreateJob(projectName);
		//Select Profile
		globalProductCockpit.action().select_Profile_CreateJob(profile);
		Thread.sleep(3000);
		String JobName = BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().SiteRoot_JobName_Input).getAttribute("value");
		System.out.println("Job Name to Search---->"+JobName);

		 Thread.sleep(2000);
		 BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().jobName_Label).click();
		 Thread.sleep(2000);
		//Select Language
		if(lang) 
			{
				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().selectTarget_Lang(TargetLanguageName)).click();
				Thread.sleep(2000);
				System.out.println(TargetLanguageName+"--------->Target Language Selected");
			}
		else {
			//Select Multiple Language
			BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().selectTarget_Lang(GlobalProductCockpit_Common_Prop_Cred.selectALL)).click();
			Thread.sleep(2000);
		}
		//Select check box for Submit Only new changed content and translate prod feature
		if(submit_onlyNew)
			{
				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().checkbox_submitonly_newcontent).click();
				Thread.sleep(2000);
				System.out.println("--------->Submit Only new changed content");
			}
		if(translate_Product)
		{
			BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().checkbox_translate_prod_feature).click();
			Thread.sleep(2000);
			System.out.println("--------->Translate Product Features");
		}
			
		BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().click_On_Button(GlobalProductCockpit_Common_Prop_Cred.translate_Btn)).click();
		Thread.sleep(3000);
		Verify.action().verifyTextPresent(items+" item(s) queued for translation.", 5);
		System.out.println("Queued Job Values---------->"+items);
		Thread.sleep(2000);
		BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().click_On_Button(GlobalProductCockpit_Common_Prop_Cred.close_Btn)).click();
		Thread.sleep(3000);
		
		BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().SiteRoot_selectMenu(menu)).click();
		Thread.sleep(3000);
		
		BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().globalCMS_search_Job_Input).sendKeys(JobName);
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().globalCMS_search_Job_Icon).click();
		Thread.sleep(3000);
		
		return JobName;
	} 
	
	public void SelectProject_CreateJob(String ProjectName) throws Exception {
		
		if(Verify.action().verifyElementPresent(Hybris_GlobalProductCockpit.exts().select_Project_Page, 1)){
			//Select Project
			BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().select_Project(ProjectName)).click();
			Thread.sleep(1000);
			BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().click_On_Button(GlobalProductCockpit_Common_Prop_Cred.next_Btn)).click();
			Thread.sleep(1000);
		}
		}
	
	public void select_Profile_CreateJob(String profileName) throws Exception {
		
		if(Verify.action().verifyElementPresent(Hybris_GlobalProductCockpit.exts().select_Profile_Page, 1)){
			//Select Project
			BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().select_Profile(profileName)).click();
			Thread.sleep(1000);
			BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().click_On_Button(GlobalProductCockpit_Common_Prop_Cred.next_Btn)).click();
			Thread.sleep(1000);
		}
		}
	
	public boolean verify_Job_Status(String status) throws Exception {
		boolean check = false;
		if(Verify.action().verifyElementPresent(Hybris_GlobalProductCockpit.exts().verify_Job_Status(status),  5));
		{
			System.out.println("Status of job is: "+ status);
			Thread.sleep(500);
			check = true;
		}
		return check;
	}
	
	public void Close_ActiveTab(String originalHandle) throws Exception {

		System.out.println("\nIn vendor.java -  switchToPopupWindow()\n");
		System.out.println("originalHandle--->" + originalHandle);

		BrowserFactory.SystemEngine();
		for (String handle : BrowserFactory.driver.getWindowHandles()) {
			if (!handle.equals(originalHandle)) {
				System.out.println("handle--->" + handle);
				BrowserFactory.SystemEngine();
				BrowserFactory.driver.switchTo().window(handle);
				BrowserFactory.SystemEngine();
				BrowserFactory.driver.close();

			}
		}
		BrowserFactory.SystemEngine();
		BrowserFactory.driver.switchTo().window(originalHandle);
		System.out.println("\nEOM - Close_ActiveTab().\n");
		;
		Thread.sleep(3000);
	}
	
	
	//***************************************GLOBAL CMS***************************//
	public void globalCMS_Cockpit_login() {
		try {
			
			Thread.sleep(3000);
			Verify.action().verifyElementPresent(Hybris_dashboard.exts().Login_email, 5);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Login_email).sendKeys(GlobalProductCockpit_Common_Prop_Cred.gpc_cms_UserName);
			Thread.sleep(1000);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Login_password).sendKeys(GlobalProductCockpit_Common_Prop_Cred.gpc_cms_Password);
			Thread.sleep(1000);
			BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().login_Btn).click();
			Thread.sleep(6000);
			
			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	public void searchCMSProduct(String menu,String Site,String Catalog,String productName) throws Exception {
		BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().selectMenu(menu)).click();
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().selectCMS_SideMenu(Site)).click();
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().selectCMS_SideMenu(Catalog)).click();
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().searchProduct_Input).sendKeys(productName);
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().searchProduct_Icon).click();
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().searchProduct_Icon).click();
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().clickOn_Product(productName)).click();
		Thread.sleep(3000);
	}
	
	public void searchCMS_Created_Job(String menu, String jobName) throws Exception {
		BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().selectCMS_SideMenu(menu)).click();
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().search_Job_Input).sendKeys(jobName);
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().search_Job_Icon).click();
		Thread.sleep(3000);
	}
}
