package actions;

import java.util.ArrayList;

import org.gs4tr.qa.utility.BrowserFactory;
import org.openqa.selenium.JavascriptExecutor;

import locators.Hybris_dashboard;
import locators.Hybris_smartedit;

public class smartedit{
	private static smartedit add_objects;
	
	
	/**
	 * Method used to self-instantiate the class.  Will make sure one object,
	 * and one object only is created in memory for this class.  The purpose is
	 * to be able to call this object dynamically from any JAVA class that 
	 * includes this as an import.
	 * 
	 * 
	 * @return Returns the object instantiated from the class.
	 */
	public static synchronized smartedit action(){
		try{
			if(add_objects.equals(null))
			{
				add_objects = new smartedit();
			}
		}
		catch(Exception NOSYSTEM){
			add_objects = new smartedit();
		}
		return add_objects;
	}
	
	
	public void open_New_Instance_SmartEdit(String URL) throws Exception {
		((JavascriptExecutor) BrowserFactory.driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(BrowserFactory.driver.getWindowHandles());
		BrowserFactory.driver.switchTo().window(tabs.get(1));
		Thread.sleep(3000);
		BrowserFactory.SystemEngine();
		BrowserFactory.driver.get(URL);
		Thread.sleep(10000);
	}
	
	public void SmartEdit_login() {
		try {
			
			Thread.sleep(3000);
			Verify.action().verifyElementPresent(Hybris_dashboard.exts().Login_email, 5);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Login_email).sendKeys(SE_Common_Prop_Cred.SmartEdit_UserName);
			Thread.sleep(1000);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Login_password).sendKeys(SE_Common_Prop_Cred.SmartEdit_Password);
			Thread.sleep(1000);
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Login_button).click();
			Thread.sleep(6000);
			
			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	
	public void select_yourSite(String SiteName) throws Exception {
		BrowserFactory.SystemEngine().findElement(Hybris_smartedit.exts().select_yoursite_dropdown).click();
		Thread.sleep(2000);
		BrowserFactory.SystemEngine().findElement(Hybris_smartedit.exts().select_yoursiteX(SiteName)).click();
		Thread.sleep(1000);
	}
	
	
	public void create_Job_SmartEdit(String catalogtype,String pagetype,String pagename,String SubmissionName,boolean lang,String SourceLanguage,String TargetLanguageName,boolean submit_onlyNew) throws Exception {
		
		//Select Catalog
		BrowserFactory.SystemEngine().findElement(Hybris_smartedit.exts().select_apparel_uk_content_Catalog(catalogtype,pagetype)).click();
		
		//Select Page
		BrowserFactory.SystemEngine().findElement(Hybris_smartedit.exts().select_staged_page(pagename)).click();
		Thread.sleep(5000);
		
		//Select Basic Edit if not selected
		smartedit.action().select_Basic_EditMode(SE_Common_Prop_Cred.basic_Edit_Mode);
		
		//Click on Translate Icon
		Verify.action().verifyElementPresent(Hybris_smartedit.exts().translate_Icon,5);
		BrowserFactory.SystemEngine().findElement(Hybris_smartedit.exts().translate_Icon).click();
//		Thread.sleep(3000);
		//Select profile if Present
		smartedit.action().select_Profile_CreateJob(SE_Common_Prop_Cred.profileName_Default);
		Thread.sleep(2000);
		
		Verify.action().verifyElementPresent(Hybris_smartedit.exts().gl_Job_Name,5);
		BrowserFactory.SystemEngine().findElement(Hybris_smartedit.exts().gl_Job_Name).click();
		BrowserFactory.SystemEngine().findElement(Hybris_smartedit.exts().gl_Job_Name).clear();
		Thread.sleep(500);
		BrowserFactory.SystemEngine().findElement(Hybris_smartedit.exts().gl_Job_Name).sendKeys(SubmissionName);
		Thread.sleep(2000);
		//Select Language
		if(lang) {
			if(Verify.action().verifyElementNotPresent(Hybris_smartedit.exts().gl_job_select_TargetLanguage_checkbox_checked(TargetLanguageName),5));
			{
				BrowserFactory.SystemEngine().findElement(Hybris_smartedit.exts().gl_job_select_TargetLanguage_checkbox(TargetLanguageName)).click();
				Thread.sleep(2000);
				System.out.println(TargetLanguageName+"--------->Target Language Selected");
			}
		}
		else {
			//Select Multiple Language
			Verify.action().verifyElementPresent(Hybris_smartedit.exts().gl_job_select_all_TargetLanguage_checkbox,5);
			BrowserFactory.SystemEngine().findElement(Hybris_smartedit.exts().gl_job_select_all_TargetLanguage_checkbox).click();
			Thread.sleep(1000);
		}
		//Select check box for Submit Only new changed content and translate prod feature
		if(submit_onlyNew){
			if(Verify.action().verifyElementNotPresent(Hybris_smartedit.exts().gl_job_SubmitNewChanged_checkbox_checked,5));
			{
				BrowserFactory.SystemEngine().findElement(Hybris_smartedit.exts().gl_job_SubmitNewChanged_checkbox).click();
				Thread.sleep(2000);
				System.out.println("--------->Submit Only new changed content");
			}
		}
			
		BrowserFactory.SystemEngine().findElement(Hybris_smartedit.exts().NextButton).click();
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_smartedit.exts().NextButton).click();
		Thread.sleep(5000);
		
		Verify.action().verifyElementPresent(Hybris_smartedit.exts().Job_Created, 5);
		
		BrowserFactory.SystemEngine().findElement(Hybris_smartedit.exts().CloseButton).click();
		Thread.sleep(2000);
	
	} 
	
	public void select_Basic_EditMode(String edit) throws Exception {
		Thread.sleep(8000);
		if(Verify.action().verifyElementPresent(Hybris_smartedit.exts().preview_Btn, 5)){
			//Select Basic Edit Mode
			BrowserFactory.SystemEngine().findElement(Hybris_smartedit.exts().preview_Btn).click();
			Thread.sleep(2000);
			BrowserFactory.SystemEngine().findElement(Hybris_smartedit.exts().selectEditMode(edit)).click();
			Thread.sleep(2000);		
		}
		}
	
	public void select_Profile_CreateJob(String profile) throws Exception {
		
		if(Verify.action().verifyElementPresent(Hybris_smartedit.exts().gl_Job_selectProject_label, 5)){
			Thread.sleep(2000);
			//Select Profile
			Verify.action().verifyElementPresent(Hybris_smartedit.exts().select_Profile(profile),5);
			BrowserFactory.SystemEngine().findElement(Hybris_smartedit.exts().select_Profile(profile)).click();
			Thread.sleep(2000);
			BrowserFactory.SystemEngine().findElement(Hybris_smartedit.exts().NextButton).click();
			Thread.sleep(1000);
		}
		}
	
	
	/**
	 * Close_ActiveTab
	 * 
	 * @author Proteek
	 * 
	 * @param originalHandle switch back to original Handle window
	 * @throws Exception used by isPresent, in case element is not present.
	 */

	public void Close_ActiveTab(String originalHandle) throws Exception {

		System.out.println("\nIn vendor.java -  switchToPopupWindow()\n");
		System.out.println("originalHandle--->" + originalHandle);

		BrowserFactory.SystemEngine();
		for (String handle : BrowserFactory.driver.getWindowHandles()) {
			if (!handle.equals(originalHandle)) {
				System.out.println("handle--->" + handle);
				BrowserFactory.SystemEngine();
				BrowserFactory.driver.switchTo().window(handle);
				BrowserFactory.SystemEngine();
				BrowserFactory.driver.close();

			}
		}
		BrowserFactory.SystemEngine();
		BrowserFactory.driver.switchTo().window(originalHandle);
		System.out.println("\nEOM - Close_ActiveTab().\n");
		;

	}
}
