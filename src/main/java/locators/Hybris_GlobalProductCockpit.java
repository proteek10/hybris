package locators;

public class Hybris_GlobalProductCockpit {
	
	private static Hybris_GlobalProductCockpit gp;
	
	public static synchronized Hybris_GlobalProductCockpit exts(){
		try{
			if(gp.equals(null))
			{
				gp = new Hybris_GlobalProductCockpit();
			}
		}
		catch(Exception NOSYSTEM){
			gp = new Hybris_GlobalProductCockpit();
		}
		return gp;
	}

	//Login
	public final String Login_email ="//input[contains(@name,'username')]";
	public final String Login_password = "//input[contains(@name,'password')]";
	public final String Login_button = "//button[contains(@type,'submit')]";
	public final String Login_cookies_Ok_button ="//div[@class='cookies jsCookies']//button[text()='Ok']";
	//Search Product
	public final String searchProduct_Input = "(//tr[@valign='middle']//input[@class='z-textbox'])[1]";
	public final String searchPage_Input = "//tr[@valign='middle']//input[@class='z-textbox']";
	public final String searchProduct_Icon = "//tr[@valign='middle']//img[contains(@src,'BUTTON_search.png')]";
	
	//select Product
	public  String clickOn_Product (String prodName){
		return "//img//..//..//span[contains(text(),'"+prodName+"')]";
	}
	public final String selectMulti_Products = "//div[@class='gridImageDiv']";
	
	//Translate Icon
	public final String translate_Icon = "//img[contains(@src,'globallink.png')]";
	public final String translate_Icon_Disabled = "//img[contains(@src,'globallink_inactive.png')]";
	public final String translateIcon_SiteRoot = "//div[contains(@class,'tree')]//span[contains(text(),'My Account')]//..//..//..//..//..//..//..//img[contains(@src,'globallink.png')]";
	
	public String translateIcon_SiteRoot_Name(String RootName) {
		 return "//div[contains(@class,'tree')]//span[contains(text(),'"+RootName+"')]//..//..//..//..//..//..//..//img[contains(@src,'globallink.png')]";
	 }
	
	//Select Project
	public final String select_Project_Page = "//span[contains(text(),'Select a Project: ')]";
	public String select_Project(String ProjectName) {
		 return "//div[contains(text(),'"+ProjectName+"')]";
	 }
	
	//Select Profile
	public final String select_Profile_Page = "//span[contains(text(),'Select a Attribute Profile: ')]";
	public String select_Profile(String profile) {
		 return "//div[contains(text(),'"+profile+"')]";
	 }
	
	//Create Submission Page
	public final String jobName_Input = "//div[contains(@class,'wizardContentFrame')]//table[2]//input[contains(@class,'textbox')]";//"//span[contains(text(),'Job Name:')]//..//..//input";
	public final String SiteRoot_JobName_Input = "//div[contains(@class,'wizardContentFrame')]//table[1]//input[contains(@class,'textbox')]";
	public final String jobName_Label = "//div[contains(text(),'GlobalLink Submission Wizard')]";//"//span[contains(text(),'Job Name:')]";
	
	//Select Language
	public String selectTarget_Lang(String lang) {
		return "//div[contains(text(),'"+lang+"')]//input[@type='checkbox']";
	}
	//Select checkbox submit only new and changed content
	public final String checkbox_submitonly_newcontent = "//label[contains(text(),'Submit only new and changed content')]";
	//Select checkbox submit only new and changed content
	public final String checkbox_translate_prod_feature = "//label[contains(text(),'Translate product features')]";
	
	//Create Submission Page Buttons etc..
	public  String click_On_Button(String name){
		return "//td[contains(text(),'"+name+"')]";
	}
	
	//Select Menu's
	public  String selectMenu(String name){
		return "//table[contains(@class,'plainBtn')]//td[contains(text(),'"+name+"')]";
	}
	
	public  String SiteRoot_selectMenu(String name){
		return "//div[contains(@id,'qk')]//table[contains(@class,'plainBtn')]//td[contains(text(),'"+name+"')]";
	}
	//Search Created Job
	public final String search_Job_Input = "(//tr[@valign='middle']//input[@class='z-textbox'])[2]";
	public final String globalCMS_search_Job_Input = "//tr[@valign='middle']//input[@class='z-textbox']";
	public final String search_Job_Icon = "(//tr[@valign='middle']//img[contains(@src,'BUTTON_search.png')])[2]";
	public final String globalCMS_search_Job_Icon = "//tr[@valign='middle']//img[contains(@src,'BUTTON_search.png')]";
	public final String GlobalLink_Submission_Div = "//div[contains(@class,'columnIndex')]//span";
	public final String GlobalLink_activeTab = "//table[contains(@class,'plainBtn z-button z-button-disd')]//td[contains(text(),'GlobalLink')]";
	public final String Select_SiteRoot = "//div[contains(@class,'navigationSectionContainer')]//span[contains(text(),'SiteRootNode')]";
	public final String Open_Select_SiteRoot = "//div[contains(@class,'navigationSectionContainer')]//span[contains(@class,'dottree-root-close')]";
	public final String Select_Tree_Apparel_UK_Site = "//div[contains(@class,'navigationSectionContainer')]//span[contains(text(),'Apparel UK Site')]";
	public final String Select_Open_Apparel_UK_Site = "//div[contains(@class,'dottree')]//span[contains(text(),'Apparel UK Site')]/..//..//..//..//..//span[contains(@class,'dottree-last-close')]";
	public final String Select_Open_Cateogories = "//div[contains(@class,'dottree')]//span[contains(text(),'Categories')]/..//..//..//..//..//span[contains(@class,'dottree-tee-close')]";
	public final String Select_MyAccount = "//div[contains(@class,'tree')]//span[contains(text(),'My Account')]";
	
	public final String Get_CMS_Submission_Name = "//div[contains(@class,'mainContentAboveStatus')]//div[contains(@class,'columnIndex')][6]//div[contains(@class,'List')][1]"; 
	
	public String Select_SiteRootNode(String nodename) {
		return "//div[contains(@class,'tree')]//span[contains(text(),'"+nodename+"')]";
	}
	
	
	//Verify JOB Status under Jobs
	public String verify_Job_Status(String status) {
		return "//div[@class='listColumnContentFixed']//span[contains(text(),'"+status+"')]";
	}
	
	//Catalog
	public String selectProduct_Type_From_Catalog(String product) {
		return "//div[@class='catalog_section_container']//div[contains(text(),'"+product+"')]";
	}
	
	//Login Button
	public final String login_Btn = "//td[contains(text(),'Login')]";
	
	//Advanced Search Icon
	public final String globalLink_AdvancedSearch_Icon = "(//a[@title='Advanced search'])[2]";
	public String advancedSearch_Fields(String name) {
		return "//div[@class='advancedSearchGroup-cnt']//span[contains(text(),'"+name+"')]";
	}
	
	//Adance Search CMS
	public final String globalLink_CMS_Search_Icon = "//table[contains(@class,'searchbtn')]";
	
	public String CMSSearch_Fields(String name) {
		return "//div[@class='listViewMainDiv']//span[contains(text(),'"+name+"')]";
	}
	
	//table[contains(@class,'hiddenLabel')]//span[contains(text(),'')]
	public  String selectCMS_SideMenu(String name){
		return "//table[contains(@class,'hiddenLabel')]//span[contains(text(),'"+name+"')]";
	}
	
	public  String selectCMS_SideMenu_Siteroot(String name){
		return "//div[contains(@id,'j!cave')]//table[contains(@class,'hiddenLabel')]//span[contains(text(),'"+name+"')]";
	}
	
	public final String siteRootNode_Expand_Arrow = "//span[contains(text(),'SiteRootNode')]//..//..//..//..//..//span[contains(@class,'dottree-root-close')]";
	public final String apparelUKSite_Expand_Plus_Icon = "//span[contains(text(),'Apparel UK Site')]//..//..//..//..//..//span[contains(@class,'dottree-last-close')]";
	public final String categories_Expand_Plus_Icon = "//span[contains(text(),'Categories')]//..//..//..//..//..//span[@z.type='Tcop']";
	public final String siteRootNode_ApparelUK_MyAccount_SubMenu = "//span[contains(text(),'My Account')]";
	public final String siteRootNode_ApparelUK_Categories_SubMenu = "//span[contains(text(),'Categories')]";
	public final String siteRootNode_ApparelUK_TranslateIcon = "//tr[contains(@class,'row-seld')]//img[contains(@src,'globallink.png')]";
	public final String categories_SubMenu_AllBrands = "//table[contains(@class,'navigationNodesDropSlotVbox')]//span[contains(text(),'All Brands')]";
	
	
	//Item Name
	public String verifyItemName(String name) {
		return "//div[@class='inspectorlistboxcontainer']//div[contains(text(),'"+name+"')]";
	}
	
	//Job Configuration Fields
	public String verify_JobConfig_Fields(String name) {
		return "//table[contains(@class,'write_protected_row')]//span[contains(text(),'"+name+"')]";
	}
	
	public final String CMS_SubmissionName (String subName) {
		return "//div[contains(@class,'mainContentAboveStatus')]//div[contains(@class,'columnIndex')][6]//span[contains(text(),'"+subName+"')]";
	}
}
