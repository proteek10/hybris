package locators;

public class Hybris_dashboard {
	private static Hybris_dashboard ue;
	public static synchronized Hybris_dashboard exts(){
		try{
			if(ue.equals(null))
			{
				ue = new Hybris_dashboard();
			}
		}
		catch(Exception NOSYSTEM){
			ue = new Hybris_dashboard();
		}
		return ue;
	}

	//Login
	public final String Login_email ="//input[contains(@name,'username')]";
	public final String Login_password = "//input[contains(@name,'password')]";
	public final String Login_button = "//button[contains(@type,'submit')]";
	public final String Login_cookies_Ok_button ="//div[@class='cookies jsCookies']//button[text()='Ok']";
	
	//Dashboard Menu's
	public final String Dashboard_side_menus(String name){
		return "//tr[@title='"+name+"']";
			}
	
	public final String adavancedbutton = "//button[contains(text(),'Advanced')]";
	public final String proceed_button ="//a[contains(text(),'Proceed')]";
	
	
	//Create User > General Dialouge
	public  String create_new_employee_general(String fieldname){
		return "//span[contains(text(),'"+fieldname+":')]//..//input[contains(@class,'cockpitng_editor')]";
	}
	
	//New Page
	public final String New_Page_Approval_Status_DropDown_Btn = "//span[contains(text(),'Approval Status:')]//..//a";
	public final String New_page_Catalog_Version_InputField = "//span[contains(text(),'Catalog Version:')]//..//input";
	public final String New_Page_pageTemplate_InputField = "//span[contains(text(),'Page Template:')]//..//input";
	public final String New_Page_ID_InputField = "//span[contains(text(),'ID:')]//..//input";
	
	
	//New Employee
	public final String Dashboard_user_emp_newEmp = "//div[contains(@class,'cockpitng_advancedsearch')]//div[contains(@class,'action_create')]";
	public final String create_user_nextbutton = "//button[contains(text(),'Next')]";
	public final String create_user_donebutton = "//button[contains(text(),'Done')]";
	
	//Create User > Locale Information
	public  String create_new_employee_locale_information_values(String fieldname){
		return "//span[contains(text(),'"+fieldname+"')]//..//input[contains(@class,'bandbox')]";
	}
	
	public  String create_new_employee_locale_information_selectLanguage(String languagename){
		return "//div[contains(@class,'reference-editor-listbox')]//span[contains(text(),'"+languagename+"')]";
	}
	
	public  String create_new_employee_locale_information_selectCurrency(String currency){
		return "//div[contains(@class,'reference-editor-listbox')]//span[contains(text(),'"+currency+"')]";
	}
	
	//Create User > Membership
	public final String create_user_selectGroup_CountryInput = "//span[contains(text(),'Groups:')]//..//input[contains(@class,'bandbox')]";
	
	
	public  String create_new_employee_locale_information_selectGroup(String group){
		return "//div[contains(@class,'reference-editor-listbox')]//span[contains(text(),'"+group+"')]";
	}
	
	public  String create_new_employee_locale_information_selectAdminGroup(String groupName){
		return "//div[contains(@class,'reference-editor-listbox')]//span[contains(text(),'"+groupName+"')]";
	}
	
	//Search Employee & Search Button
	public final String search_input = "//span[contains(@class, 'yw-textsearch-searchbox')]//input";
	public final String search_Button = "//button[contains(text(),'Search')]";
	public final String addNewAssignment_SearchBtn = "//div[contains(text(),'Add New Assignment')]//..//..//..//button[contains(text(),'Search')]";
	public final String addNewAssignment_addBtn = "//div[contains(@ytestid,'classassignment-search')]//button[contains(@class,'advancedsearch-add-btn')]";
	public String searched_Result(String Name) {
		return "//span[contains(text(),'"+Name+"')]";
	}
	
	//Set Password
	public final String title_Password = "//span[contains(text(),'Password')]";
	public final String new_Password = "//input[@placeholder='New Password']";
	public final String confirm_Password = "//input[@placeholder='Confirm New Password']";
	
	//Save Button
	public final String save_Password = "//div[contains(@class,'y-toolbar-rightslot')]//..//button[contains(text(),'Save')]";
	public final String editItem_Save_Button = "//div[contains(@class,'y-toolbar-rightslot')]//..//button[contains(text(),'Save')]";
	
	//Catalog Version
	public final String catalog_Version = "//tr[contains(@class,'coll-browser-hyperlink')]//span[contains(text(),': Staged')]";
	
	//Submission Dashboard Jobs
	public final String pagination_next_button = "//div[contains(@class,'cockpitng_advancedsearch')]//a[contains(@class,'paging-next')]";
	public final String pagination_previous_button = "//div[contains(@class,'cockpitng_advancedsearch')]//a[contains(@class,'paging-previous')]";
	public final String select_search_firstrow = "//div[contains(@class,'hybris_cockpitng_collectionBrowser')]//table[1]//tbody//tr[contains(@class,'hyperlink')][1]";
	public final String select_firstrow_GetSubName = "//div[contains(@class,'hybris_cockpitng_collectionBrowser')]//table[1]//tbody//tr[contains(@class,'hyperlink')][1]//td[3]";
	
	public String select_row_GetSubName(int i) {
		 return "//div[contains(@class,'hybris_cockpitng_collectionBrowser')]//table[1]//tbody//tr[contains(@class,'hyperlink')]["+i+"]//td[3]";
	 }
	
	public String select_row_GetSubDetails(int j,int i) {
		 return "//div[contains(@class,'hybris_cockpitng_collectionBrowser')]//table[1]//tbody//tr[contains(@class,'hyperlink')]["+j+"]//td["+i+"]";
	 }
	
	public String GetSubDetails_noneditableBox_input(String inputvalue) {
		 return "//table[contains(@class,'box')]//input[contains(@value,'"+inputvalue+"')]";
	 }
	
	public String GetSubDetails_noneditableBox_span(String spanvalue) {
		 return "//table[contains(@class,'box')]//span[contains(text(),'"+spanvalue+"')]";
	 }
	
	//Submiision Sort Ascending/Descending
	public String Ascending_up_arrow (String columnName) {
	return	"//div[not(contains(@class,'jobitem')) and (contains(@class,'advancedsearch'))]//th[contains(@title,'"+columnName+"')]//div[contains(text(),'"+columnName+"')]";
	////i[contains(@class,'up')
	}
	
	public String Descending_down_arrow (String columnName) {
	return	"//div[not(contains(@class,'jobitem')) and (contains(@class,'advancedsearch'))]//th[contains(@title,'"+columnName+"')]//div[contains(text(),'"+columnName+"')]";
	////i[contains(@class,'down')]
		}
	
	public String pagination_page_value(int i) {
		 return "//div[contains(@class,'listview-actioncontainer')]//input[contains(@value,"+i+")]";
	 }
	
	//Verify first row job name
		public String select_row_VerifyJobName(String JobName,int i) {
			 return "//div[contains(@class,'hybris_cockpitng_collectionBrowser')]//table[1]//tbody//tr[contains(@class,'hyperlink')]["+i+"]//span[contains(text(),'"+JobName+"')]";
		 }
		
	//Verify job name with Change Detection status
			public String VerifyJobName_With_ChangeDetectionStatus(String JobName,String CDstatusvalue,int i) {
				 return "//div[contains(@class,'hybris_cockpitng_collectionBrowser')]//table[1]//tbody//tr[contains(@class,'hyperlink')]["+i+"]//td[10]//span[contains(text(),'"+CDstatusvalue+"')]";
			 }	
			
	//Verify job name with 'Product Feature Translation'.
	public String VerifyJobName_With_ProductFeatureTranslationStatus(String JobName,String ProdFeatValue,int i) {
		 return "//div[contains(@class,'hybris_cockpitng_collectionBrowser')]//table[1]//tbody//tr[contains(@class,'hyperlink')]["+i+"]//td[12]//span[contains(text(),'"+ProdFeatValue+"')]";
	 }	
	
	//Verify first row job name
	public String FirstRow_VerifyJobName(String JobName) {
		 return "//div[contains(@class,'hybris_cockpitng_collectionBrowser')]//table[1]//tbody//tr[contains(@class,'hyperlink')][1]//span[contains(text(),'"+JobName+"')]";
	 }
	
	//Translate Icon
	public final String translate_icon = "//div[contains(@class,'yw-editorarea-actioncontainer')]//img[@title='Translate']";
	public final String WithoutSelectItem_translate_icon = "//table[contains(@class,'translate')]//img[@title='Translate']";
	
	//Globe Icon Fields
	public final String globe_Icon_Fields = "//div[contains(@class,'editorarea-label-container z-div')]//span[contains(@class,'editorarea')]";
	public final String downArrow_Icon = "//div[contains(@class,'center-header')]//button[contains(@class,'expandCollapse')]";
	public final String globe_Icon_Fields_Input = "//span[contains(@class,'icon-loceditor')]//..//..//input";
	public final String globe_Icon_Name = "//span[@title='name']//..//span[contains(@class,'icon-loceditor')]";
	public final String globe_Icon_Identifier = "//span[contains(text(),'Identifier')]//..//span[contains(@class,'icon-loceditor')]";
	public final String globe_Icon_ProcessorModel = "//span[contains(text(),'Processor model')]//..//span[contains(@class,'icon-loceditor')]";
	public final String globe_Icon_Page_Title = "//span[@title='title']//..//span[contains(@class,'icon-loceditor')]";
	
	//Select categories/products
	public final String select_Multiple_Products = "//i[contains(@class,'listitem-icon')]";
	
	//Select Project
	
	public final String selectProject_tab = "//span[contains(text(),'Select Project')]";
	
	public String select_Project(String ProjectName) {
		 return "//div[contains(text(),'"+ProjectName+"')]";
	 }
	
	public String Verify_project_selected (String ProjectName) {
		return "//tr[contains(@class,'selected')]//div[contains(text(),'"+ProjectName+"')]";
	}
	
	//Select Profile
		public final String selectProfile_Tab = "//span[contains(text(),'Select Profile')]";
	
	//Select Project
		public String select_Language(String LanguageName) {
			 return "//div[contains(text(),'"+LanguageName+"')]//..//span[contains(@class,'checkbox')]";
		 }
		
	//Select checkbox submit only new and changed content
		public final String checkbox_submitonly_newcontent = "//span[contains(text(),'Submit only new and changed content')]//..//input[contains(@checked,'checked')]";
		public final String checkbox_submitonly_newcontent_uncheck = "//span[contains(text(),'Submit only new and changed content')]//..//span[contains(@class,'checkbox')]";
				//input[contains(@type,'checkbox')]";
	//Select checkbox submit only new and changed content
		public final String checkbox_translate_prod_feature = "//span[contains(text(),'Translate product features')]//..//input[contains(@checked,'checked')]";
		public final String checkbox_translate_prod_feature_uncheck = "//span[contains(text(),'Translate product features')]//..//span[contains(@class,'checkbox')]";			
		
	//Select Languages
	public final String clickOn_Lang_CheckBox = "//span[contains(text(),'Target Languages:')]//..//span[@class='z-listheader-checkable']";
	
	//Create New GL JOB buttons
	public final String gl_Job_Nxt_Btn = "//button[contains(text(),'Next')]";
	public final String gl_Job_Create_Btn = "//button[contains(text(),'Create')]";
	public final String gl_Job_Done_Btn = "//button[contains(text(),'Done')]";
	public final String gl_createJob_Window = "//div[contains(@class,'create-job-wizard')]";
	public final String gl_createjob_closeX_button = "//div[contains(@id,'close')]//i[contains(@class,'icon')]";
	public final String gl_createJobNameEmpty_Validation = "//span[contains(text(),'Please enter a job name.')]";
	public final String gl_createJobLanguageSelect_Validation ="//span[contains(text(),'target language')]";
	public final String gl_createJob_enterJobName_close = "//span[contains(text(),'Please enter a job name.')]//..//..//div[1]";
	public final String gl_createJob_SelectLangauge_close = "//span[contains(text(),'target language')]//..//..//div[1]";
//	public final String gl_createJob_Instructions = "(//iframe[@class='cke_wysiwyg_frame cke_reset'])[2]";
	public final String gl_createJob_Instructions_Input = "//body";
	
	//GL Job ID
	public final String gl_Job_ID = "//span[contains(text(),'Name:')]//..//input";
	
	//Advanced Search Icon
	public final String advanceSearchIcon = "//button[@title='Switch search mode']";
	
	//Attribute Value
	public final String articleNumber_Value_input = "//tr[@class='yw-active-attributes-grid yw-advancedsearch-last-row-for-condition z-row']//input[contains(@class,'hybris_cockpitng_editor_defaulttext')]";
	public final String catalogVersion_Value_input = "//tr[@class='yw-active-attributes-grid yw-advancedsearch-last-row-for-condition z-row']//input[contains(@class,'z-bandbox-input')]";
//	public final String catagories_Name_Value_input = "//span[contains(text(),'Name')]//..//..//input[@class='ye-input-text ye-com_hybris_cockpitng_editor_defaulttext z-textbox']";
	public final String catalog_identified_value_input = "//span[contains(text(),'Identifier')]//..//..//input[contains(@class,'hybris_cockpitng_editor_defaulttext')]";
	public final String catagories_Name_Value_input = "//span[contains(text(),'Name')]//..//..//input[contains(@class,'hybris_cockpitng_editor_defaulttext')]";
	public final String catagories_Identifier_Value_Input = "//span[contains(text(),'Identifier')]//..//..//input[contains(@class,'hybris_cockpitng_editor_defaulttext')]";
	public final String product_ArticleNumber_Input = "//div[contains(@class,'tabbox-content-wrapper')]//span[contains(text(),'Article Number')]//..//..//input";
	
	public String select_CatalogVersion(String catalogName) {
		 return "//span[contains(text(),'"+catalogName+"')]";
	 }
	
	//Create New Product
	public final String createNewProduct_ArticleNumber_Input = "//span[contains(text(),'Article Number:')]//..//input";
	public final String createNewProduct_Approval_DropDown_Arrow = "//span[contains(text(),'Approval:')]//..//a";
	public final String createNewProduct_CatalogVersion = "//span[contains(text(),'Catalog version:')]//..//input";
	public final String createNewProduct_Identifier_Input = "//span[contains(text(),'Identifier:')]//..//..//input";
	public final String delete_Product_Icon = "//img[@title='Delete [Delete]']";
	public final String delete_PopUp_Yes_Btn = "//button[contains(text(),'Yes')]";
	
	//Page attribute names
	public String pageAttriubteNames(String attributeName) {
		return "//div[@class='z-grid-body']//tbody[contains(@class,'active-rows')]//tr[contains(@class,'active-attributes')]//span[contains(text(),'"+attributeName+"')]";
	}
	
	//Attribute header
	public final String clickOn_Attribute_dropDown = "//span[contains(@class,'attributes-selector')]//input[contains(@class,'z-combobox-input')]";
	
	
	
	//Comparator
	public final String clickOn_comparator_dropDown = "//span[contains(@ytestid,'addOperatorSelector')]//input[contains(@class,'z-combobox-input')]";
	public final String featureTabdefault_clickOn_comparator_dropDown = "//span[contains(@ytestid,'classificationClass')]//input[contains(@class,'z-combobox-input')]";
	
	public String select_comparatorName(String comparator_name)
	{
		return "//div[contains(@class,'shadow')]//span[contains(text(),'"+comparator_name+"')]";
	}
	
	//Select Attribute
	public String select_PageAttributeName(String attributeName)
	{
		return "//div[contains(@class,'attributes-selector')]//span[contains(text(),'"+attributeName+"')]";
	}
	
	//Attribute Value
	public final String clickOn_Value_dropDown = "//tr[contains(@class,'yw-active-attributes-grid yw-add-field-row')]//div[contains(@class,'yw-advancedsearch-editor')]//input";
	public final String featureTab_default_clickOn_Value_dropDown = "//tr[contains(@class,'advancedsearch')]//div[contains(@ytestid,'editor-classificationClass')]//input";
	
	//Attribute Name for JOB_SEARCH
	public final String gl_Jobs_Attribute_Name = "//span[contains(text(),'Name')]//..//..//input[contains(@class,'input-text')]";
	public final String gl_jobs_attributeText = "//div[contains(@class,'search')]//div[contains(text(),'Attribute')]";
	
	//Select attribute value
	public String select_Attribute_Value(String attributeValue)
	{
		return "//div[contains(@class,'reference-editor')]//span[contains(text(),'"+attributeValue+"')]";
	}
	
	//Add Button
	public final String add_Button="//button[contains(@class,'advancedsearch-add-btn')]";
	public final String gl_Config_Add_Button = "//button[contains(text(),'Add')]";
	
	//Searched Page Result
	public String clickOn_SearchedPage(String pageName) {
		return "//tbody//span[(contains(text(),'"+pageName+"'))]";
	}	
	
	//Cron_Job Submitted Message
	public final String Cronjob_Submit_Message = "//span[contains(text(),'Cronjob submitToPDCronJob has been started.')]";
	
	//Cron_Job Fetch Message
	public final String Cronjob_Fetch_Message = "//span[contains(text(),'Cronjob fetchFromPDCronJob has been started.')]";
	
	//Verify Created Job details
	public String JobCreated_Details(String METADATA) {
		return "//div[contains(@class,'editorarea')]//span[contains(text(),'"+METADATA+"')]";
	}
	
	//Verify JOB Status under Jobs
	public String verify_Job_Status(String status) {
		return "//tr[@data-drag-key='GlobalLinkJob']//span[contains(text(),'"+status+"')]";
	}
	
	public final String jobsCount = "//tr[@data-drag-key='GlobalLinkJob']";
	
	public String verify_Job_Name_withRequestor(String SubmissionName,String requestorName) {
		return "//tr[@data-drag-key='GlobalLinkJob']//span[contains(text(),'"+SubmissionName+"')]/..//..//..//span[contains(text(),'"+requestorName+"')]";
	}
	
	public final String gl_Jobs_ExportIcon = "//img[@title='Export to CSV']";
	public final String gl_Jobs_Count = "//div[@ytestid='pagingContainerTop']//span[@ytestid='numberOfItemsLabel']";
	//Header Columns
	public String verify_Gl_Jobs_Header_Rows(String name) {
		return "//div[contains(text(),'"+name+"')]";
	}
	
	//List View
	public final String gl_Jobs_ListView = "//tr[contains(@class,'coll-browser-hyperlink')]";
	//Tree View
	public final String gl_Jobs_TreeView_Icon = "//div[@title='Tree View']";
	public final String gl_Jobs_TreeView = "//div[contains(@class,'treeview-cell yw-treeview-cell-label')]";
	//Grid View
	public final String gl_Jobs_GridView_Icon = "//div[@title='Grid View']";
	public final String gl_Jobs_GridView = "//div[contains(@ytestid,'gridViewTile')]";
	
	public String verify_Job_Status_changeDetectionStatus(String SubmissionName,String value) {
		return "//div[contains(@class,'listbox')]//table//tr//td[3]//span[contains(text(),'"+SubmissionName+"')]/..//..//following-sibling::td[7]//span[contains(text(),'"+value+"')]"; 
		}
	
	public String verify_JobName_WithStatus(String JobName,String status) {
		return "//tr[1]//span[text()='"+JobName+"']//..//..//..//span[contains(text(),'"+status+"')]";
	}
	//Verify TARGETS column under Job_Items
	public String verify_Status(String status) {
		return "//tr[@data-drag-key='GlobalLinkJobItem']//span[contains(text(),'"+status+"')]";
	}
	
	//Job Items > Details > Targets
	public final String jobItems_Details_Targets_label = "//span[@title='targets']//..//..//tr[contains(@class,'listitem')]";
	
	//Edit item > Job Item
	public final String editItem_JobItem = "//span[@title='jobItem']//..//..//tr[contains(@class,'listitem')]";
	
	//Edit item > Item
	public final String editItem_Item = "(//span[@title='item']//..//..//tr[contains(@class,'listitem')])[2]";
	public final String jobItems_Click_On_Item = "//span[@title='item']//..//..//div[contains(@class,'item')]";
	
	//Edit item > Properties > Icon
	public String editItem_TranslateIcon(String title) {
		return "//span[@title='"+title+"']//..//span[contains(@class,'icon')]";
	} 
	
	
	//Edit Item > Properties > Content Slots
	public String Edit_item_Select_Tab(String title) {
		return "//li[@title='"+title+"']";
	} 
	
	public String editeitem_content_slots(String SlotName) {
		return "//div[contains(@class,'reference-editor')]//span[contains(text(),'"+SlotName+"')]";
	} 
	
	public String editeitem_content_source_slots_propertiesvalue(String SourceLanguageName) {
		return "//div[contains(@class,'validation')]//span[contains(@title,'content')]//..//..//span[contains(@title,'"+SourceLanguageName+"')]/..//input";
	} 
	
	public String editeitem_content_target_slots_propertiesvalue(String TargetLanguageName) {
		return "//div[contains(@class,'groupbox-content')]//span[contains(@title,'"+TargetLanguageName+"')]/..//input";
	} 
	//[contains(@value,'')]
	public final String content_globeIcon = "//span[contains(@title,'content')]/..//span[contains(@class,'icon-loceditor')]";
	public final String Identifier_globeIcon = "//span[contains(@title,'name')]/..//span[contains(@class,'icon-loceditor')]";
	
	//Edit item > Properties > English Language
	public final String editItem_Lang_English = "//span[@title='English']//..//input";
	
	//Categories > General > Name > Language
	public final String catagories_Name_Lang_English = "(//span[@title='English']//..//input)[1]";
	public final String catagories_Name_Lang_German = "(//span[@title='German']//..//input)[1]";
	
	//Products > Properties/Attributes
	public final String products_Identifier_Lang_English = "(//span[@title='English']//..//input)[1]";
	public final String products_Identifier_Lang_German = "(//span[@title='German']//..//input)[1]";
	public final String products_Attributes_ProcessorModel = "//span[contains(text(),'Processor model')]//..//..//input[contains(@class,'hybris_cockpitng_editor_defaulttext')]";
	public final String products_Attributes_PM_Lang_English = "//span[contains(text(),'Processor model')]//..//..//..//span[@title='English']//..//input[contains(@class,'input-text')]";
	public final String products_Attributes_PM_Lang_German = "//span[contains(text(),'Processor model')]//..//..//..//span[@title='German']//..//input[contains(@class,'input-text')]";
	
	//Products > Attributes > Technical Features
	public final String products_Attributes_TechnicalFeatures = "//span[contains(text(),'Technical features')]";
	public final String products_Attributes_TechnicalFeatures_AddIcon = "//span[contains(text(),'Technical features')]//..//..//button[@title='Add new item']";
	public final String products_Attributes_TechnicalFeatures_InputField = "//span[contains(text(),'Technical features')]//..//..//input[@ytestid='focusComponent']";
	public final String products_Attributes_TechnicalFeatures_AddBtn ="//span[contains(text(),'Technical features')]//..//..//button[contains(text(),'Add')]";
	public final String products_Attributes_TechnicalFeatures_Values ="//span[contains(text(),'Technical features')]//..//..//div[@class='z-listcell-content']";
	public final String products_Attributes_TechnicalFeatures_DeleteIcon= "//span[contains(text(),'Technical features')]//..//..//button[@title='Delete selected item']";
	
	//Product Internal Labels
	public String catalog_product_Labels(String label) {
		return "//li[@title='"+label+"']";
	}
	
	//JobItem > Item > Identifier/Summary > Language
	public final String jobItem_Item_Identifier_Lang_English = "(//span[@title='English']//..//input)[1]";
	public final String jobItem_Item_Identifier_Lang_German = "(//span[@title='German']//..//input)[1]";
	public final String jobItem_Item_Summary_Lang_English = "(//span[@title='English']//..//input)[2]";
	public final String jobItem_Item_Summary_Lang_German = "(//span[@title='German']//..//input)[2]";
	
	//Edit item > Properties > German Language
	public final String editItem_Lang_German = "//span[@title='German']//..//input";
	
	//Job Items Search Button
	public final String jobItems_search_Button = "(//button[contains(text(),'Search')])[2]";
	public final String verify_jobitemWindow = "//div[contains(@class,'jobitem')]//table//div[contains(text(),'Item')]";
	
	//GlobalLink > Config
		public final String create_GL_Config_Icon = "(//div[contains(@class,'com_hybris_cockpitng_action_create cng-action')])[1]";
//		public final String create_GL_Config_Icon = "//span[contains(text(),'Create GlobalLink Configuration')]";
		public String GL_Config_Inputs(String label) {
			return "//span[contains(text(),'"+label+"')]//..//input";
		}

		public final String progress_bar = "//div[contains(@class,'loading-indicator')]";
		public final String pd_config_invalidURL_warning = "//a[contains(text(),'Unexpected error during object update')]";
		public final String pdURL_Input = "//span[contains(text(),'Project Director URL')]//..//..//input";
		public final String done_Button = "//button[contains(text(),'Done')]";
		public final String cancel_Button = "//button[contains(text(),'Cancel')]";
		public final String csv_Icon = "//img[@title='Export to CSV']";
		public final String create_GL_Config_ErrorMsg = "//span[contains(text(),'Unable to create: Unexpected error')]";
		public final String refresh_Button = "//div[contains(@class,'editorarea-actioncontainer')]//button[contains(text(),'Refresh')]";
		
		//Language Mapping
		public final String langMapping_AddBtn = "//input[@value='Create new GlobalLink Language Mapping']";
		public final String selectLang_Input = "//span[contains(text(),'Language:')]//..//input";
		public String selectLang_Option(String lang) {
			return "//span[contains(text(),'"+lang+"')]";
		}
		public final String selectLocaleCode_Input = "//span[contains(text(),'Locale Code:')]//..//input";
		public String selectLocaleCode_Option(String value) {
			return "//div[contains(text(),'"+value+"')]";
		}
		public String verifyMappedLanguages(String lang) {
			return "//span[contains(text(),'"+lang+"')]";
			
		}
		public String editLocaleCode_Option(String editCode) {
			return "//div[contains(@class,'bandpopup')]//div[contains(text(),'"+editCode+"')]";
		}
		public final String langField_Disabled = "//span[contains(@class,'editor-disabled')]";
		public final String search_Icon = "//span[contains(text(),'Locale Code')]//..//..//i";
		public final String editLocaleCode_SaveBtn = "//div[contains(text(),'Edit item  English - en-US')]//..//..//..//button[contains(text(),'Save')]";
		
		//Notification Recipients
		public final String notificationRecipients_AddBtn = "//input[@value='Create new GlobalLink Notification Recipient']";
		public final String emailAddrss_Input = "//span[contains(text(),'eMail address:')]//..//input[@type='text']";

		
		//Globallink>Configuration
		public final String Project_Dir_URL = "//div[contains(text(),'Username')]//..//..//span";//"//div[contains(text(),'Username')]//..//..//span[contains(text(),'')]/..";
		public final String Project_Dir_URL_tooltip_msg = "//th[contains(@title,'Click to sort by Project Director URL')]";
		public final String UserName_tab = "//div[contains(text(),'Username')]";
		public final String Default_Proj_ShortCode = "//div[contains(text(),'Default Project Short Code')]";
		public final String pd_URL = "//tr[@data-drag-key='GlobalLinkConfig']";
		public String gl_Config_Labels(String label) {
			return "//span[contains(text(),'"+label+"')]";
		}
		public final String gl_Config_Attribute_TypeConfig = "//input[@value='Create new GlobalLink Type Configuration']";
		
		public String verify_tootltip_msg(String tooltiptext) {
			return "//th[contains(@title,'"+tooltiptext+"')]";	
		}
		public final String gl_Config_FeatureTrans_IncludeFeatures = "//label[contains(text(),'Include only listed Features')]";
		public final String gl_Config_FeatureTrans_ExcludeFeatures = "//label[contains(text(),'Exclude only listed Features')]";
		public final String AddNewAssignment_Classifying_Category = "//span[contains(text(),'Classifying Category')]//..//..//input[contains(@class,'bandbox-input')]";
		public final String AddNewAssignment_Feature_Descriptor = "//span[contains(text(),'Feature Descriptor')]//..//..//input[contains(@class,'bandbox-input')]";
		public final String add_NewAssignment_SelectBox = "//tr[contains(@class,'hyperlink z-listitem')]//i[contains(@class,'listitem-icon')]";
		public final String add_NewAssignment_AddIcon = "//img[@title='Add Class Attribute Assignment']";
		
		//Features:
		public final String gl_Config_FeatureTrans_Features = "//span[@title='classificationAttributeAssignments']//..//..//span[@ytestid='reference-editor-reference']";
		public final String gl_Config_FeatureTrans_FeatureRemove_Icon = "//span[contains(text(),'Features')]//..//..//div[@ytestid='reference-editor-remove-button']";
		public final String gl_Config_FeatureTrans_FeatureRemove_Icon_Hover = "//span[contains(text(),'Features')]//..//..//div[@ytestid='reference-editor-remove-button']/..";
		
		public String gl_Config_verifyFeatureValue(String value) {
			return "//div[contains(@class,'selected-list')]//span[contains(text(),'"+value+"')]";
		}
		
		public String gl_Config_treeview_verifyCategory(String value) {
			return "//div[contains(@class,'tree')]//span[contains(text(),'"+value+"')]";
		}
		
		public String gl_Config_treeview_SelectCategory (String value) {
			return "//div[contains(@class,'tree')]//span[contains(text(),'"+value+"')]//..//..//span[contains(@class,'treerow-checkbox')]";
		}
		
		public String gl_Config_gridview_verifyCategory(String value) {
			return "//div[contains(@class,'grid')]//span[contains(text(),'"+value+"')]";
		}
		
		public String gl_Config_gridview_selectCategory(String value) {
			return "//div[contains(@class,'grid')]//span[contains(text(),'"+value+"')]//..//button[contains(@ytestid,'selectButton')]";
		}
		
		public String gl_Config_listview_verifyCategory(String value) {
			return "//div[contains(@class,'list')]//span[contains(text(),'"+value+"')]";
		}
		
		public String gl_Config_listview_SelectCategory (String value) {
			return "//div[contains(@class,'list')]//tr[1]//span[contains(text(),'"+value+"')]//..//span[contains(@class,'listitem-checkbox')]";
		}
		
		public final String gl_Config_Listview_Btn = "//div[contains(@ytestid,'classassignment')]//div[contains(@title,'List View')]";
		public final String gl_Config_Treeview_Btn = "//div[contains(@ytestid,'classassignment')]//div[contains(@title,'Tree View')]";
		public final String gl_Config_Gridview_Btn = "//div[contains(@ytestid,'classassignment')]//div[contains(@title,'Grid View')]";
		public final String gl_Config_treeview_PlusIcon = "//div[contains(@ytestid,'classassignment')]//tr[1]//div[contains(@class,'tree')]//span[contains(@id,'open')]";
		public final String gl_Config_view_PlusIcon = "//div[contains(@class,'list')]//img[contains(@title,'Add Class')]";
		public final String gl_Config_CloseAssignmentWindow_X = "//div[contains(@class,'assignment')]//div[contains(@class,'close')]";
		public final String gl_Config_AddNewAssignment = "//div[contains(text(),'Add New Assignment')]";
		public final String gl_Config_AddAssign_Save_Btn = "//div[contains(@class,'listview')]//button[contains(text(),'Save')]";
		//Administration Cockpit
		public final String title_Administration_Cockpit = "//div[@title='Administration Cockpit']";
		public String clickON_Cockpit_Dropdown(String name) {
			return "//div[@title='"+name+"']";
		}
		public String select_Cockpit_From_dropDown(String value) {
			return "//tr[@title='"+value+"']";	
		}
		
	//Product cockpit > Icon > Products
	public final String productCockpit_Icon_Products = "//span[contains(text(),'Products')]//..//..//..//img";
	
	public String productCockpit_Titles(String title) {
		 return "(//div[contains(@class,'treeCellInner')]//span[contains(text(),'"+title+"')])[1]";
	}
	
	public String productCockpit_subTitles(String subtitle) {
		 return "//div[contains(@class,'treeCellInner')]//span[contains(text(),'"+subtitle+"')]";
	}
	
	public String productCockpit_SelectProduct(String productName) {
		return "//span[contains(@title,'"+productName+"')]//..//button";
	} 
	
	public final String productCockpit_TranslateIcon = "//img[@title='Translate']";	
	
	//Create new job page elements
	//Select Project:
	public final String createNewJob_header_Title = "//div[contains(text(),'Create New GlobalLink Job')]";
	public final String clickOn_projectDescription_Icon = "//div[@title='Which project should the job be created for?']";
	public final String project_Description = "//span[contains(text(),'Which project should the job be created for?')]";
	
	//Job Configuration:
	public String create_New_GL_Job_Labels(String label) {
		return "//span[contains(text(),'"+label+"')]";
	}
	public final String createJob_HeaderTitle = "//div[contains(text(),'Create New GlobalLink Job')]";
	public final String gl_Job_backButton = "//button[contains(text(),'Back')]";
	public final String clickOn_ItemsDescription_Icon = "//div[@title='Which items should be submitted?']";

	public final String Job_Config_DefaultChecked_SelectedItemsOnly ="//input[contains(@name,'itemsSelection') and (contains(@checked,'checked'))]//..//label[contains(text(),'Selected items only')]";	
	
	public final String job_Config_DueDate_input = "//span[contains(text(),'Due Date:')]//..//input[@class='z-datebox-input']";
	public final String job_Config_Calender_Icon = "//span[contains(text(),'Due Date:')]//..//a[@class='z-datebox-button']";
	public String select_Due_Date(String date) {
		return "(//td[@class='z-calendar-cell z-calendar-weekday' and contains(text(),'"+date+"')]|//td[@class='z-calendar-cell z-calendar-weekend' and contains(text(),'"+date+"')])[1]";
	}

	public final String Job_Config_DefaultChecked_AllItemsFromThe_SearchResult ="//input[contains(@name,'itemsSelection') and (contains(@checked,'checked'))]//..//label[contains(text(),'All items from the search result')]";	
}
