package locators;

public class Hybris_smartedit {
	private static Hybris_smartedit ue;
	public static synchronized Hybris_smartedit exts(){
		try{
			if(ue.equals(null))
			{
				ue = new Hybris_smartedit();
			}
		}
		catch(Exception NOSYSTEM){
			ue = new Hybris_smartedit();
		}
		return ue;
	}

	//Login
	public final String Login_email ="//input[contains(@name,'username')]";
	public final String Login_password = "//input[contains(@name,'password')]";
	public final String Login_button = "//button[contains(@type,'submit')]";
	public final String Login_cookies_Ok_button ="//div[@class='cookies jsCookies']//button[text()='Ok']";
	
	
	//Your Site
	public final String select_yoursite_dropdown = "//span[contains(@class,'arrow ui-select-toggle')]";
	public  String select_yoursiteX(String sitename){
		return "//div[contains(@class,'select-dropdown')]//span[contains(text(),'"+sitename+"')]";
	}
	
	//Dashboard Smart site
	public  String select_apparel_uk_content_Catalog(String Cateogary,String pagetype){
		return "//div[contains(text(),'"+Cateogary+"')]//..//a[contains(text(),'"+pagetype+"')]";
	}
	
	//Staged
	public  String select_staged_page (String pageName){
		return "//div[contains(@class,'dynamic-paged')]//a[contains(text(),'"+pageName+"')]";
	}
	public final String translate_Icon = "//img[contains(@src,'icon_action_translate.png')]";
	public final String gl_Job_Name = "//label[contains(text(),'Job Name:')]//..//input";
	public final String gl_Job_selectsource_dropdown = "//div[contains(@class,'sourceLanguage')]";
	public final String gl_Job_selectSource_Language = "";
	public final String NextButton = "//button[contains(text(),'Next')]";
	public final String CloseButton = "//button[contains(text(),'Close')]";
	public final String Job_Created = "//div[contains(text(),'successfully created')]";
	public final String gl_Job_Click_Outside_CreationWindow = "//div[contains(@class,'dialog-window--normalized')]";
	public final String gl_create_job_window = "//div[contains(text(),'Create Job')]";
	public final String gl_calender_icon = "//i[contains(@class,'glyphicon-calendar')]";
	public final String gl_calender_next_month = "//i[contains(@class,'glyphicon-chevron-right')]";
	public final String gl_calender_previous_month = "//i[contains(@class,'glyphicon-chevron-left')]";
	public final String gl_window_close_window_X = "//button[contains(@class,'close')]";
	public final String gl_Job_selectProject_label = "//label[contains(text(),'Please select the project that you want to submit')]";
	
	public  String gl_calender_select_Day (String day){
		return "//td[contains(@class,'day')]//span[contains(text(),'"+day+"')]";
	}
	
	public  String gl_job_select_TargetLanguage_checkbox (String TargetLanguage){
		return "//div[contains(@class,'targetLanguage')]//..//label[contains(text(),'"+TargetLanguage+"')]/..//input[contains(@class,'empty')]";
	}
	
	public  String gl_job_select_TargetLanguage_checkbox_checked (String TargetLanguage){
		return "//div[contains(@class,'targetLanguage')]//..//label[contains(text(),'"+TargetLanguage+"')]/..//input[contains(@class,'empty') and contains(@class,'not')]";
	}
	
	public final String gl_job_select_all_TargetLanguage_checkbox= "//div[contains(@class,'targetLanguage')]//..//label[contains(text(),'Target Languages:')]/..//input[contains(@class,'empty')]";
	
	public final String gl_job_SubmitNewChanged_checkbox = "//label[contains(text(),'Submit new and changed only')]/..//input[contains(@class,'empty')]";
		
	
	public final String gl_job_SubmitNewChanged_checkbox_checked= "//label[contains(text(),'Submit new and changed only')]/..//input[contains(@class,'empty')  and contains(@class,'not')]";
	
	//Preview Button
	public final String preview_Btn = "//button[contains(text(),' Preview ')]";
	public String selectEditMode(String editMode) {
		return "//a[contains(text(),'"+editMode+"')]";
	}
	public final String down_Arrow_Icon = "//span[contains(@class,'se-perspective-selector__btn-arrow')]";
	
	//Select Profile
	public final String selectProfile_Page = "//label[contains(text(),'Please select the attribute configuration profile that you want use:')]";
	public String select_Profile(String profile) {
		 return "//label[contains(text(),'"+profile+"')]";
	 }
	public final String default_Radio_Btn = "//input[@value='Default']";
	
	//Create Job Buttons, Labels ..etc
	public String createJob_Labels(String label) {
		 return "//label[contains(text(),'"+label+"')]";
	 }
	public final String jobName_Alert_Message = "//span[contains(text(),'Job Name must not be empty.')]";
	public final String disabled_Next_Btn = "//button[@disabled='disabled' and contains(text(),'Next')]";
	public final String createJob_Title = "//div[contains(@id,'translatewizard.title')]";
	public final String sourceLang_DropDown = "//label[contains(text(),'Source Language:')]//..//select";
}
