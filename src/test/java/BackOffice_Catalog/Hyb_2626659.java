package BackOffice_Catalog;

import static org.testng.Assert.assertTrue;

import java.util.LinkedHashMap;

import org.gs4tr.qa.testrail.framework.TestRailClient;
import org.gs4tr.qa.utility.BrowserFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import actions.General;
import actions.Hybris_Common_Properties_Cred;
import actions.Hybris_TestRail_Common_Properties;
import actions.Verify;
import actions.common;

/*****************************************************************************************************************************************************************
* Author        : Praveen
*
* Test case     : Verify that product attributes and extend attributes are translated
* Test Id       : 2626659
* Summary   	: 
* Precondition  : GlobalLink adaptor should be deployed.
				  GlobalLink Config should be done.
******************************************************************************************************************************************************************/
public class Hyb_2626659 {
	
	Boolean assertion = true;
	LinkedHashMap<String, String> dataSet = new LinkedHashMap<String, String>();
	
	String SubmissionName = "HYB_2626659";
	
	@BeforeMethod	
	public void setUp() throws Exception{
		BrowserFactory.SystemEngine().startApplication();
		// Write you Test case description here
			dataSet.put("TL_test_case_description","Hyb_2626659: Verify that product attributes and extend attributes are translated");
				dataSet.put("TL_internal_testCase_ID", "2626659");
	}	
		public void testcase(LinkedHashMap<String, String> dataSet) throws Exception{
			try{
					
				General.action().login();
				Thread.sleep(2000);
				common.action().search_Multiple_Products(Hybris_Common_Properties_Cred.Title_catalog, Hybris_Common_Properties_Cred.Subtitle_products,true,
						Hybris_Common_Properties_Cred.Product_Article_Num_3,true, Hybris_Common_Properties_Cred.Catalog_categries_Electronics_Product_Catalog_Staged);
				Thread.sleep(1000);
				common.action().create_Job_Multiple_Categories(true,2,Hybris_Common_Properties_Cred.ProjectName_Hybris, SubmissionName,false,
						Hybris_Common_Properties_Cred.TargetLanguage_German,false,false,1);
				Thread.sleep(500);
				common.action().search_Job(Hybris_Common_Properties_Cred.Title_globalLink,Hybris_Common_Properties_Cred.subtitle_jobs,SubmissionName);
				Thread.sleep(1000);
				assertion=common.action().verify_Job_Status("","", Hybris_Common_Properties_Cred.Job_Status_Queued, SubmissionName);
				if(assertion == false){
					report("f"," assertion is failed while verifying  that Job should be queued.");
				}
				assertion=common.action().verify_Job_Status(Hybris_Common_Properties_Cred.subtitle_SubmitQuedJobs,"",
						Hybris_Common_Properties_Cred.Job_Status_inProgress, SubmissionName);
				if(assertion == false){
					report("f"," assertion is failed while verifying  that Job status in_progress");

				}
				assertion=common.action().verify_Job_Status("",Hybris_Common_Properties_Cred.subtitle_FetchTranslations,
						Hybris_Common_Properties_Cred.Job_Status_Complete, SubmissionName);
				if(assertion == false){
					report("f"," assertion is failed while verifying  that Job status Complete");
				}
				assertion=common.action().verify_Content_In_Catalog(false,true,"",Hybris_Common_Properties_Cred.Subtitle_products);
				if(assertion == false){
					report("f"," assertion is failed while verifying  translated content");
				}
			}
				catch(Throwable e){
				report("f", "Execution level error was encountered.\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				}
			}

		@Test
		public void execute() throws Exception{
			
			testcase(dataSet);
			assertion();
			
		}
			
		@AfterMethod
		public void tearDown() throws Exception{
		try {
			BrowserFactory.quitBrowser();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
					
		public void assertion() throws Exception{
		try{
			assertion=common.action().verify_Catalog_Product_Attribute_Values(true,Hybris_Common_Properties_Cred.Product_Label_Attributes,
					Hybris_Common_Properties_Cred.Product_Label_Attributes_Processor_Model_Value,false);
			if(assertion == false){
				report("f"," assertion is failed while verifying  that Job should be queued.");
			}else{
				report("p","Passed on "+Hybris_Common_Properties_Cred.Hybris_Version_TestRail_comment);
			}	
				}catch (Throwable e) {
					report("f", "Execution level error was encountered in assertion() .\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				
				}
		}
		
		
			public void report(String result, String notes) throws Exception
			{
			TestRailClient.testRailReportByID_production(dataSet.get("TL_internal_testCase_ID"),Hybris_TestRail_Common_Properties.idTestPlan,Hybris_TestRail_Common_Properties.idBuild,result,Hybris_TestRail_Common_Properties.assignedTo,notes);
			if(result == "f")
				assertTrue(false);
			}	

}