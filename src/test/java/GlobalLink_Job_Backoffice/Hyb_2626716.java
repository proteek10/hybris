package GlobalLink_Job_Backoffice;

import static org.testng.Assert.assertTrue;

import java.util.LinkedHashMap;

import org.gs4tr.qa.testrail.framework.TestRailClient;
import org.gs4tr.qa.utility.BrowserFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import actions.General;
import actions.Hybris_Common_Properties_Cred;
import actions.Hybris_TestRail_Common_Properties;
import actions.Verify;
import actions.common;
import locators.Hybris_dashboard;

/*****************************************************************************************************************************************************************
* Author        : Praveen
*
* Test case     : Verify that proper job name and due date is displayed as entered while creating new GlobalLink job configuration for multiple pages
* Test Id       : 2626716
* Summary   	: 
* Precondition  : GlobalLink adaptor should be deployed.
				  GlobalLink Config should be done.
******************************************************************************************************************************************************************/
public class Hyb_2626716 {
	
	Boolean assertion = true;
	LinkedHashMap<String, String> dataSet = new LinkedHashMap<String, String>();
	private static String editedDueDate;
	int num = 1;
	
	String SubmissionName = "HYB_2626716";
	
	@BeforeMethod	
	public void setUp() throws Exception{
		BrowserFactory.SystemEngine().startApplication();
		// Write you Test case description here
			dataSet.put("TL_test_case_description","Hyb_2626716: Verify that proper job name and due date is displayed as entered while creating new GlobalLink job configuration for multiple pages");
				dataSet.put("TL_internal_testCase_ID", "2626716");
	}	
		public void testcase(LinkedHashMap<String, String> dataSet) throws Exception{
			try{
					
				General.action().login();
				Thread.sleep(2000);
				common.action().search_Page_ProductComparator(Hybris_Common_Properties_Cred.Title_wcms, Hybris_Common_Properties_Cred.Subtitle_page,
						Hybris_Common_Properties_Cred.Page_Attribute_Catalog_Version, Hybris_Common_Properties_Cred.Page_Comparator_Value_Equals,
						Hybris_Common_Properties_Cred.Electronics_Content_Catalog_Staged, Hybris_Common_Properties_Cred.Page_Attribute_PageName, 
						Hybris_Common_Properties_Cred.Page_Comparator_Value_Equals, Hybris_Common_Properties_Cred.Page_Attribute_PageName_Cameras);
				Thread.sleep(1000);
				common.action().selectMulti_Products(num);
				Thread.sleep(1000);
				common.action().SelectProject_CreateJob(Hybris_Common_Properties_Cred.ProjectName_Hybris);
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Job_ID).clear();
				Thread.sleep(500);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Job_ID).sendKeys(SubmissionName);
				Thread.sleep(1000);
				//Edit Due Date
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().job_Config_DueDate_input).clear();
				Thread.sleep(500);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().job_Config_Calender_Icon).click();
				Thread.sleep(1000);
				editedDueDate = common.action().editDueDate();
				//Select Language
				Verify.action().verifyElementPresent(Hybris_dashboard.exts().select_Language(Hybris_Common_Properties_Cred.TargetLanguage_German),5);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_Language(Hybris_Common_Properties_Cred.TargetLanguage_German)).click();
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Job_Nxt_Btn).click();
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Job_Create_Btn).click();
				Thread.sleep(500);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Job_Done_Btn).click();
				Thread.sleep(1000);
				common.action().search_Job(Hybris_Common_Properties_Cred.Title_globalLink, Hybris_Common_Properties_Cred.subtitle_jobs, SubmissionName);
				Thread.sleep(1000);
				assertion=common.action().verify_Job_Status("","", Hybris_Common_Properties_Cred.Job_Status_Queued, SubmissionName);
				if(assertion == false){
					report("f"," assertion is failed while verifying  that Job should be queued.");
				}
				
				}catch(Throwable e){
				report("f", "Execution level error was encountered.\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				}
			}

		@Test
		public void execute() throws Exception{
			
			testcase(dataSet);
			assertion();
			
		}
			
		@AfterMethod
		public void tearDown() throws Exception{
		try {
			BrowserFactory.quitBrowser();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
					
		public void assertion() throws Exception{
		try{
			assertion=Verify.action().verifyElementPresent(Hybris_dashboard.exts().searched_Result(editedDueDate),  5);
			if(assertion == false){
				report("f"," assertion is failed while verifying  that GlobalLink module is present");
			}else{
				report("p","Passed on "+Hybris_Common_Properties_Cred.Hybris_Version_TestRail_comment);
			}	
				}catch (Throwable e) {
					report("f", "Execution level error was encountered in assertion() .\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				
				}
		}
		
		
			public void report(String result, String notes) throws Exception
			{
			TestRailClient.testRailReportByID_production(dataSet.get("TL_internal_testCase_ID"),Hybris_TestRail_Common_Properties.idTestPlan,Hybris_TestRail_Common_Properties.idBuild,result,Hybris_TestRail_Common_Properties.assignedTo,notes);
			if(result == "f")
				assertTrue(false);
			}

}
