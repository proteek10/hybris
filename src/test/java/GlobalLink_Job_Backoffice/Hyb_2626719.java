package GlobalLink_Job_Backoffice;

import static org.testng.Assert.assertTrue;

import java.util.LinkedHashMap;

import org.gs4tr.qa.testrail.framework.TestRailClient;
import org.gs4tr.qa.utility.BrowserFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import actions.General;
import actions.Hybris_Common_Properties_Cred;
import actions.Hybris_TestRail_Common_Properties;
import actions.Verify;
import actions.common;
import locators.Hybris_dashboard;

/*************************************************************************************************************************************************************
* Author        : Proteek
*
* Test case     : Verify that user is able to see details about the content of job by single click on listed GlobalLink job
* Test Id       : 2626719
* Summary   	: This test case is to verify that GlobalLink jobs listed on page are by default sorted with latest jobs on top i.e. in descending order w.r.t ID
* Precondition  : GlobalLink adaptor should be deployed.
				  Jobs created by GlobalLink should be present under GlobalLinkJob.
****************************************************************************************************************************************************************/
public class Hyb_2626719 {
	
	Boolean assertion = true;
	LinkedHashMap<String, String> dataSet = new LinkedHashMap<String, String>();
	
	String SubmissionName = "HYB_2626719_A1";
	String [] data={"PK","Type","Time created","Time modified","Owner"};
	@BeforeMethod	
	public void setUp() throws Exception{
		BrowserFactory.SystemEngine().startApplication();
		// Write you Test case description here
				dataSet.put("TL_test_case_description","Hyb_2626719:Verify that user is able to see details about the content of job by single click on listed GlobalLink job");
				dataSet.put("TL_internal_testCase_ID", "2626719");
	}	
		public void testcase(LinkedHashMap<String, String> dataSet) throws Exception{
			try{
					
				General.action().login();
				Thread.sleep(2000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(Hybris_Common_Properties_Cred.Title_globalLink)).click();
		  		Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(Hybris_Common_Properties_Cred.subtitle_jobs)).click();
				Thread.sleep(3000);
				
				String SubName = BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_firstrow_GetSubName).getText();
				Thread.sleep(3000);
				System.out.println("First Row SubName------>"+SubName);
				
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(Hybris_Common_Properties_Cred.Title_globalLink)).click();
		  		Thread.sleep(1000);
				
				common.action().search_Job(Hybris_Common_Properties_Cred.Title_globalLink,Hybris_Common_Properties_Cred.subtitle_jobs,SubName);
				
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_search_firstrow).click();
				Thread.sleep(3000);
				}catch(Throwable e){
				report("f", "Execution level error was encountered.\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				}
			}

		@Test
		public void execute() throws Exception{
			
			testcase(dataSet);
			assertion();
			
		}
			
		@AfterMethod
		public void tearDown() throws Exception{
		try {
			BrowserFactory.quitBrowser();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
					
		public void assertion() throws Exception{
		try{
			for(int i=0;i<5;i++){
				assertion = BrowserFactory.SystemEngine().verifyElementPresent(Hybris_dashboard.exts().JobCreated_Details(data[i]), 5);
		        if(assertion == false)
		              report("f", "Failed at final assertion Verify that selected attribute has been deleted ");
				
				Thread.sleep(2000);
				System.out.println("OUT OF LOOP JOBDETAILS");
			}	
				}catch (Throwable e) {
					report("f", "Execution level error was encountered in assertion() .\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				
				}
		}
		
		
			public void report(String result, String notes) throws Exception
			{
			TestRailClient.testRailReportByID_production(dataSet.get("TL_internal_testCase_ID"),Hybris_TestRail_Common_Properties.idTestPlan,Hybris_TestRail_Common_Properties.idBuild,result,Hybris_TestRail_Common_Properties.assignedTo,notes);
			if(result == "f")
				assertTrue(false);
			}	

}