package GlobalLink_Job_Backoffice;

import static org.testng.Assert.assertTrue;

import java.util.LinkedHashMap;

import org.gs4tr.qa.testrail.framework.TestRailClient;
import org.gs4tr.qa.utility.BrowserFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import actions.General;
import actions.Hybris_Common_Properties_Cred;
import actions.Hybris_TestRail_Common_Properties;
import actions.Verify;
import locators.Hybris_dashboard;

/*************************************************************************************************************************************************************
* Author        : Proteek
*
* Test case     : Verify that user is able to navigate to next pages using pagination when there are jobs listed in multiple pages
* Test Id       : 2626724
* Summary   	: 
* Precondition  : GlobalLink adaptor should be deployed.
				  There should be more than 50+ jobs present.
****************************************************************************************************************************************************************/
public class Hyb_2626724 {
	
	Boolean assertion = true;
	LinkedHashMap<String, String> dataSet = new LinkedHashMap<String, String>();
	
	
	@BeforeMethod	
	public void setUp() throws Exception{
		BrowserFactory.SystemEngine().startApplication();
		// Write you Test case description here
				dataSet.put("TL_test_case_description","Hyb_2626724:Verify that user is able to navigate to next pages using pagination when there are jobs listed in multiple pages");
				dataSet.put("TL_internal_testCase_ID", "2626724");
				
	}	
		public void testcase(LinkedHashMap<String, String> dataSet) throws Exception{
			try{
					
				General.action().login();
				Thread.sleep(2000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(Hybris_Common_Properties_Cred.Title_globalLink)).click();
		  		Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(Hybris_Common_Properties_Cred.subtitle_jobs)).click();
				Thread.sleep(3000);
				
				for(int i=1;i<10;i++){
					String SubName = BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_row_GetSubName(i)).getText();
					System.out.println(i+"Row SubName------>"+SubName);
					Thread.sleep(1000);
					assertion = BrowserFactory.SystemEngine().verifyElementPresent(Hybris_dashboard.exts().select_row_VerifyJobName(SubName,i), 5);
			        if(assertion == false)
			              report("f", "Failed at final assertion Verify that GlobalLink jobs page should be displayed and jobs should be listed.");
				}
				
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().pagination_next_button).click();
				Thread.sleep(3000);
				
				}catch(Throwable e){
				report("f", "Execution level error was encountered.\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				}
			}

		@Test
		public void execute() throws Exception{
			
			testcase(dataSet);
			assertion();
			
		}
			
		@AfterMethod
		public void tearDown() throws Exception{
		try {
			BrowserFactory.quitBrowser();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
					
		public void assertion() throws Exception{
		try{
				assertion = BrowserFactory.SystemEngine().verifyElementPresent(Hybris_dashboard.exts().pagination_page_value(2), 5);
		        if(assertion == false)
		              report("f", "Failed at final assertion Verify that User should be navigated to next page of job listing.");
		        else{
					report("p","Passed on "+Hybris_Common_Properties_Cred.Hybris_Version_TestRail_comment);
				}	
				
				}catch (Throwable e) {
					report("f", "Execution level error was encountered in assertion() .\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				
				}
		}
		
		
			public void report(String result, String notes) throws Exception
			{
			TestRailClient.testRailReportByID_production(dataSet.get("TL_internal_testCase_ID"),Hybris_TestRail_Common_Properties.idTestPlan,Hybris_TestRail_Common_Properties.idBuild,result,Hybris_TestRail_Common_Properties.assignedTo,notes);
			if(result == "f")
				assertTrue(false);
			}	

}