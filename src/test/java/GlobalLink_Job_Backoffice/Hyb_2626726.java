package GlobalLink_Job_Backoffice;

import static org.testng.Assert.assertTrue;

import java.util.LinkedHashMap;

import org.gs4tr.qa.testrail.framework.TestRailClient;
import org.gs4tr.qa.utility.BrowserFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import actions.General;
import actions.Hybris_Common_Properties_Cred;
import actions.Hybris_TestRail_Common_Properties;
import actions.Verify;
import actions.common;
import locators.Hybris_dashboard;

/*************************************************************************************************************************************************************
* Author        : Proteek
*
* Test case     : Verify that when user clicks on any of the column heads, job items listed are sorted accordingly as per user action
* Test Id       : 2626726
* Summary   	: 
* Precondition  : User should be logged in backoffice
				  GlobalLink adaptor should be configured
****************************************************************************************************************************************************************/
public class Hyb_2626726 {
	
	Boolean assertion = true;
	LinkedHashMap<String, String> dataSet = new LinkedHashMap<String, String>();
	String SubmissionName = "HYB_2626726";
	String SubmissionName_1 = "HYB_2626726_A1";
	String SubmissionName_2 = "HYB_2626726_B1";
	
	String data [] = {"ID","PK","Name","Time created"};
	
	@BeforeMethod	
	public void setUp() throws Exception{
		BrowserFactory.SystemEngine().startApplication();
		// Write you Test case description here
				dataSet.put("TL_test_case_description","Hyb_2626726:Verify that when user clicks on any of the column heads, job items listed are sorted accordingly as per user action");
				dataSet.put("TL_internal_testCase_ID", "2626726");
				
	}	
		public void testcase(LinkedHashMap<String, String> dataSet) throws Exception{
			try{
					
				General.action().login();
				Thread.sleep(2000);
				
				
				common.action().search_Page(Hybris_Common_Properties_Cred.Title_wcms, Hybris_Common_Properties_Cred.Subtitle_page,
				Hybris_Common_Properties_Cred.Page_Attribute_Catalog_Version,Hybris_Common_Properties_Cred.Page_ApparelUK_Content_Cat_Staged, 
				Hybris_Common_Properties_Cred.Page_Attribute_PageName,Hybris_Common_Properties_Cred.Page_Attribute_PageName_FAQ);
				Thread.sleep(2000);
				common.action().create_Job_Multiple_Categories(true,1,Hybris_Common_Properties_Cred.ProjectName_Hybris, SubmissionName_1,true,
				Hybris_Common_Properties_Cred.TargetLanguage_German,true,true,1);
				common.action().search_Job(Hybris_Common_Properties_Cred.Title_globalLink,Hybris_Common_Properties_Cred.subtitle_jobs,SubmissionName_1);
				
				Thread.sleep(3000);
				
				//Submission Name 2
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(Hybris_Common_Properties_Cred.Title_wcms)).click();
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(Hybris_Common_Properties_Cred.Subtitle_page)).click();
				Thread.sleep(1000);
				common.action().create_Job_Multiple_Categories(true,1,Hybris_Common_Properties_Cred.ProjectName_Hybris, SubmissionName_2,true,
				Hybris_Common_Properties_Cred.TargetLanguage_German,true,true,1);
				
				BrowserFactory.driver.navigate().refresh();
				Thread.sleep(5000);
				
				common.action().search_Job(Hybris_Common_Properties_Cred.Title_globalLink, Hybris_Common_Properties_Cred.subtitle_jobs, SubmissionName);
				Thread.sleep(5000);
				
				for(int i=0;i<4;i++){
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Ascending_up_arrow(data[i])).click();
				Thread.sleep(3000);
					assertion = BrowserFactory.SystemEngine().verifyElementPresent(Hybris_dashboard.exts().FirstRow_VerifyJobName(SubmissionName_1), 5);
			        if(assertion == false)
			              report("f", "Failed at final assertion Verify that they are sorted in ascending order, then they should get sorted in descending order and vice versa");
			        
			        BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Descending_down_arrow(data[i])).click();
					Thread.sleep(3000);
			        assertion = BrowserFactory.SystemEngine().verifyElementPresent(Hybris_dashboard.exts().FirstRow_VerifyJobName(SubmissionName_2), 5);
			        if(assertion == false)
			              report("f", "Failed at final assertion Verify that they are sorted in ascending order, then they should get sorted in descending order and vice versa");
				}
				
				
				}catch(Throwable e){
				report("f", "Execution level error was encountered.\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				}
			}

		@Test
		public void execute() throws Exception{
			
			testcase(dataSet);
			assertion();
			
		}
			
		@AfterMethod
		public void tearDown() throws Exception{
		try {
			BrowserFactory.quitBrowser();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
					
		public void assertion() throws Exception{
		try{
			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Ascending_up_arrow("Due Date")).click();
			Thread.sleep(3000);
				assertion = BrowserFactory.SystemEngine().verifyElementPresent(Hybris_dashboard.exts().FirstRow_VerifyJobName(SubmissionName_1), 5);
		        if(assertion == false)
		              report("f", "Failed at final assertion Verify that they are sorted in ascending order, then they should get sorted in descending order and vice versa");
		        
		        BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Descending_down_arrow("Due Date")).click();
				Thread.sleep(3000);
		        assertion = BrowserFactory.SystemEngine().verifyElementPresent(Hybris_dashboard.exts().FirstRow_VerifyJobName(SubmissionName_2), 5);
		        if(assertion == false)
		              report("f", "Failed at final assertion Verify that they are sorted in ascending order, then they should get sorted in descending order and vice versa");
		        else{
					report("p","Passed on "+Hybris_Common_Properties_Cred.Hybris_Version_TestRail_comment);
				}	
		}catch (Throwable e) {
			report("f", "Execution level error was encountered in assertion() .\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
		
		}
}
		
			public void report(String result, String notes) throws Exception
			{
			TestRailClient.testRailReportByID_production(dataSet.get("TL_internal_testCase_ID"),Hybris_TestRail_Common_Properties.idTestPlan,Hybris_TestRail_Common_Properties.idBuild,result,Hybris_TestRail_Common_Properties.assignedTo,notes);
			if(result == "f")
				assertTrue(false);
			}	

}