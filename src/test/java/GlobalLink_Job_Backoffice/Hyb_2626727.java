package GlobalLink_Job_Backoffice;

import static org.testng.Assert.assertTrue;

import java.util.LinkedHashMap;

import org.gs4tr.qa.testrail.framework.TestRailClient;
import org.gs4tr.qa.utility.BrowserFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import actions.General;
import actions.Hybris_Common_Properties_Cred;
import actions.Hybris_TestRail_Common_Properties;
import actions.Verify;
import locators.Hybris_dashboard;

/*****************************************************************************************************************************************************************
* Author        : Praveen
*
* Test case     : Verify that when the user performs search without entering any search data, list of all GlobalLink jobs is displayed with proper count of jobs
* Test Id       : 2626727
* Summary   	: 
* Precondition  : GlobalLink adaptor should be deployed.
				  GlobalLink Config should be done.
******************************************************************************************************************************************************************/
public class Hyb_2626727 {
	
	Boolean assertion = true;
	LinkedHashMap<String, String> dataSet = new LinkedHashMap<String, String>();
	String countOfJobs;
	
	@BeforeMethod	
	public void setUp() throws Exception{
		BrowserFactory.SystemEngine().startApplication();
		// Write you Test case description here
			dataSet.put("TL_test_case_description","Hyb_2626727: Verify that when the user performs search without entering any search data, list of all GlobalLink jobs is displayed with proper count of jobs");
				dataSet.put("TL_internal_testCase_ID", "2626727");
	}	
		public void testcase(LinkedHashMap<String, String> dataSet) throws Exception{
			try{
					
				General.action().login();
				Thread.sleep(2000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(Hybris_Common_Properties_Cred.Title_globalLink)).click();
		  		Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(Hybris_Common_Properties_Cred.subtitle_jobs)).click();
				Thread.sleep(3000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().search_Button).click();
				Thread.sleep(2000);
				assertion = Verify.action().verifyElementPresent(Hybris_dashboard.exts().gl_Jobs_ListView, 5);
				if(assertion == false){
					report("f"," assertion is failed while verifying  List View");
				}
				Thread.sleep(1000);
				countOfJobs = BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Jobs_Count).getText();
				Thread.sleep(1000);
				
				}catch(Throwable e){
				report("f", "Execution level error was encountered.\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				}
			}

		@Test
		public void execute() throws Exception{
			
			testcase(dataSet);
			assertion();
			
		}
			
		@AfterMethod
		public void tearDown() throws Exception{
		try {
			BrowserFactory.quitBrowser();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
					
		public void assertion() throws Exception{
		try{
			assertion = Verify.action().verifyTextPresent(countOfJobs, 5);
			if(assertion == false){
				report("f"," assertion is failed while verifying  total count of Jobs");
			}else{
				report("p","Passed on "+Hybris_Common_Properties_Cred.Hybris_Version_TestRail_comment);
			}	
				}catch (Throwable e) {
					report("f", "Execution level error was encountered in assertion() .\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				
				}
		}
		
		
			public void report(String result, String notes) throws Exception
			{
			TestRailClient.testRailReportByID_production(dataSet.get("TL_internal_testCase_ID"),Hybris_TestRail_Common_Properties.idTestPlan,Hybris_TestRail_Common_Properties.idBuild,result,Hybris_TestRail_Common_Properties.assignedTo,notes);
			if(result == "f")
				assertTrue(false);
			}	

}