package GlobalLink_Job_Backoffice;

import static org.testng.Assert.assertTrue;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;

import org.gs4tr.qa.testrail.framework.TestRailClient;
import org.gs4tr.qa.utility.BrowserFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import actions.General;
import actions.Hybris_Common_Properties_Cred;
import actions.Hybris_TestRail_Common_Properties;
import actions.Verify;
import locators.Hybris_dashboard;

/*************************************************************************************************************************************************************
* Author        : Proteek
*
* Test case     : Verify that user is able to search GlobalLink jobs using due date
* Test Id       : 2626731
* Summary   	: 
* Precondition  : User should be logged in backoffice
				  GlobalLink adaptor should be configured
****************************************************************************************************************************************************************/
public class Hyb_2626731 {
	
	Boolean assertion = true;
	LinkedHashMap<String, String> dataSet = new LinkedHashMap<String, String>();
	
	
	
	@BeforeMethod	
	public void setUp() throws Exception{
		BrowserFactory.SystemEngine().startApplication();
		// Write you Test case description here
				dataSet.put("TL_test_case_description","Hyb_2626731:Verify that user is able to search GlobalLink jobs using due date");
				dataSet.put("TL_internal_testCase_ID", "2626731");
				
	}	
		public void testcase(LinkedHashMap<String, String> dataSet) throws Exception{
			try{

				General.action().login();
				Thread.sleep(2000);
		
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(Hybris_Common_Properties_Cred.Title_globalLink)).click();
		  		Thread.sleep(2000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(Hybris_Common_Properties_Cred.subtitle_jobs)).click();
				Thread.sleep(5000);
				
				Date date = new Date();  
			    SimpleDateFormat formatter = new SimpleDateFormat("MMM dd,"+"yyyy");  
			    String strDate = formatter.format(date);  
			    System.out.println("Searching with Below Due Date---> : "+strDate);  
				
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().advanceSearchIcon).click();
				Thread.sleep(1000);
				
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().clickOn_Attribute_dropDown).click();
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_PageAttributeName("Due")).click();
				Thread.sleep(1000);		
				//Comparator
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().clickOn_comparator_dropDown).click();
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_comparatorName("Equals")).click();
				Thread.sleep(1000);		
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().clickOn_Value_dropDown).sendKeys(strDate);
				Thread.sleep(1000);	
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().add_Button).click();
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().search_Button).click();
				Thread.sleep(1000);
				
				
				}catch(Throwable e){
				report("f", "Execution level error was encountered.\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				}
			}

		@Test
		public void execute() throws Exception{
			
			testcase(dataSet);
			assertion();
			
		}
			
		@AfterMethod
		public void tearDown() throws Exception{
		try {
			BrowserFactory.quitBrowser();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
					
		public void assertion() throws Exception{
		try{
			
		        assertion = BrowserFactory.SystemEngine().verifyElementPresent(Hybris_dashboard.exts().select_row_GetSubName(1), 5);
		        if(assertion == false)
		              report("f", "Failed at final assertion Verify that User should be able to search a GlobalLink job using due date of a GlobalLink Job");
		        else{
					report("p","Passed on "+Hybris_Common_Properties_Cred.Hybris_Version_TestRail_comment);
				}	
		}catch (Throwable e) {
			report("f", "Execution level error was encountered in assertion() .\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
		
		}
}
		
			public void report(String result, String notes) throws Exception
			{
			TestRailClient.testRailReportByID_production(dataSet.get("TL_internal_testCase_ID"),Hybris_TestRail_Common_Properties.idTestPlan,Hybris_TestRail_Common_Properties.idBuild,result,Hybris_TestRail_Common_Properties.assignedTo,notes);
			if(result == "f")
				assertTrue(false);
			}	

}