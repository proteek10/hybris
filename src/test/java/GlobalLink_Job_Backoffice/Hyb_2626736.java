package GlobalLink_Job_Backoffice;

import static org.testng.Assert.assertTrue;

import java.util.LinkedHashMap;

import org.gs4tr.qa.testrail.framework.TestRailClient;
import org.gs4tr.qa.utility.BrowserFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import actions.General;
import actions.Hybris_Common_Properties_Cred;
import actions.Hybris_TestRail_Common_Properties;
import actions.Verify;
import actions.common;
import locators.Hybris_dashboard;

/*****************************************************************************************************************************************************************
* Author        : Proteek
*
* Test case     : Verify that status for all the newly created GlobalLink job is listed as “QUEUED”
* Test Id       : 2626736
* Summary   	: 
* Precondition  : GlobalLink adaptor should be deployed.
				  GlobalLink Config should be done.
******************************************************************************************************************************************************************/
public class Hyb_2626736 {
	
	Boolean assertion = true;
	LinkedHashMap<String, String> dataSet = new LinkedHashMap<String, String>();
	
	String SubmissionName = "HYB_2626736";
	
	@BeforeMethod	
	public void setUp() throws Exception{
		BrowserFactory.SystemEngine().startApplication();
		// Write you Test case description here
			dataSet.put("TL_test_case_description","Hyb_2626736: Verify that status for all the newly created GlobalLink job is listed as “QUEUED”");
				dataSet.put("TL_internal_testCase_ID", "2626736");
	}	
		public void testcase(LinkedHashMap<String, String> dataSet) throws Exception{
			try{
					
				General.action().login();
				Thread.sleep(2000);
				/*common.action().search_Product(Hybris_Common_Properties_Cred.Title_catalog, Hybris_Common_Properties_Cred.Subtitle_products, "29533", 
				Hybris_Common_Properties_Cred.Catalog_categories_Apparel_Product_Catalog_Staged);
				common.action().create_Job_Multiple_Categories(true,1,Hybris_Common_Properties_Cred.ProjectName_Hybris, SubmissionName,true,
						Hybris_Common_Properties_Cred.TargetLanguage_German,true,false,1);*/
				common.action().search_Job(Hybris_Common_Properties_Cred.Title_globalLink,Hybris_Common_Properties_Cred.subtitle_jobs,SubmissionName);
				
				assertion=common.action().verify_Job_Status("","", Hybris_Common_Properties_Cred.Job_Status_Queued, SubmissionName);
				if(assertion == false){
					report("f"," assertion is failed while verifying  that Job should be queued.");
				}
				
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Jobs_Attribute_Name).clear();
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().search_Button).click();
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().advanceSearchIcon).click();
				Thread.sleep(2000);
				
				for(int i=1;i<5;i++){
					String SubName = BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_row_GetSubName(i)).getText();
					System.out.println(i+"Row SubName------>"+SubName);
					Thread.sleep(1000);
					assertion = BrowserFactory.SystemEngine().verifyElementPresent(Hybris_dashboard.exts().select_row_VerifyJobName(SubName,i), 5);
			        if(assertion == false)
			              report("f", "Failed at final assertion Verify that selected attribute has been deleted ");
				}
				
				}catch(Throwable e){
				report("f", "Execution level error was encountered.\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				}
			}

		@Test
		public void execute() throws Exception{
			
			testcase(dataSet);
			assertion();
			
		}
			
		@AfterMethod
		public void tearDown() throws Exception{
		try {
			BrowserFactory.quitBrowser();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
					
		public void assertion() throws Exception{
		try{
			
				String SubName = BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_row_GetSubName(6)).getText();
				System.out.println(6+"Row SubName------>"+SubName);
				Thread.sleep(1000);
				assertion = BrowserFactory.SystemEngine().verifyElementPresent(Hybris_dashboard.exts().select_row_VerifyJobName(SubName,6), 5);
		        if(assertion == false)
		              report("f", "Failed at final assertion Verify that selected attribute has been deleted ");
		        else{
					report("p","Passed on "+Hybris_Common_Properties_Cred.Hybris_Version_TestRail_comment);
				}	
				
				Thread.sleep(2000);
				}catch (Throwable e) {
					report("f", "Execution level error was encountered in assertion() .\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				
				}
		}
		
			public void report(String result, String notes) throws Exception
			{
			TestRailClient.testRailReportByID_production(dataSet.get("TL_internal_testCase_ID"),Hybris_TestRail_Common_Properties.idTestPlan,Hybris_TestRail_Common_Properties.idBuild,result,Hybris_TestRail_Common_Properties.assignedTo,notes);
			if(result == "f")
				assertTrue(false);
			}	

}