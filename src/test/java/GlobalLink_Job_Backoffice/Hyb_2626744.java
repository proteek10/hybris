package GlobalLink_Job_Backoffice;

import static org.testng.Assert.assertTrue;

import java.util.LinkedHashMap;

import org.gs4tr.qa.testrail.framework.TestRailClient;
import org.gs4tr.qa.utility.BrowserFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import actions.General;
import actions.Hybris_Common_Properties_Cred;
import actions.Hybris_TestRail_Common_Properties;
import actions.Verify;
import actions.common;
import locators.Hybris_dashboard;

/**********************************************************************************************************************************************************************
* Author        : Proteek
*
* Test case     : Verify that on GlobalLink job details page all the job configurations displayed are same as those on listing page

* Test Id       : 2626744
* Summary   	: 
* Precondition  : GlobalLink adaptor should be deployed.
				  There should be some jobs present under GlobalLink > Jobs.
***********************************************************************************************************************************************************************/
public class Hyb_2626744 {
	
	Boolean assertion = true;
	LinkedHashMap<String, String> dataSet = new LinkedHashMap<String, String>();
	String SubmissionName = "HYB_2626744_B1";
	
	@BeforeMethod	
	public void setUp() throws Exception{
		BrowserFactory.SystemEngine().startApplication();
		// Write you Test case description here
				dataSet.put("TL_test_case_description","Hyb_2626744:Verify that on GlobalLink job details page all the job configurations displayed are same as those on listing page");
				dataSet.put("TL_internal_testCase_ID", "2626744");
				
	}	
		public void testcase(LinkedHashMap<String, String> dataSet) throws Exception{
			try{
					
				General.action().login();
				Thread.sleep(2000);				
				common.action().search_Page(Hybris_Common_Properties_Cred.Title_wcms, Hybris_Common_Properties_Cred.Subtitle_page,
				Hybris_Common_Properties_Cred.Page_Attribute_Catalog_Version, Hybris_Common_Properties_Cred.Page_ApparelUK_Content_Cat_Staged, 
				Hybris_Common_Properties_Cred.Page_Attribute_PageName, Hybris_Common_Properties_Cred.Page_Attribute_PageName_HomePage);
				Thread.sleep(2000);
				common.action().create_Job_Multiple_Categories(true,1,Hybris_Common_Properties_Cred.ProjectName_Hybris, SubmissionName,true,Hybris_Common_Properties_Cred.TargetLanguage_German,true,false,1);
					
				common.action().search_Job(Hybris_Common_Properties_Cred.Title_globalLink, Hybris_Common_Properties_Cred.subtitle_jobs, SubmissionName);
				
				
				String SubName = BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_row_GetSubDetails(1,3)).getText();
				System.out.println("SubName------>"+SubName);
				Thread.sleep(1000);
				
				String RequestorName = BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_row_GetSubDetails(1,3)).getText();
				System.out.println("RequestorName------>"+RequestorName);
				Thread.sleep(1000);
				
				String DueDate = BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_row_GetSubDetails(1,3)).getText();
				System.out.println("DueDate------>"+DueDate);
				Thread.sleep(1000);
				
				String SourceLanguage = BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_row_GetSubDetails(1,8)).getText();
				System.out.println("SourceLanguage------>"+SourceLanguage);
				Thread.sleep(1000);
				
				String TargetLanguage = BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_row_GetSubDetails(1,9)).getText();
				System.out.println("TargetLanguage------>"+TargetLanguage);
				Thread.sleep(1000);
		
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_firstrow_GetSubName).click();
				Thread.sleep(2000);
				
				assertion = BrowserFactory.SystemEngine().verifyElementPresent(Hybris_dashboard.exts().GetSubDetails_noneditableBox_input(SubName), 5);
		        if(assertion == false)
		              report("f", "Failed at final assertion Verify that The Source/Target language, due date, requestor, name, project short code, etc. should be the same as in listing page.");
		        
		        assertion = BrowserFactory.SystemEngine().verifyElementPresent(Hybris_dashboard.exts().GetSubDetails_noneditableBox_input(RequestorName), 5);
		        if(assertion == false)
		              report("f", "Failed at final assertion Verify that The Source/Target language, due date, requestor, name, project short code, etc. should be the same as in listing page.");
		        
		        assertion = BrowserFactory.SystemEngine().verifyElementPresent(Hybris_dashboard.exts().GetSubDetails_noneditableBox_input(DueDate), 5);
		        if(assertion == false)
		              report("f", "Failed at final assertion Verify that The Source/Target language, due date, requestor, name, project short code, etc. should be the same as in listing page.");
		        
		        assertion = BrowserFactory.SystemEngine().verifyElementPresent(Hybris_dashboard.exts().GetSubDetails_noneditableBox_span(SourceLanguage), 5);
		        if(assertion == false)
		              report("f", "Failed at final assertion Verify that The Source/Target language, due date, requestor, name, project short code, etc. should be the same as in listing page.");
		        
		        assertion = BrowserFactory.SystemEngine().verifyElementPresent(Hybris_dashboard.exts().GetSubDetails_noneditableBox_span(TargetLanguage), 5);
		        if(assertion == false)
		              report("f", "Failed at final assertion Verify that The Source/Target language, due date, requestor, name, project short code, etc. should be the same as in listing page.");
		        
				}catch(Throwable e){
				report("f", "Execution level error was encountered.\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				}
			}

		@Test
		public void execute() throws Exception{
			
			testcase(dataSet);
			assertion();
			
		}
			
		@AfterMethod
		public void tearDown() throws Exception{
		try {
			BrowserFactory.quitBrowser();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
					
		public void assertion() throws Exception{
		try{	
				String ProjectShortCode = BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_row_GetSubDetails(1,13)).getText();
				System.out.println("ProjectShortCode------>"+ProjectShortCode);
				Thread.sleep(1000);
				
				 assertion = BrowserFactory.SystemEngine().verifyElementPresent(Hybris_dashboard.exts().GetSubDetails_noneditableBox_input(ProjectShortCode), 5);
			        if(assertion == false)
			              report("f", "Failed at final assertion Verify that The Source/Target language, due date, requestor, name, project short code, etc. should be the same as in listing page.");
		        else{
					report("p","Passed on "+Hybris_Common_Properties_Cred.Hybris_Version_TestRail_comment);
				}	
				
				}catch (Throwable e) {
					report("f", "Execution level error was encountered in assertion() .\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				
				}
		}
		
		
			public void report(String result, String notes) throws Exception
			{
			TestRailClient.testRailReportByID_production(dataSet.get("TL_internal_testCase_ID"),Hybris_TestRail_Common_Properties.idTestPlan,Hybris_TestRail_Common_Properties.idBuild,result,Hybris_TestRail_Common_Properties.assignedTo,notes);
			if(result == "f")
				assertTrue(false);
			}	

}