package GlobalLink_Job_Backoffice;

import static org.testng.Assert.assertTrue;

import java.util.LinkedHashMap;

import org.gs4tr.qa.testrail.framework.TestRailClient;
import org.gs4tr.qa.utility.BrowserFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import actions.General;
import actions.Hybris_Common_Properties_Cred;
import actions.Hybris_TestRail_Common_Properties;
import actions.Verify;
import actions.common;
import locators.Hybris_dashboard;

/**********************************************************************************************************************************************************************
* Author        : Proteek
*
* Test case     : Verify that on GlobalLink job details page user is able to see all “JobItems” that were included by user while creating new GlobalLink job

* Test Id       : 2626745
* Summary   	: 
* Precondition  : GlobalLink adaptor should be deployed.
				  There should be some jobs present under GlobalLink > Jobs.
***********************************************************************************************************************************************************************/
public class Hyb_2626745 {
	
	Boolean assertion = true;
	LinkedHashMap<String, String> dataSet = new LinkedHashMap<String, String>();
	String SubmissionName = "HYB_2626744"; //reference to old submission fron 2626744
	
	@BeforeMethod	
	public void setUp() throws Exception{
		BrowserFactory.SystemEngine().startApplication();
		// Write you Test case description here
				dataSet.put("TL_test_case_description","Hyb_2626745:Verify that on GlobalLink job details page user is able to see all “JobItems” that were included by user while creating new GlobalLink job");
				dataSet.put("TL_internal_testCase_ID", "2626745");
				
	}	
		public void testcase(LinkedHashMap<String, String> dataSet) throws Exception{
			try{
					
				General.action().login();
				Thread.sleep(2000);				
			
				common.action().search_Job(Hybris_Common_Properties_Cred.Title_globalLink, Hybris_Common_Properties_Cred.subtitle_jobs, SubmissionName);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_firstrow_GetSubName).click();
				Thread.sleep(2000);
				
				General.action().doubleClick(Hybris_dashboard.exts().GetSubDetails_noneditableBox_span("homepage"));
			
				assertion = BrowserFactory.SystemEngine().verifyElementPresent(Hybris_dashboard.exts().GetSubDetails_noneditableBox_span("Homepage"), 5);
			       if(assertion == false)
			              report("f", "Failed at final assertion Verify thatAll the jobitems in the job should be listed that were included by the user when creating GlobalLink job.");
			       
			       
			   	assertion = BrowserFactory.SystemEngine().verifyElementPresent(Hybris_dashboard.exts().GetSubDetails_noneditableBox_span(SubmissionName), 5);
			       if(assertion == false)
			              report("f", "Failed at final assertion Verify thatAll the jobitems in the job should be listed that were included by the user when creating GlobalLink job.");
			       
			   	assertion = BrowserFactory.SystemEngine().verifyElementPresent(Hybris_dashboard.exts().GetSubDetails_noneditableBox_span("German"), 5);
			       if(assertion == false)
			              report("f", "Failed at final assertion Verify thatAll the jobitems in the job should be listed that were included by the user when creating GlobalLink job.");
				
				}catch(Throwable e){
				report("f", "Execution level error was encountered.\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				}
			}

		@Test
		public void execute() throws Exception{
			
			testcase(dataSet);
			assertion();
			
		}
			
		@AfterMethod
		public void tearDown() throws Exception{
		try {
			BrowserFactory.quitBrowser();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
					
		public void assertion() throws Exception{
		try{	
		 	assertion = BrowserFactory.SystemEngine().verifyElementPresent(Hybris_dashboard.exts().GetSubDetails_noneditableBox_span("French"), 5);
		       if(assertion == false)
		              report("f", "Failed at final assertion Verify thatAll the jobitems in the job should be listed that were included by the user when creating GlobalLink job.");
		        else{
					report("p","Passed on "+Hybris_Common_Properties_Cred.Hybris_Version_TestRail_comment);
				}	
				
				}catch (Throwable e) {
					report("f", "Execution level error was encountered in assertion() .\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				
				}
		}
		
		
			public void report(String result, String notes) throws Exception
			{
			TestRailClient.testRailReportByID_production(dataSet.get("TL_internal_testCase_ID"),Hybris_TestRail_Common_Properties.idTestPlan,Hybris_TestRail_Common_Properties.idBuild,result,Hybris_TestRail_Common_Properties.assignedTo,notes);
			if(result == "f")
				assertTrue(false);
			}	

}