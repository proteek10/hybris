package GlobalLink_Job_Backoffice;

import static org.testng.Assert.assertTrue;

import java.util.LinkedHashMap;

import org.gs4tr.qa.testrail.framework.TestRailClient;
import org.gs4tr.qa.utility.BrowserFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import actions.General;
import actions.Hybris_Common_Properties_Cred;
import actions.Hybris_TestRail_Common_Properties;
import actions.Pmuser_client;
import actions.Verify;
import actions.common;
import locators.Hybris_dashboard;
import locators.PD4_main_client_dashboard_Locators;

/*****************************************************************************************************************************************************************
* Author        : Praveen
*
* Test case     : Include translation instructions field while creating new GlobalLink job   
* Test Id       : 2626756
* Summary   	: 
* Precondition  : GlobalLink adaptor should be deployed.
				  GlobalLink Config should be done.
******************************************************************************************************************************************************************/
public class Hyb_2626756 {
	
	Boolean assertion = true;
	LinkedHashMap<String, String> dataSet = new LinkedHashMap<String, String>();
	
	String SubmissionName = "Hyb_2626756";
	
	
	
	String instructions = "Verify Instructions";
	
	@BeforeMethod	
	public void setUp() throws Exception{
		BrowserFactory.SystemEngine().startApplication();
		// Write you Test case description here
			dataSet.put("TL_test_case_description","Hyb_2626756: Include translation instructions field while creating new GlobalLink job ");
				dataSet.put("TL_internal_testCase_ID", "2626756");
	}	
		public void testcase(LinkedHashMap<String, String> dataSet) throws Exception{
			try{
					
				General.action().login();
				Thread.sleep(2000);
				common.action().search_Multiple_Products(Hybris_Common_Properties_Cred.Title_catalog, Hybris_Common_Properties_Cred.Subtitle_products,true,
						Hybris_Common_Properties_Cred.Product_Article_Num_1,true, Hybris_Common_Properties_Cred.Catalog_products_Apparel_Product_Catalog_Staged);
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_search_firstrow).click();
	  			Thread.sleep(1000);
	  			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().translate_icon).click();
	  			Thread.sleep(1000);
	  			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Job_ID).clear();
	  			Thread.sleep(500);
	  			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Job_ID).sendKeys(SubmissionName);
	  			Thread.sleep(2000);
	  			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_Language(Hybris_Common_Properties_Cred.TargetLanguage_German)).click();
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().checkbox_submitonly_newcontent_uncheck).click();
				Thread.sleep(500);
				
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Job_Nxt_Btn).click();
				Thread.sleep(1000);
				
				BrowserFactory.SystemEngine().switchToIframe(2);
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_createJob_Instructions_Input).sendKeys(instructions);
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().switchToDefault();
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Job_Create_Btn).click();
				Thread.sleep(5000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Job_Done_Btn).click();
				Thread.sleep(5000);

				common.action().search_Job(Hybris_Common_Properties_Cred.Title_globalLink,Hybris_Common_Properties_Cred.subtitle_jobs,SubmissionName);
				Thread.sleep(3000);
				
				assertion=common.action().verify_Job_Status("","", Hybris_Common_Properties_Cred.Job_Status_Queued, SubmissionName);
				if(assertion == false){
					report("f"," assertion is failed while verifying  that Job should be queued.");
				}
				Thread.sleep(3000);
				assertion=common.action().verify_Job_Status(Hybris_Common_Properties_Cred.subtitle_SubmitQuedJobs,"", Hybris_Common_Properties_Cred.Job_Status_inProgress, SubmissionName);
				if(assertion == false){
					report("f"," assertion is failed while verifying  that Job should be InProgress");
					
				}
				
				//STEP 07 PD
				common.action().Open_New_Instance_URL(Hybris_Common_Properties_Cred.PD_URL);
				
				common.action().PDlogIn(Hybris_Common_Properties_Cred.PD_Config_UserName, Hybris_Common_Properties_Cred.PD_Config_Password);
				Thread.sleep(2000);
				Pmuser_client.action().filterAndWaitForStatus(SubmissionName, "Completed", "Completed", 5);
				
				assertion =Verify.action().verifyElementPresent(PD4_main_client_dashboard_Locators.exts().previewMain_expandLevelLanguage("en-us⇒de-de"), 5);
				if(assertion == false){
				   report("f","Assertion failed while verifying the PD should contain a single job in single submission for all target languages");
				}
				Thread.sleep(500);
				BrowserFactory.SystemEngine().findElement(PD4_main_client_dashboard_Locators.exts().instructions_CheckMark).click();
				Thread.sleep(500);
				String subInstructions = BrowserFactory.SystemEngine().findElement(PD4_main_client_dashboard_Locators.exts().submission_Instructions).getText();
				Thread.sleep(500);
				if(subInstructions.equals(instructions)) {
					System.out.println("Instructions are same as entered while creating a GL Job.");
				}
				
				}catch(Throwable e){
				report("f", "Execution level error was encountered.\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				}
			}

		@Test
		public void execute() throws Exception{
			
			testcase(dataSet);
			assertion();
			
		}
			
		@AfterMethod
		public void tearDown() throws Exception{
		try {
			BrowserFactory.quitBrowser();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
					
		public void assertion() throws Exception{
		try{
			assertion=Verify.action().verifyTextPresent(instructions,  5);
			if(assertion == false){
				report("f"," assertion is failed while verifying  instructions");
			}else{
				report("p","Passed on "+Hybris_Common_Properties_Cred.Hybris_Version_TestRail_comment);
			}	
				}catch (Throwable e) {
					report("f", "Execution level error was encountered in assertion() .\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				
				}
		}
		
		
			public void report(String result, String notes) throws Exception
			{
			TestRailClient.testRailReportByID_production(dataSet.get("TL_internal_testCase_ID"),Hybris_TestRail_Common_Properties.idTestPlan,Hybris_TestRail_Common_Properties.idBuild,result,Hybris_TestRail_Common_Properties.assignedTo,notes);
			if(result == "f")
				assertTrue(false);
			}	

}