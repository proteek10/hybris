package Initial_config;

import static org.testng.Assert.assertTrue;
import java.util.LinkedHashMap;

import org.gs4tr.qa.testrail.framework.TestRailClient;
import org.gs4tr.qa.utility.BrowserFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import actions.General;
import actions.Hybris_Common_Properties_Cred;
import actions.Hybris_TestRail_Common_Properties;
import actions.Verify;
import actions.common;
import locators.Hybris_dashboard;

public class Page_Creation {
	
	Boolean assertion = true;
	LinkedHashMap<String, String> dataSet = new LinkedHashMap<String, String>();
	
	@BeforeMethod	
	public void setUp() throws Exception{
		BrowserFactory.SystemEngine().startApplication();
		// Write you Test case description here
				dataSet.put("TL_test_case_description","HYB_1010305:Verify version of GlobalLink Adaptor");
				dataSet.put("TL_internal_testCase_ID", "CaseID");
	}	
	
public void testcase(LinkedHashMap<String, String> dataSet) throws Exception{
	try{
					
		General.action().login();
		Thread.sleep(2000);
		General.action().createPage(Hybris_Common_Properties_Cred.Title_wcms, Hybris_Common_Properties_Cred.Subtitle_page);	
		Thread.sleep(1000);
		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(Hybris_Common_Properties_Cred.Title_wcms)).click();
		common.action().search_Page_ProductComparator(Hybris_Common_Properties_Cred.Title_wcms, Hybris_Common_Properties_Cred.Subtitle_page,
			Hybris_Common_Properties_Cred.Page_Attribute_Catalog_Version, Hybris_Common_Properties_Cred.Page_Comparator_Value_Equals,
			Hybris_Common_Properties_Cred.Page_ApparelUK_Content_Cat_Staged, Hybris_Common_Properties_Cred.Page_Attribute_PageID, 
			Hybris_Common_Properties_Cred.Page_Comparator_Value_Equals, Hybris_Common_Properties_Cred.newPage_ID);
		Thread.sleep(1000);
		common.action().verify_Globe_Icon_Fields(true, 1,true,"Page is created through automation");

	}catch(Throwable e){
		report("f", "Execution level error was encountered.\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
		}
	}
	
	@Test
	public void execute() throws Exception{
		
		testcase(dataSet);
		assertion();
		
	}
	
	@AfterMethod
	public void tearDown() throws Exception{
	try {
		BrowserFactory.quitBrowser();
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	}
	
	public void assertion() throws Exception{
		try{
				
				}catch (Throwable e) {
					report("f", "Execution level error was encountered in assertion() .\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				
				}
		}
	
	public void report(String result, String notes) throws Exception
	{
	TestRailClient.testRailReportByID_production(dataSet.get("TL_internal_testCase_ID"),Hybris_TestRail_Common_Properties.idTestPlan,Hybris_TestRail_Common_Properties.idBuild,result,Hybris_TestRail_Common_Properties.assignedTo,notes);
	if(result == "f")
		assertTrue(false);
	}	

}
