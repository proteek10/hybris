package Initial_config;

import static org.testng.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

import org.gs4tr.qa.testrail.framework.TestRailClient;
import org.gs4tr.qa.utility.BrowserFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import actions.General;
import actions.Hybris_Common_Properties_Cred;
import actions.Hybris_TestRail_Common_Properties;
import actions.Verify;

public class User_initial_config {
	
	Boolean assertion = true;
	LinkedHashMap<String, String> dataSet = new LinkedHashMap<String, String>();
	List<String> groupList = Arrays.asList("itemLockingGroup", "globallinkconfiggroup");
	ArrayList<String> list = new ArrayList<String>();
	String user_title = "User";
	String employees_title = "Employees";
	String language = "English";
	String currency = "Euro";
	String groupname1 = "admingroup";
	String groupname_1 = "[admingroup]";
	
	@BeforeMethod	
	public void setUp() throws Exception{
		BrowserFactory.SystemEngine().startApplication();
		// Write you Test case description here
				dataSet.put("TL_test_case_description","HYB_1010305:Verify version of GlobalLink Adaptor");
				dataSet.put("TL_internal_testCase_ID", "CaseID");
	}	
	
public void testcase(LinkedHashMap<String, String> dataSet) throws Exception{
	try{
					
		General.action().login();
		Thread.sleep(2000);
		list.addAll(groupList);
		General.action().createEmployee(user_title, employees_title, language, currency, groupname1, groupname_1, list);
		General.action().setPasswordForNewEmployee(Hybris_Common_Properties_Cred.autouserid, Hybris_Common_Properties_Cred.autouserpwd);		

	}catch(Throwable e){
		report("f", "Execution level error was encountered.\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
		}
	}
	
	@Test
	public void execute() throws Exception{
		
		testcase(dataSet);
		assertion();
		
	}
	
	@AfterMethod
	public void tearDown() throws Exception{
	try {
		BrowserFactory.quitBrowser();
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	}
	
	public void assertion() throws Exception{
		try{
				
				}catch (Throwable e) {
					report("f", "Execution level error was encountered in assertion() .\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				
				}
		}
	
	public void report(String result, String notes) throws Exception
	{
	TestRailClient.testRailReportByID_production(dataSet.get("TL_internal_testCase_ID"),Hybris_TestRail_Common_Properties.idTestPlan,Hybris_TestRail_Common_Properties.idBuild,result,Hybris_TestRail_Common_Properties.assignedTo,notes);
	if(result == "f")
		assertTrue(false);
	}	

}
