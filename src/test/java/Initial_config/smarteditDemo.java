package Initial_config;

import static org.testng.Assert.assertTrue;

import java.util.LinkedHashMap;

import org.gs4tr.qa.testrail.framework.TestRailClient;
import org.gs4tr.qa.utility.BrowserFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import actions.General;
import actions.Hybris_Common_Properties_Cred;
import actions.Hybris_TestRail_Common_Properties;
import actions.SE_Common_Prop_Cred;
import actions.Verify;
import actions.common;
import actions.smartedit;
import locators.Hybris_dashboard;

/***************************************************************************************************************
* Author        : Proteek
*
* Test case     : Verify that GlobalLink jobs listed on page are by default sorted in descending order w.r.t ID
* Test Id       : smarteditDemo
* Summary   	: This test case is to verify that GlobalLink jobs listed on page are by default sorted with latest jobs on top i.e. in descending order w.r.t ID
* Precondition  : User should be logged in backoffice
				  GlobalLink adaptor should be configured
****************************************************************************************************************/
public class smarteditDemo {
	
	Boolean assertion = true;
	LinkedHashMap<String, String> dataSet = new LinkedHashMap<String, String>();
	
	String SubmissionName = "HYB_DEMO_A1";
	
	@BeforeMethod	
	public void setUp() throws Exception{
		BrowserFactory.SystemEngine().startApplication();
		// Write you Test case description here
				dataSet.put("TL_test_case_description","smarteditDemo:Verify that user is able to create GlobalLink jobs for Pages");
				dataSet.put("TL_internal_testCase_ID", "smarteditDemo");
	}	
		public void testcase(LinkedHashMap<String, String> dataSet) throws Exception{
			try{
					
				General.action().login();
				smartedit.action().open_New_Instance_SmartEdit(SE_Common_Prop_Cred.SmartEdit_URL);
				
				smartedit.action().SmartEdit_login();

				smartedit.action().select_yourSite(SE_Common_Prop_Cred.Apparel_Site_UK);
				
				smartedit.action().create_Job_SmartEdit(SE_Common_Prop_Cred.catalog_staged, SE_Common_Prop_Cred.catalog_page_pages, SE_Common_Prop_Cred.catalog_page_addedit_Address_page,
									SubmissionName, true, "", SE_Common_Prop_Cred.German, true);
				
				
				}catch(Throwable e){
				report("f", "Execution level error was encountered.\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				}
			}

		@Test
		public void execute() throws Exception{
			
			testcase(dataSet);
			assertion();
			
		}
			
		@AfterMethod
		public void tearDown() throws Exception{
		try {
			BrowserFactory.quitBrowser();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
					
		public void assertion() throws Exception{
		try{
			assertion= Verify.action().verifyElementPresent(Hybris_dashboard.exts().FirstRow_VerifyJobName(SubmissionName),5);
			if(assertion == false){
				report("f","Recently created GlobalLink job should be listed on top which should have unique and incremental ID for it.");
			}else{
				report("p","Passed on "+Hybris_Common_Properties_Cred.Hybris_Version_TestRail_comment);
			}	
				}catch (Throwable e) {
					report("f", "Execution level error was encountered in assertion() .\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				
				}
		}
		
		
			public void report(String result, String notes) throws Exception
			{
			TestRailClient.testRailReportByID_production(dataSet.get("TL_internal_testCase_ID"),Hybris_TestRail_Common_Properties.idTestPlan,Hybris_TestRail_Common_Properties.idBuild,result,Hybris_TestRail_Common_Properties.assignedTo,notes);
			if(result == "f")
				assertTrue(false);
			}	

}