package Sanity_Backoffice;

import static org.testng.Assert.assertTrue;

import java.util.LinkedHashMap;

import org.gs4tr.qa.testrail.framework.TestRailClient;
import org.gs4tr.qa.utility.BrowserFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import actions.General;
import actions.Hybris_Common_Properties_Cred;
import actions.Hybris_TestRail_Common_Properties;
import actions.Verify;
import actions.common;
import locators.Hybris_dashboard;
/******************************************************************************************************************************
* Author        : Proteek
*
* Test case     : Verify that UI of Create New GlobalLink Job pop up for categories window is inline with that of Hybris backend
* Test Id       : 2626588
* Summary   	: 
* Precondition  : GlobalLink adaptor should be deployed.
				  GlobalLink Config should be done.
********************************************************************************************************************************/
public class Hyb_2626588 {

	
	Boolean assertion = true;
	LinkedHashMap<String, String> dataSet = new LinkedHashMap<String, String>();

	@BeforeMethod	
	public void setUp() throws Exception{
		BrowserFactory.SystemEngine().startApplication();
		// Write you Test case description here
				dataSet.put("TL_test_case_description","HYB_2626588:Verify that UI of Create New GlobalLink Job pop up for categories window is inline with that of Hybris backend");
				dataSet.put("TL_internal_testCase_ID", "2626588");
	}	
		public void testcase(LinkedHashMap<String, String> dataSet) throws Exception{
			try{
					
				General.action().login();
				Thread.sleep(2000);
				
				common.action().search_Page("Catalog", "Categories", Hybris_Common_Properties_Cred.Catalog_categories_Catalog_Version, Hybris_Common_Properties_Cred.Catalog_categories_Apparel_Product_Catalog_Staged, Hybris_Common_Properties_Cred.Catalog_categories_Catalog_Version, Hybris_Common_Properties_Cred.Catalog_categories_Apparel_Product_Catalog_Staged);		
				Thread.sleep(1000);
				
				Verify.action().verifyElementPresent(Hybris_dashboard.exts().select_search_firstrow,5);
	  			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_search_firstrow).click();
	  			
				Verify.action().verifyElementPresent(Hybris_dashboard.exts().translate_icon,5);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().translate_icon).click();
				Thread.sleep(1000);
				
				assertion=Verify.action().verifyTextPresent("Create New GlobalLink Job", 5);
				if(assertion == false){
					report("f"," assertion is failed while verifying  that GlobalLink create submission window open");
				}
				
				Verify.action().verifyElementPresent(Hybris_dashboard.exts().gl_Job_Nxt_Btn,5);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Job_Nxt_Btn).click();
				Thread.sleep(1000);
				
				
				}catch(Throwable e){
				report("f", "Execution level error was encountered.\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				}
			}

		@Test
		public void execute() throws Exception{
			
			testcase(dataSet);
			assertion();
			
		}
			
		@AfterMethod
		public void tearDown() throws Exception{
		try {
			BrowserFactory.quitBrowser();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
					
		public void assertion() throws Exception{
		try{
			assertion=Verify.action().verifyElementPresent(Hybris_dashboard.exts().gl_createJob_Window, 5);
			if(assertion == false){
				report("f"," assertion is failed while verifying  that GlobalLink 'Create New GlobalLink Job' should not be exactly opposite or it should not have default html style.");
			}else{
				report("p","Passed on "+Hybris_Common_Properties_Cred.Hybris_Version_TestRail_comment);
			}	
				}catch (Throwable e) {
					report("f", "Execution level error was encountered in assertion() .\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				
				}
		}
		
		
			public void report(String result, String notes) throws Exception
			{
			TestRailClient.testRailReportByID_production(dataSet.get("TL_internal_testCase_ID"),Hybris_TestRail_Common_Properties.idTestPlan,Hybris_TestRail_Common_Properties.idBuild,result,Hybris_TestRail_Common_Properties.assignedTo,notes);
			if(result == "f")
				assertTrue(false);
			}	
}
