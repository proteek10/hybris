package Sanity_Backoffice;

import static org.testng.Assert.assertTrue;

import java.awt.Robot;
import java.util.LinkedHashMap;

import org.gs4tr.qa.testrail.framework.TestRailClient;
import org.gs4tr.qa.utility.BrowserFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import actions.General;
import actions.Hybris_Common_Properties_Cred;
import actions.Hybris_TestRail_Common_Properties;
import actions.Verify;
import locators.Hybris_dashboard;
/********************************************************************************************************
* Author        : Proteek
*
* Test case     : Verify that tooltips present on all tabs of configuration page display complete text
* Test Id       : 2626591
* Summary   	: 
* Precondition  : GlobalLink adaptor should be deployed.
				  GlobalLink Config should be done.
**********************************************************************************************************/
public class Hyb_2626591 {

	
	Boolean assertion = true;
	LinkedHashMap<String, String> dataSet = new LinkedHashMap<String, String>();

	@BeforeMethod	
	public void setUp() throws Exception{
		BrowserFactory.SystemEngine().startApplication();
		// Write you Test case description here
				dataSet.put("TL_test_case_description","HYB_2626591:Verify that tooltips present on all tabs of configuration page display complete text");
				dataSet.put("TL_internal_testCase_ID", "2626591");
	}	
		public void testcase(LinkedHashMap<String, String> dataSet) throws Exception{
			try{
					
				General.action().login();
				Thread.sleep(2000);
				
				Verify.action().verifyElementPresent(Hybris_dashboard.exts().Dashboard_side_menus("GlobalLink"),5);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus("GlobalLink")).click();
				Thread.sleep(1000);
				
				Verify.action().verifyElementPresent(Hybris_dashboard.exts().Dashboard_side_menus("Configuration"),5);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus("Configuration")).click();
				Thread.sleep(1000);
				
				assertion=Verify.action().verifyTextPresent(Hybris_Common_Properties_Cred.PD_URL, 5);
				if(assertion == false){
					report("f"," assertion is failed while verifying  that GlobalLink module is present");
				}
				
				Robot robot = new Robot();		
				org.openqa.selenium.Point coordinates = BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Project_Dir_URL).getLocation();	
				robot.mouseMove(coordinates.x,coordinates.y+80);
				Thread.sleep(2000);
				assertion=Verify.action().verifyElementPresent(Hybris_dashboard.exts().verify_tootltip_msg("Click to sort by Project Director URL"),5);
				if(assertion == false){
					report("f"," assertion is failed while verifying  that Tool-tip should display correct and complete text without any cut-off.");
				}
				
				org.openqa.selenium.Point coordinates1 = BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().UserName_tab).getLocation();	
				robot.mouseMove(coordinates1.x,coordinates1.y+80);
				Thread.sleep(2000);
				assertion=Verify.action().verifyElementPresent(Hybris_dashboard.exts().verify_tootltip_msg("Click to sort by Username"),5);
				if(assertion == false){
					report("f"," assertion is failed while verifying  that Tool-tip should display correct and complete text without any cut-off.");
				}
				
				org.openqa.selenium.Point coordinates2 = BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Default_Proj_ShortCode).getLocation();	
				robot.mouseMove(coordinates2.x,coordinates2.y+80);
				Thread.sleep(2000);
				}catch(Throwable e){
				report("f", "Execution level error was encountered.\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				}
			}

		@Test
		public void execute() throws Exception{
			
			testcase(dataSet);
			assertion();
			
		}
			
		@AfterMethod
		public void tearDown() throws Exception{
		try {
			BrowserFactory.quitBrowser();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
					
		public void assertion() throws Exception{
		try{
			assertion=Verify.action().verifyElementPresent(Hybris_dashboard.exts().verify_tootltip_msg("Click to sort by Default Project Short Code"), 5);
			if(assertion == false){
				report("f"," assertion is failed while verifying  that Tool-tip should display correct and complete text without any cut-off.");
			}else{
				report("p","Passed on "+Hybris_Common_Properties_Cred.Hybris_Version_TestRail_comment);
			}	
				}catch (Throwable e) {
					report("f", "Execution level error was encountered in assertion() .\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				
				}
		}
		
		
			public void report(String result, String notes) throws Exception
			{
			TestRailClient.testRailReportByID_production(dataSet.get("TL_internal_testCase_ID"),Hybris_TestRail_Common_Properties.idTestPlan,Hybris_TestRail_Common_Properties.idBuild,result,Hybris_TestRail_Common_Properties.assignedTo,notes);
			if(result == "f")
				assertTrue(false);
			}	
}
