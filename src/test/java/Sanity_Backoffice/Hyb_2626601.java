package Sanity_Backoffice;

import static org.testng.Assert.assertTrue;


import java.util.LinkedHashMap;
import org.gs4tr.qa.testrail.framework.TestRailClient;
import org.gs4tr.qa.utility.BrowserFactory;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import actions.General;
import actions.Hybris_Common_Properties_Cred;
import actions.Hybris_TestRail_Common_Properties;
import actions.Verify;
import actions.common;
import locators.Hybris_dashboard;

/********************************************************************************************************
* Author        : Praveen
*
* Test case     : Verify that when user clicks on "Fetch Translations" link all the jobs of "IN_PROGRESS" are successfully fetched from PD 
* Test Id       : 2626601
* Summary   	: 
* Precondition  : GlobalLink adaptor should be deployed.
**********************************************************************************************************/
public class Hyb_2626601 {
	
	Boolean assertion = true;
	LinkedHashMap<String, String> dataSet = new LinkedHashMap<String, String>();
	
	String SubmissionName = "HYB_2626601";
	
	@BeforeMethod	
	public void setUp() throws Exception{
		BrowserFactory.SystemEngine().startApplication();
		// Write you Test case description here
				dataSet.put("TL_test_case_description","Hyb_2626601:Verify that when user clicks on \"Fetch Translations\" link all the jobs of \"IN_PROGRESS\" are successfully fetched from PD");
				dataSet.put("TL_internal_testCase_ID", "2626601");
	}	
		public void testcase(LinkedHashMap<String, String> dataSet) throws Exception{
			try{
					
				General.action().login();
				Thread.sleep(2000);
				common.action().search_Page(Hybris_Common_Properties_Cred.Title_wcms, Hybris_Common_Properties_Cred.Subtitle_page,
					Hybris_Common_Properties_Cred.Page_Attribute_Catalog_Version, Hybris_Common_Properties_Cred.Page_ApparelUK_Content_Cat_Staged, 
					Hybris_Common_Properties_Cred.Page_Attribute_PageName, Hybris_Common_Properties_Cred.Page_Attribute_PageName_HomePage);
				common.action().create_Job(Hybris_Common_Properties_Cred.ProjectName_Hybris, SubmissionName, Hybris_Common_Properties_Cred.TargetLanguage_German,true,false);
				common.action().search_Job(Hybris_Common_Properties_Cred.Title_globalLink,Hybris_Common_Properties_Cred.subtitle_jobs,SubmissionName);
				assertion = common.action().verify_Job_Status(Hybris_Common_Properties_Cred.subtitle_SubmitQuedJobs,"", Hybris_Common_Properties_Cred.Job_Status_inProgress, SubmissionName);
				if(assertion == false){
					report("f"," assertion is failed while verifying job status");
				}
				assertion = common.action().verify_Job_Status("",Hybris_Common_Properties_Cred.subtitle_FetchTranslations, Hybris_Common_Properties_Cred.Job_Status_Complete, SubmissionName);
				if(assertion == false){
					report("f"," assertion is failed while verifying job status");
				}
				
				}catch(Throwable e){
				report("f", "Execution level error was encountered.\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				}
			}

		@Test
		public void execute() throws Exception{
			
			testcase(dataSet);
			assertion();
			
		}
			
		@AfterMethod
		public void tearDown() throws Exception{
		try {
			BrowserFactory.quitBrowser();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
					
		public void assertion() throws Exception{
		try{
			assertion=common.action().verify_Translated_Content(Hybris_Common_Properties_Cred.subtitle_Job_Items,SubmissionName,Hybris_Common_Properties_Cred.editItem_Page_Title);
			if(assertion == false){
				report("f"," assertion is failed while verifying  that GlobalLink module is present");
			}else{
				report("p","Passed on "+Hybris_Common_Properties_Cred.Hybris_Version_TestRail_comment);
			}	
				}catch (Throwable e) {
					report("f", "Execution level error was encountered in assertion() .\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				
				}
		}
		
		
			public void report(String result, String notes) throws Exception
			{
			TestRailClient.testRailReportByID_production(dataSet.get("TL_internal_testCase_ID"),Hybris_TestRail_Common_Properties.idTestPlan,Hybris_TestRail_Common_Properties.idBuild,result,Hybris_TestRail_Common_Properties.assignedTo,notes);
			if(result == "f")
				assertTrue(false);
			}	

}