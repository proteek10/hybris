package Sanity_Backoffice;

import static org.testng.Assert.assertTrue;

import java.util.LinkedHashMap;

import org.gs4tr.qa.testrail.framework.TestRailClient;
import org.gs4tr.qa.utility.BrowserFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import actions.General;
import actions.Hybris_Common_Properties_Cred;
import actions.Hybris_TestRail_Common_Properties;
import actions.Verify;
import actions.common;

/*******************************************************************************************************************************************
* Author        : Proteek
*
* Test case     : Verify that when user clicks on "Fetch Translations" link all the jobs of "IN_PROGRESS" are successfully fetched from PD
* Test Id       : 2626602
* Summary   	: 
* Precondition  : GlobalLink adaptor should be deployed.
*********************************************************************************************************************************************/
public class Hyb_2626602 {
	
	Boolean assertion = true;
	LinkedHashMap<String, String> dataSet = new LinkedHashMap<String, String>();
	
	//This test case will use submission from TC 2626598 --HYB_2626598 Sub Name
	String SubmissionName = "HYB_2626598";
	
	@BeforeMethod	
	public void setUp() throws Exception{
		BrowserFactory.SystemEngine().startApplication();
		// Write you Test case description here
				dataSet.put("TL_test_case_description","Hyb_2626602:Verify that when user clicks on \"Submit Queued Jobs\" all the queued GlobalLink jobs are submitted successfully to PD");
				dataSet.put("TL_internal_testCase_ID", "2626602");
	}	
		public void testcase(LinkedHashMap<String, String> dataSet) throws Exception{
			try{
					
				General.action().login();
				common.action().search_Job(Hybris_Common_Properties_Cred.Title_globalLink,Hybris_Common_Properties_Cred.subtitle_jobs,SubmissionName);
				common.action().verify_Job_Status(Hybris_Common_Properties_Cred.subtitle_FetchTranslations,"", "", SubmissionName);				
				Thread.sleep(30000);
				}catch(Throwable e){
				report("f", "Execution level error was encountered.\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				}
			}

		@Test
		public void execute() throws Exception{
			
			testcase(dataSet);
			assertion();
			
		}
			
		@AfterMethod
		public void tearDown() throws Exception{
		try {
			BrowserFactory.quitBrowser();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
					
		public void assertion() throws Exception{
		try{
			assertion=common.action().Verify_WaitUntil_JobStatus(SubmissionName, "COMPLETE");
			if(assertion == false){
				report("f"," assertion is failed while verifying  that GlobalLink module is present");
			}else{
				report("p","Passed on "+Hybris_Common_Properties_Cred.Hybris_Version_TestRail_comment);
			}	
				}catch (Throwable e) {
					report("f", "Execution level error was encountered in assertion() .\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				
				}
		}
		
		
			public void report(String result, String notes) throws Exception
			{
			TestRailClient.testRailReportByID_production(dataSet.get("TL_internal_testCase_ID"),Hybris_TestRail_Common_Properties.idTestPlan,Hybris_TestRail_Common_Properties.idBuild,result,Hybris_TestRail_Common_Properties.assignedTo,notes);
			if(result == "f")
				assertTrue(false);
			}	

}