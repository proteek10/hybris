package Sanity_Backoffice;

import static org.testng.Assert.assertTrue;

import java.util.LinkedHashMap;

import org.gs4tr.qa.testrail.framework.TestRailClient;
import org.gs4tr.qa.utility.BrowserFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import actions.General;
import actions.Hybris_Common_Properties_Cred;
import actions.Hybris_TestRail_Common_Properties;
import actions.Verify;
import locators.Hybris_dashboard;
/***********************************************************************************************************************************
* Author        : Proteek
*
* Test case     : Verify that when user click on job item under globallink configuration then no error message should get display
* Test Id       : 2626604
* Summary   	: 
* Precondition  : GlobalLink adaptor should be deployed.
*************************************************************************************************************************************/
public class Hyb_2626604 {

	
	Boolean assertion = true;
	LinkedHashMap<String, String> dataSet = new LinkedHashMap<String, String>();

	@BeforeMethod	
	public void setUp() throws Exception{
		BrowserFactory.SystemEngine().startApplication();
		// Write you Test case description here
				dataSet.put("TL_test_case_description","HYB_2626604:Verify that when user click on job item under globallink configuration then no error message should get display");
				dataSet.put("TL_internal_testCase_ID", "2626604");
	}	
		public void testcase(LinkedHashMap<String, String> dataSet) throws Exception{
			try{
					
				General.action().login();
				Thread.sleep(2000);
				
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus("GlobalLink")).click();
				Thread.sleep(2000);
				
				assertion=Verify.action().verifyElementPresent(Hybris_dashboard.exts().Dashboard_side_menus("Configuration"), 5);
				if(assertion == false){
					report("f"," assertion is failed while verifying  that Configuration sub module is present");
				}
				
				assertion=Verify.action().verifyElementPresent(Hybris_dashboard.exts().Dashboard_side_menus("Logging"), 5);
				if(assertion == false){
					report("f"," assertion is failed while verifying  that Logging sub module is present");
				}
				
				assertion=Verify.action().verifyElementPresent(Hybris_dashboard.exts().Dashboard_side_menus("Submit Queued Jobs"), 5);
				if(assertion == false){
					report("f"," assertion is failed while verifying  that Submit Queued Jobs sub module is present");
				}
				
				//Jobs Items
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus("Job Items")).click();
				Thread.sleep(2000);
				
				}catch(Throwable e){
				report("f", "Execution level error was encountered.\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				}
			}

		@Test
		public void execute() throws Exception{
			
			testcase(dataSet);
			assertion();
			
		}
			
		@AfterMethod
		public void tearDown() throws Exception{
		try {
			BrowserFactory.quitBrowser();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
					
		public void assertion() throws Exception{
		try{
			assertion=Verify.action().verifyElementPresent(Hybris_dashboard.exts().verify_jobitemWindow, 5);
			if(assertion == false){
				report("f"," assertion is failed while verifying Job Items page should open.");
			}else{
				report("p","Passed on "+Hybris_Common_Properties_Cred.Hybris_Version_TestRail_comment);
			}	
				}catch (Throwable e) {
					report("f", "Execution level error was encountered in assertion() .\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				
				}
		}
		
		
			public void report(String result, String notes) throws Exception
			{
			TestRailClient.testRailReportByID_production(dataSet.get("TL_internal_testCase_ID"),Hybris_TestRail_Common_Properties.idTestPlan,Hybris_TestRail_Common_Properties.idBuild,result,Hybris_TestRail_Common_Properties.assignedTo,notes);
			if(result == "f")
				assertTrue(false);
			}	
}
