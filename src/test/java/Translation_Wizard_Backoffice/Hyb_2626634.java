package Translation_Wizard_Backoffice;

import static org.testng.Assert.assertTrue;

import java.util.LinkedHashMap;

import org.gs4tr.qa.testrail.framework.TestRailClient;
import org.gs4tr.qa.utility.BrowserFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import actions.General;
import actions.Hybris_Common_Properties_Cred;
import actions.Hybris_TestRail_Common_Properties;
import actions.Verify;
import actions.common;
import locators.Hybris_dashboard;

/***********************************************************************************************************************************
* Author        : Proteek
*
* Test case     : Verify that validation message is displayed when user tries to create new GlobalLink job with Name field as empty


* Test Id       : 2626634
* Summary   	: 
* Precondition  : GlobalLink adaptor should be deployed.
************************************************************************************************************************************/
public class Hyb_2626634 {
	
	Boolean assertion = true;
	LinkedHashMap<String, String> dataSet = new LinkedHashMap<String, String>();
	

	@BeforeMethod	
	public void setUp() throws Exception{
		BrowserFactory.SystemEngine().startApplication();
		// Write you Test case description here
				dataSet.put("TL_test_case_description","Hyb_2626634:Verify that validation message is displayed when user tries to create new GlobalLink job with Name field as empty");	
				dataSet.put("TL_internal_testCase_ID", "2626634");
	}	
		public void testcase(LinkedHashMap<String, String> dataSet) throws Exception{
			try{
					
				General.action().login();
				Thread.sleep(2000);
				common.action().search_Product(Hybris_Common_Properties_Cred.Title_catalog, Hybris_Common_Properties_Cred.Subtitle_products,
						  Hybris_Common_Properties_Cred.Product_Article_Num_1, Hybris_Common_Properties_Cred.Product_Catalog_Staged_Version_1 );
				common.action().click_On_TranlationIcon();
				
				assertion=Verify.action().verifyElementTextPresent(Hybris_dashboard.exts().createNewJob_header_Title, Hybris_Common_Properties_Cred.create_New_Job_Header_Title, 5);
				if(assertion == false){
					report("f"," assertion is failed while verifying  that \"Create New GlobalLink Job\" wizard should open.");
				}
				
				//click Next
				Verify.action().verifyElementPresent(Hybris_dashboard.exts().gl_Job_Nxt_Btn,5);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Job_Nxt_Btn).click();
				Thread.sleep(1000);
				
				//Clear Job Name and click on next button to verify validation msg
				Verify.action().verifyElementPresent(Hybris_dashboard.exts().gl_Job_ID,5);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Job_ID).clear();
				Thread.sleep(100);
				
				//click Next
				Verify.action().verifyElementPresent(Hybris_dashboard.exts().gl_Job_Nxt_Btn,5);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Job_Nxt_Btn).click();
				Thread.sleep(1000);
				
				}catch(Throwable e){
				report("f", "Execution level error was encountered.\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				}					
			}

		@Test
		public void execute() throws Exception{
			
			testcase(dataSet);
			assertion();
			
		}
			
		@AfterMethod
		public void tearDown() throws Exception{
		try {
			BrowserFactory.quitBrowser();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
					
		public void assertion() throws Exception{
		try{
			//verify that after clear job name and click on next button validation msg appears as a ""Please enter a job name." should be displayed"
			assertion=Verify.action().verifyElementPresent((Hybris_dashboard.exts().gl_createJobNameEmpty_Validation), 5);
			if(assertion == false){
				report("f"," assertion is failed while verifying  that \"Please enter a job name.\" should be displayed");
			}else{
				report("p","Passed on "+Hybris_Common_Properties_Cred.Hybris_Version_TestRail_comment);
			}	
				}catch (Throwable e) {
					report("f", "Execution level error was encountered in assertion() .\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				
				}
		}
		
		
			public void report(String result, String notes) throws Exception
			{
			TestRailClient.testRailReportByID_production(dataSet.get("TL_internal_testCase_ID"),Hybris_TestRail_Common_Properties.idTestPlan,Hybris_TestRail_Common_Properties.idBuild,result,Hybris_TestRail_Common_Properties.assignedTo,notes);
			if(result == "f")
				assertTrue(false);
			}	

}