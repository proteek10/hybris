package Translation_Wizard_Backoffice;

import static org.testng.Assert.assertTrue;


import java.util.LinkedHashMap;
import org.gs4tr.qa.testrail.framework.TestRailClient;
import org.gs4tr.qa.utility.BrowserFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import actions.General;
import actions.Hybris_Common_Properties_Cred;
import actions.Hybris_TestRail_Common_Properties;
import actions.Verify;
import actions.common;
import locators.Hybris_dashboard;

/********************************************************************************************************
* Author        : Praveen
*
* Test case     : Verify that when user changes source language, target language displayed also changes as per the configurations in PD
* Test Id       : 2626638
* Summary   	: 
* Precondition  : GlobalLink adaptor should be deployed.
**********************************************************************************************************/
public class Hyb_2626638 {
	
	Boolean assertion = true;
	LinkedHashMap<String, String> dataSet = new LinkedHashMap<String, String>();
	String SubmissionName = "HYB_2626638";
	
	@BeforeMethod	
	public void setUp() throws Exception{
		BrowserFactory.SystemEngine().startApplication();
		// Write you Test case description here
				dataSet.put("TL_test_case_description","Hyb_2626638:Verify that when user changes source language, target language displayed also changes as per the configurations in PD");
				dataSet.put("TL_internal_testCase_ID", "2626638");
	}	
		public void testcase(LinkedHashMap<String, String> dataSet) throws Exception{
			try{
					
				General.action().login();
				Thread.sleep(2000);
				common.action().search_Product(Hybris_Common_Properties_Cred.Title_catalog, Hybris_Common_Properties_Cred.Subtitle_products,
						  Hybris_Common_Properties_Cred.Product_Article_Num_1, Hybris_Common_Properties_Cred.Product_Catalog_Staged_Version_1 );
				common.action().click_On_TranlationIcon();
				Thread.sleep(1000);
				common.action().SelectProject_CreateJob(Hybris_Common_Properties_Cred.ProjectName_Hybris);
				Thread.sleep(1000);
				String text = BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().GL_Config_Inputs(Hybris_Common_Properties_Cred.jobConfig_Label_Source)).getAttribute("value");
				Thread.sleep(500);
				Assert.assertEquals("English", text);
				System.out.println("Source language is: "+ text);
				Thread.sleep(500);
				//Select Language
				Verify.action().verifyElementPresent(Hybris_dashboard.exts().select_Language(Hybris_Common_Properties_Cred.TargetLanguage_German),5);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_Language(Hybris_Common_Properties_Cred.TargetLanguage_German)).click();
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Job_Nxt_Btn).click();
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Job_Create_Btn).click();
				Thread.sleep(500);
			
				}catch(Throwable e){
				report("f", "Execution level error was encountered.\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				}
			}

		@Test
		public void execute() throws Exception{
			
			testcase(dataSet);
			assertion();
			
		}
			
		@AfterMethod
		public void tearDown() throws Exception{
		try {
			BrowserFactory.quitBrowser();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
					
		public void assertion() throws Exception{
		try{
			assertion=Verify.action().verifyTextPresent("1 item(s) queued for translation.",  5);
			if(assertion == false){
				report("f"," assertion is failed while verifying  that GlobalLink module is present");
			}else{
				report("p","Passed on "+Hybris_Common_Properties_Cred.Hybris_Version_TestRail_comment);
			}	
				}catch (Throwable e) {
					report("f", "Execution level error was encountered in assertion() .\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				
				}
		}
		
		
			public void report(String result, String notes) throws Exception
			{
			TestRailClient.testRailReportByID_production(dataSet.get("TL_internal_testCase_ID"),Hybris_TestRail_Common_Properties.idTestPlan,Hybris_TestRail_Common_Properties.idBuild,result,Hybris_TestRail_Common_Properties.assignedTo,notes);
			if(result == "f")
				assertTrue(false);
			}	

}