package WCMS_Backoffice;

import static org.testng.Assert.assertTrue;

import java.util.LinkedHashMap;

import org.gs4tr.qa.testrail.framework.TestRailClient;
import org.gs4tr.qa.utility.BrowserFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import actions.General;
import actions.Hybris_Common_Properties_Cred;
import actions.Hybris_TestRail_Common_Properties;
import actions.Verify;
import actions.common;
import locators.Hybris_dashboard;

/********************************************************************************************************
* Author        : Proteek
*
* Test case     : Verify that when a page is sent for translation, NavigationBarCollectionComponent together with 
* 				  all its nested components is also sent for translation
* Test Id       : 2626694
* Summary   	: 
* Precondition  : User should be logged in back office
				  Select a page in "Staged Catalog Version" for testing
				  There should be pages which have multiple content slots with components assigned to the slots
				  Pages with multiple navigation nodes assigned to them should be present
**********************************************************************************************************/
public class Hyb_2626694 {
	
	Boolean assertion = true;
	LinkedHashMap<String, String> dataSet = new LinkedHashMap<String, String>();
	
	String SubmissionName = "HYB_2626694";
	
	@BeforeMethod	
	public void setUp() throws Exception{
		BrowserFactory.SystemEngine().startApplication();
		// Write you Test case description here
		dataSet.put("TL_test_case_description","Hyb_2626694:Verify that when a page is sent for translation, NavigationBarCollectionComponent togheter with all its nested components is also sent for translation");
		dataSet.put("TL_internal_testCase_ID", "2626694");
	}	
		public void testcase(LinkedHashMap<String, String> dataSet) throws Exception{
			try{
					
				General.action().login();
				Thread.sleep(2000);
				common.action().serach_Multi_Pages(Hybris_Common_Properties_Cred.Title_wcms, Hybris_Common_Properties_Cred.Subtitle_page,false,
				Hybris_Common_Properties_Cred.Page_Attribute_Catalog_Version, Hybris_Common_Properties_Cred.Page_ApparelUK_Content_Cat_Staged, true,
				Hybris_Common_Properties_Cred.Page_Attribute_PageName, Hybris_Common_Properties_Cred.Page_Attribute_PageName_TermsConditions);
				common.action().create_Job_Multiple_Categories(true,2,Hybris_Common_Properties_Cred.ProjectName_Hybris, SubmissionName,true,
						Hybris_Common_Properties_Cred.TargetLanguage_German,true,false,1);
				common.action().search_Job(Hybris_Common_Properties_Cred.Title_globalLink,Hybris_Common_Properties_Cred.subtitle_jobs,SubmissionName);
				
				common.action().verify_Job_Status("","", Hybris_Common_Properties_Cred.Job_Status_Queued, SubmissionName);
				common.action().verify_Job_Status(Hybris_Common_Properties_Cred.subtitle_SubmitQuedJobs,"",Hybris_Common_Properties_Cred.Job_Status_inProgress, SubmissionName);
				Thread.sleep(7000);
				
				assertion=common.action().verify_Job_Status("",Hybris_Common_Properties_Cred.subtitle_FetchTranslations,Hybris_Common_Properties_Cred.Job_Status_Complete, SubmissionName);				
				if(assertion == false){
					report("f"," assertion is failed while verifying  that Job status Complete");
				}
				Thread.sleep(2000);
				
				//Go back to STEP 01 Search Terms and conditions and inside content slot verify translatable content translated or not
				
				common.action().serach_Multi_Pages(Hybris_Common_Properties_Cred.Title_wcms, Hybris_Common_Properties_Cred.Subtitle_page,false,
				Hybris_Common_Properties_Cred.Page_Attribute_Catalog_Version, Hybris_Common_Properties_Cred.Page_ApparelUK_Content_Cat_Staged, true,
				Hybris_Common_Properties_Cred.Page_Attribute_PageName, Hybris_Common_Properties_Cred.Page_Attribute_PageName_TermsConditions);
				
	  			General.action().doubleClick(Hybris_dashboard.exts().select_search_firstrow);
	  			Thread.sleep(1000);
	  			
	  			Verify.action().verifyElementPresent(Hybris_dashboard.exts().Edit_item_Select_Tab("Content Slots"),  5);
	  			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Edit_item_Select_Tab("Content Slots")).click();
				Thread.sleep(1000);
				
				General.action().doubleClick(Hybris_dashboard.exts().editeitem_content_slots("Section2B-TermsAndConditions"));
	  			Thread.sleep(1000);
				
	  			General.action().doubleClick(Hybris_dashboard.exts().editeitem_content_slots("Section2B Slot for Terms and Conditions Page"));
	  			Thread.sleep(1000);
				
	  			General.action().doubleClick(Hybris_dashboard.exts().editeitem_content_slots("Terms and Conditions Text Paragraph"));
	  			Thread.sleep(1000);
	  			
	  			//Click On Globe Icon
	  			Verify.action().verifyElementPresent(Hybris_dashboard.exts().content_globeIcon,  5);
	  			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().content_globeIcon).click();
				Thread.sleep(1000);
				
				String EnglishContent = BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().editeitem_content_source_slots_propertiesvalue("English")).getAttribute("value");
				System.out.println("English_Translable_Content--->"+EnglishContent);
		
				String GermanContent = BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().editeitem_content_target_slots_propertiesvalue("German")).getAttribute("value");
				System.out.println("German_Translable_Content--->"+GermanContent);
				
				if(!EnglishContent.equals(GermanContent))
				if(assertion == false){
					report("f"," assertion is failed while verifying  that GlobalLink module is present");
				}else{
					report("p","Passed on "+Hybris_Common_Properties_Cred.Hybris_Version_TestRail_comment);
				}	
				
				}catch(Throwable e){
				report("f", "Execution level error was encountered.\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				}
			}

		@Test
		public void execute() throws Exception{
			
			testcase(dataSet);
			
		}
			
		@AfterMethod
		public void tearDown() throws Exception{
		try {
			BrowserFactory.quitBrowser();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
		
			public void report(String result, String notes) throws Exception
			{
			TestRailClient.testRailReportByID_production(dataSet.get("TL_internal_testCase_ID"),Hybris_TestRail_Common_Properties.idTestPlan,Hybris_TestRail_Common_Properties.idBuild,result,Hybris_TestRail_Common_Properties.assignedTo,notes);
			if(result == "f")
				assertTrue(false);
			}	

}