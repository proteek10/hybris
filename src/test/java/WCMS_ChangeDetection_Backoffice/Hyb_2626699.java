package WCMS_ChangeDetection_Backoffice;

import static org.testng.Assert.assertTrue;

import java.util.LinkedHashMap;

import org.gs4tr.qa.testrail.framework.TestRailClient;
import org.gs4tr.qa.utility.BrowserFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import actions.General;
import actions.Hybris_Common_Properties_Cred;
import actions.Hybris_TestRail_Common_Properties;
import actions.Verify;
import actions.common;

/********************************************************************************************************************************************************************
* Author        : Proteek
*
* Test case     : Verify that user is able to create GlobalLink configuration job with change detection enabled for single target language on multiple navigation nodes
* Test Id       : 2626699
* Summary   	: 
* Precondition  : GlobalLink adaptor should be deployed.
*********************************************************************************************************************************************************************/
public class Hyb_2626699 {
	
	Boolean assertion = true;
	LinkedHashMap<String, String> dataSet = new LinkedHashMap<String, String>();
	
	String SubmissionName = "HYB_2626699";
	
	@BeforeMethod	
	public void setUp() throws Exception{
		BrowserFactory.SystemEngine().startApplication();
		// Write you Test case description here
				dataSet.put("TL_test_case_description","Hyb_2626699:Verify that user is able to create GlobalLink configuration job with change detection enabled for single target language on multiple navigation nodes");
				dataSet.put("TL_internal_testCase_ID", "2626699");
	}	
		public void testcase(LinkedHashMap<String, String> dataSet) throws Exception{
			try{
					
				General.action().login();
				Thread.sleep(2000);
				common.action().search_Page_ProductComparator(Hybris_Common_Properties_Cred.Title_wcms, Hybris_Common_Properties_Cred.Subtitle_navigation_node,
				Hybris_Common_Properties_Cred.Navigation_Node_Attribute_Catlog, Hybris_Common_Properties_Cred.Navigation_Node_Comparator_Value_Equals,Hybris_Common_Properties_Cred.Navigation_Node_Electronic_Content_Catalog_Staged, 
				Hybris_Common_Properties_Cred.Navigation_Node_ID, Hybris_Common_Properties_Cred.Navigation_Node_Comparator_Value_StartsWith, Hybris_Common_Properties_Cred.Navigation_Node_Name_Value_DigCamera);
				common.action().create_Job_Multiple_Categories(false,2,Hybris_Common_Properties_Cred.ProjectName_Hybris, SubmissionName,true,
						Hybris_Common_Properties_Cred.TargetLanguage_German,false,false,2);
				common.action().search_Job(Hybris_Common_Properties_Cred.Title_globalLink,Hybris_Common_Properties_Cred.subtitle_jobs,SubmissionName);
					
				}catch(Throwable e){
				report("f", "Execution level error was encountered.\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				}
			}

		@Test
		public void execute() throws Exception{
			
			testcase(dataSet);
			assertion();
			
		}
			
		@AfterMethod
		public void tearDown() throws Exception{
		try {
			BrowserFactory.quitBrowser();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
					
		public void assertion() throws Exception{
		try{
			assertion=common.action().verify_Job_Status("","", Hybris_Common_Properties_Cred.Job_Status_Queued, SubmissionName);
			if(assertion == false){
				report("f"," assertion is failed while verifying  that GlobalLink module is present");
			}else{
				report("p","Passed on "+Hybris_Common_Properties_Cred.Hybris_Version_TestRail_comment);
			}	
				}catch (Throwable e) {
					report("f", "Execution level error was encountered in assertion() .\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				
				}
		}
		
		
			public void report(String result, String notes) throws Exception
			{
			TestRailClient.testRailReportByID_production(dataSet.get("TL_internal_testCase_ID"),Hybris_TestRail_Common_Properties.idTestPlan,Hybris_TestRail_Common_Properties.idBuild,result,Hybris_TestRail_Common_Properties.assignedTo,notes);
			if(result == "f")
				assertTrue(false);
			}	

}
