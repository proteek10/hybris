package WCMS_ChangeDetection_Backoffice;

import static org.testng.Assert.assertTrue;

import java.util.LinkedHashMap;

import org.gs4tr.qa.testrail.framework.TestRailClient;
import org.gs4tr.qa.utility.BrowserFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import actions.General;
import actions.Hybris_Common_Properties_Cred;
import actions.Hybris_TestRail_Common_Properties;
import actions.Pmuser_client;
import actions.Verify;
import actions.common;
import locators.Hybris_dashboard;
import locators.PD4_main_client_dashboard_Locators;

/*****************************************************************************************************************************************************************
* Author        : Praveen
*
* Test case     : verify that a page with already translated content, can be resent for translation to a different target language – (Submit only new and changed content box ticked)
* Test Id       : 2626702
* Summary   	: 
* Precondition  : GlobalLink adaptor should be deployed.
				  GlobalLink Config should be done.
******************************************************************************************************************************************************************/
public class Hyb_2626702 {
	
	Boolean assertion = true;
	LinkedHashMap<String, String> dataSet = new LinkedHashMap<String, String>();
	
	String SubmissionName01 = "HYB_2626702_sub01";
	String SubmissionName02 = "HYB_2626702_sub02";
	
	@BeforeMethod	
	public void setUp() throws Exception{
		BrowserFactory.SystemEngine().startApplication();
		// Write you Test case description here
			dataSet.put("TL_test_case_description","Hyb_2626702: verify that a page with already translated content, can be resent for translation to a different target language – (Submit only new and changed content box ticked)");
				dataSet.put("TL_internal_testCase_ID", "2626702");
	}	
		public void testcase(LinkedHashMap<String, String> dataSet) throws Exception{
			try{
				BrowserFactory.SystemEngine();
				String originalHandle = BrowserFactory.driver.getWindowHandle();	
					
				General.action().login();
				Thread.sleep(2000);
				common.action().search_Page(Hybris_Common_Properties_Cred.Title_wcms, Hybris_Common_Properties_Cred.Subtitle_page,
						Hybris_Common_Properties_Cred.Page_Attribute_Catalog_Version, Hybris_Common_Properties_Cred.Page_ApparelUK_Content_Cat_Staged, 
						Hybris_Common_Properties_Cred.Page_Attribute_PageName, Hybris_Common_Properties_Cred.Page_Attribute_PageName_Search);
				Thread.sleep(1000);
				common.action().create_Job_Multiple_Categories(true,1,Hybris_Common_Properties_Cred.ProjectName_Hybris, SubmissionName01,true,
						Hybris_Common_Properties_Cred.TargetLanguage_German,true,false,1);
				Thread.sleep(500);
				common.action().search_Job(Hybris_Common_Properties_Cred.Title_globalLink,Hybris_Common_Properties_Cred.subtitle_jobs,SubmissionName01);
				Thread.sleep(1000);
				assertion=common.action().verify_Job_Status("","", Hybris_Common_Properties_Cred.Job_Status_Queued, SubmissionName01);
				if(assertion == false){
					report("f"," assertion is failed while verifying  that Job should be queued.");
				}
				Thread.sleep(2000);
				assertion=common.action().verify_Job_Status(Hybris_Common_Properties_Cred.subtitle_SubmitQuedJobs,"",
						Hybris_Common_Properties_Cred.Job_Status_inProgress, SubmissionName01);
				if(assertion == false){
					report("f"," assertion is failed while verifying  that Job should change to IN_PROGRESS");
				}
				
				//PD Login
				common.action().open_New_Instance(Hybris_Common_Properties_Cred.PD_URL);
				
				common.action().PDlogIn(Hybris_Common_Properties_Cred.PD_Config_UserName, Hybris_Common_Properties_Cred.PD_Config_Password);
				Thread.sleep(10000);
				Pmuser_client.action().filterAndWaitForStatus(SubmissionName01, "Completed", "Completed", 5);
				
				
				assertion =Verify.action().verifyElementPresent(PD4_main_client_dashboard_Locators.exts().previewMain_expandLevelLanguage("en-us⇒de-de"), 5);
				if(assertion == false){
				   report("f","Assertion failed while verifying the PD should contain a single job in single submission for all target languages");
				}	
				
				common.action().Close_ActiveTab(originalHandle);
				Thread.sleep(2000);
				common.action().switchTo_CurrentWindow();
				Thread.sleep(1000);
				assertion=common.action().verify_Job_Status("",Hybris_Common_Properties_Cred.subtitle_FetchTranslations,Hybris_Common_Properties_Cred.Job_Status_Complete, SubmissionName01);
				if(assertion == false){
					report("f"," assertion is failed while verifying  that Status of the GlobalLink job should change to Complete");
				}
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(Hybris_Common_Properties_Cred.Subtitle_page)).click();
	  			Thread.sleep(1000);
	  			common.action().create_Job_Multiple_Categories(true,1,Hybris_Common_Properties_Cred.ProjectName_Hybris, SubmissionName02,true,
						Hybris_Common_Properties_Cred.TargetLanguage_French,false,false,1);
				Thread.sleep(500);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(Hybris_Common_Properties_Cred.subtitle_SubmitQuedJobs)).click();
				Thread.sleep(1000);
				
				//PD Login
				common.action().open_New_Instance(Hybris_Common_Properties_Cred.PD_URL);
				
			//	common.action().PDlogIn(Hybris_Common_Properties_Cred.PD_Config_UserName, Hybris_Common_Properties_Cred.PD_Config_Password);
				Thread.sleep(10000);
				Pmuser_client.action().filterAndWaitForStatus(SubmissionName02, "Completed", "Completed", 5);
				
				
				assertion =Verify.action().verifyElementPresent(PD4_main_client_dashboard_Locators.exts().previewMain_expandLevelLanguage("en-us⇒fr-fr"), 5);
				if(assertion == false){
				   report("f","Assertion failed while verifying the PD should contain a single job in single submission for all target languages");
				}	
				
				common.action().Close_ActiveTab(originalHandle);
				Thread.sleep(2000);
				common.action().switchTo_CurrentWindow();
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(Hybris_Common_Properties_Cred.subtitle_jobs)).click();
				
				
				}catch(Throwable e){
				report("f", "Execution level error was encountered.\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				}
			}

		@Test
		public void execute() throws Exception{
			
			testcase(dataSet);
			assertion();
			
		}
			
		@AfterMethod
		public void tearDown() throws Exception{
		try {
			BrowserFactory.quitBrowser();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
					
		public void assertion() throws Exception{
		try{
			assertion=common.action().verify_Job_Status("",Hybris_Common_Properties_Cred.subtitle_FetchTranslations,Hybris_Common_Properties_Cred.Job_Status_Complete, SubmissionName02);
			if(assertion == false){
				report("f"," assertion is failed while verifying  that Status of the GlobalLink job should change to Complete");
			}
			
			else{
				report("p","Passed on "+Hybris_Common_Properties_Cred.Hybris_Version_TestRail_comment);
			}	
				}catch (Throwable e) {
					report("f", "Execution level error was encountered in assertion() .\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				
				}
		}
		
		
			public void report(String result, String notes) throws Exception
			{
			TestRailClient.testRailReportByID_production(dataSet.get("TL_internal_testCase_ID"),Hybris_TestRail_Common_Properties.idTestPlan,Hybris_TestRail_Common_Properties.idBuild,result,Hybris_TestRail_Common_Properties.assignedTo,notes);
			if(result == "f")
				assertTrue(false);
			}	

}