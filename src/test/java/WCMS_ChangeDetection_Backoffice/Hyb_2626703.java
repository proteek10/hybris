package WCMS_ChangeDetection_Backoffice;

import static org.testng.Assert.assertTrue;

import java.util.LinkedHashMap;

import org.gs4tr.qa.testrail.framework.TestRailClient;
import org.gs4tr.qa.utility.BrowserFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import actions.General;
import actions.Hybris_Common_Properties_Cred;
import actions.Hybris_TestRail_Common_Properties;
import actions.Pmuser_client;
import actions.Verify;
import actions.common;
import locators.Hybris_dashboard;
import locators.PD4_main_client_dashboard_Locators;

/********************************************************************************************************************************************************************
* Author        : Proteek
*
* Test case     : Verify that a page with already translated content (in all target languages), shall not be sent to PD for translation if user selects multiple target languages
* Test Id       : 2626703
* Summary   	: This test case is to Verify that a page with already translated content (in all target languages), shall not be sent to PD for translation if user selects multiple target languages 
* 				  – (Submit only new and changed content box ticked) with only changed content
* Precondition  : User should be logged in back office
				  GlobalLink adaptor should be configured
*********************************************************************************************************************************************************************/
public class Hyb_2626703 {
	
	Boolean assertion = true;
	LinkedHashMap<String, String> dataSet = new LinkedHashMap<String, String>();
	
	String SubmissionName_1 = "HYB_2626703_X1";
	String SubmissionName_2 = "HYB_2626703_X2";
	
	@BeforeMethod	
	public void setUp() throws Exception{
		BrowserFactory.SystemEngine().startApplication();
		// Write you Test case description here
				dataSet.put("TL_test_case_description","Hyb_2626703:Verify that a page with already translated content (in all target languages), shall not be sent to PD for translation if user selects multiple target languages");
				dataSet.put("TL_internal_testCase_ID", "2626703");
	}	
		public void testcase(LinkedHashMap<String, String> dataSet) throws Exception{
			try{
					
				General.action().login();
				Thread.sleep(2000);
				//Submission Name 1 with "Submit only new and changed content" box UNTICKED 
				common.action().search_Page(Hybris_Common_Properties_Cred.Title_wcms, Hybris_Common_Properties_Cred.Subtitle_page,
				Hybris_Common_Properties_Cred.Page_Attribute_Catalog_Version,Hybris_Common_Properties_Cred.Page_ApparelUK_Content_Cat_Staged, 
				Hybris_Common_Properties_Cred.Page_Attribute_PageName,Hybris_Common_Properties_Cred.NavigationNode_Page_Attribute_PageName_ProductGrid);
				Thread.sleep(2000);
				common.action().create_Job_Multiple_Categories(true,1,Hybris_Common_Properties_Cred.ProjectName_Hybris, SubmissionName_1,false,
				"",true,false,1);
				common.action().search_Job(Hybris_Common_Properties_Cred.Title_globalLink,Hybris_Common_Properties_Cred.subtitle_jobs,SubmissionName_1);
				
						
				assertion = common.action().verify_Job_Status(Hybris_Common_Properties_Cred.subtitle_SubmitQuedJobs,"", Hybris_Common_Properties_Cred.Job_Status_Complete, SubmissionName_1);
				if(assertion == false){
					report("f"," assertion is failed while verifying  that GlobalLink job should change to IN_PROGRESS");
				}
				
				assertion=common.action().verify_Job_Status("",Hybris_Common_Properties_Cred.subtitle_FetchTranslations,Hybris_Common_Properties_Cred.Job_Status_Complete, SubmissionName_1);
				if(assertion == false){
					report("f"," assertion is failed while verifying  that GlobalLink job should change to COMPLETE");
				}
				
				
				BrowserFactory.driver.navigate().refresh();
				Thread.sleep(6000);
				
				//Submission Name 2 with "Submit only new and changed content" box TICKED 
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(Hybris_Common_Properties_Cred.Title_wcms)).click();
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(Hybris_Common_Properties_Cred.Subtitle_page)).click();
				Thread.sleep(1000);
				common.action().create_Job_Multiple_Categories(true,1,Hybris_Common_Properties_Cred.ProjectName_Hybris, SubmissionName_2,false,
				"",false,false,1);
				
				BrowserFactory.driver.navigate().refresh();
				Thread.sleep(6000);
				
				common.action().search_Job(Hybris_Common_Properties_Cred.Title_globalLink,Hybris_Common_Properties_Cred.subtitle_jobs,SubmissionName_2);
				
				assertion = common.action().verify_Job_Status(Hybris_Common_Properties_Cred.subtitle_SubmitQuedJobs,"", Hybris_Common_Properties_Cred.Job_Status_inProgress, SubmissionName_2);
				if(assertion == false){
					report("f"," assertion is failed while verifying  that GlobalLink module is present");
				}
				
				assertion=common.action().verify_Job_Status("",Hybris_Common_Properties_Cred.subtitle_FetchTranslations,Hybris_Common_Properties_Cred.Job_Status_Complete, SubmissionName_2);
				if(assertion == false){
					report("f"," assertion is failed while verifying  that GlobalLink job should change to COMPLETE");
				}
				
				common.action().Open_New_Instance_URL(Hybris_Common_Properties_Cred.PD_URL);
				
				common.action().PDlogIn(Hybris_Common_Properties_Cred.PD_Config_UserName, Hybris_Common_Properties_Cred.PD_Config_Password);
				Thread.sleep(2000);
				
				assertion=Pmuser_client.action().filterAndWaitForStatus(SubmissionName_1, "Completed", "Delivered", 5);
				if(assertion == false){
					report("f"," assertion is failed while verifying  that There should be two different submissions for the same product if it is submitted twice");
				}
				
				Verify.action().verifyElementPresent(PD4_main_client_dashboard_Locators.exts().workflows_completedSubmissionsButton,10);
				BrowserFactory.SystemEngine().findElement(PD4_main_client_dashboard_Locators.exts().workflows_completedSubmissionsButton).click();
				Thread.sleep(2000);
				Pmuser_client.action().filterByFirstFilterType(SubmissionName_2);
				Thread.sleep(1000);
				
				
				}catch(Throwable e){
				report("f", "Execution level error was encountered.\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				}
			}

		@Test
		public void execute() throws Exception{
			
			testcase(dataSet);
			assertion();
			
		}
			
		@AfterMethod
		public void tearDown() throws Exception{
		try {
			BrowserFactory.quitBrowser();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
					
		public void assertion() throws Exception{
		try {	
			assertion=	Verify.action().verifyElementTextNotPresent(PD4_main_client_dashboard_Locators.exts().firstDataRowName,SubmissionName_2 , 5); 
			if(assertion == false){
				report("f"," assertion is failed while verifying  that There There should be NO submissions made in PD");
			}else{
				report("p","Passed on "+Hybris_Common_Properties_Cred.Hybris_Version_TestRail_comment);
			}	
				}catch (Throwable e) {
					report("f", "Execution level error was encountered in assertion() .\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				
				}
		}
		
		
			public void report(String result, String notes) throws Exception
			{
			TestRailClient.testRailReportByID_production(dataSet.get("TL_internal_testCase_ID"),Hybris_TestRail_Common_Properties.idTestPlan,Hybris_TestRail_Common_Properties.idBuild,result,Hybris_TestRail_Common_Properties.assignedTo,notes);
			if(result == "f")
				assertTrue(false);
			}	

}
