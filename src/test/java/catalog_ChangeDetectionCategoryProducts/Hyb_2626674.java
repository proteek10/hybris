package catalog_ChangeDetectionCategoryProducts;

import static org.testng.Assert.assertTrue;

import java.util.LinkedHashMap;
import java.util.List;

import org.gs4tr.qa.testrail.framework.TestRailClient;
import org.gs4tr.qa.utility.BrowserFactory;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import actions.General;
import actions.Hybris_Common_Properties_Cred;
import actions.Hybris_TestRail_Common_Properties;
import actions.Pmuser_client;
import actions.Verify;
import actions.common;
import locators.Hybris_dashboard;
import locators.PD4_main_client_dashboard_Locators;

/*****************************************************************************************************************************************************************
* Author        : Praveen
*
* Test case     : verify that a category with already translated content (in all target languages), shall not be sent to PD for translation if user selects multiple target languages – (Submit only new and changed content box ticked)
* Test Id       : 2626674
* Summary   	: 
* Precondition  : GlobalLink adaptor should be deployed.
				  GlobalLink Config should be done.
******************************************************************************************************************************************************************/
public class Hyb_2626674 {
	
	Boolean assertion = true;
	LinkedHashMap<String, String> dataSet = new LinkedHashMap<String, String>();
	
	String SubmissionName1 = "Hyb_2626674_01";
	String SubmissionName2 = "Hyb_2626674_02";
	
	@BeforeMethod	
	public void setUp() throws Exception{
		BrowserFactory.SystemEngine().startApplication();
		// Write you Test case description here
			dataSet.put("TL_test_case_description","Hyb_2626674: verify that a category with already translated content (in all target languages), shall not be sent to PD for translation if user selects multiple target languages – (Submit only new and changed content box ticked)");
				dataSet.put("TL_internal_testCase_ID", "2626674");
	}	
		public void testcase(LinkedHashMap<String, String> dataSet) throws Exception{
			try{
		
				General.action().login();
				Thread.sleep(2000);
				
				common.action().search_Category(Hybris_Common_Properties_Cred.Title_catalog, Hybris_Common_Properties_Cred.subtitle_categories,
						true, true, Hybris_Common_Properties_Cred.Catalog_Categories_Attribute_Binoculars, 
						Hybris_Common_Properties_Cred.Catalog_categries_Electronics_Product_Catalog_Staged);
				Thread.sleep(1000);
				common.action().create_Job_Multiple_Categories(true,2,Hybris_Common_Properties_Cred.ProjectName_Hybris, SubmissionName1,false,
						Hybris_Common_Properties_Cred.TargetLanguage_German,true,false,1);
				Thread.sleep(500);
				
				common.action().search_Job(Hybris_Common_Properties_Cred.Title_globalLink,Hybris_Common_Properties_Cred.subtitle_jobs,SubmissionName1);
				Thread.sleep(2000);
				assertion=common.action().verify_Job_Status("","", Hybris_Common_Properties_Cred.Job_Status_Queued, SubmissionName1);
				if(assertion == false){
					report("f"," assertion is failed while verifying  that Job should be queued.");
				}
				Thread.sleep(2000);
				assertion=common.action().verify_Job_Status(Hybris_Common_Properties_Cred.subtitle_SubmitQuedJobs,"", Hybris_Common_Properties_Cred.Job_Status_inProgress, SubmissionName1);
				if(assertion == false){
					report("f"," assertion is failed while verifying  that Job status inprogress");
				}
				Thread.sleep(2000);
				assertion=common.action().verify_Job_Status("",Hybris_Common_Properties_Cred.subtitle_FetchTranslations, Hybris_Common_Properties_Cred.Job_Status_Complete, SubmissionName1);
				if(assertion == false){
					report("f"," assertion is failed while verifying  that Job status Complete");
				}
				Thread.sleep(2000);
				
				//CREATE 2nd SUBMISSION
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(Hybris_Common_Properties_Cred.subtitle_categories)).click();
				Thread.sleep(1000);
				create_Job_Multiple_Categories(true,2,Hybris_Common_Properties_Cred.ProjectName_Hybris, SubmissionName2,true,
						Hybris_Common_Properties_Cred.TargetLanguage_German,Hybris_Common_Properties_Cred.TargetLanguage_French,false,false,1);
				Thread.sleep(500);
				
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(Hybris_Common_Properties_Cred.Title_globalLink)).click();
				common.action().search_Job(Hybris_Common_Properties_Cred.Title_globalLink,Hybris_Common_Properties_Cred.subtitle_jobs,SubmissionName2);
				Thread.sleep(2000);
				
				}catch(Throwable e){
				report("f", "Execution level error was encountered.\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				}
			}

		@Test
		public void execute() throws Exception{
			
			testcase(dataSet);
			assertion();
			
		}
			
		@AfterMethod
		public void tearDown() throws Exception{
		try {
			BrowserFactory.quitBrowser();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
					
		public void assertion() throws Exception{
		try{
			assertion=common.action().verify_Job_Status(Hybris_Common_Properties_Cred.subtitle_SubmitQuedJobs,"", Hybris_Common_Properties_Cred.Job_Status_Complete, SubmissionName2);
			if(assertion == false){
				report("f"," assertion is failed while verifying  that Job status Complete");
			}else{
				report("p","Passed on "+Hybris_Common_Properties_Cred.Hybris_Version_TestRail_comment);
			}	
				}catch (Throwable e) {
					report("f", "Execution level error was encountered in assertion() .\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				
				}
		}
		
		
			public void report(String result, String notes) throws Exception
			{
			TestRailClient.testRailReportByID_production(dataSet.get("TL_internal_testCase_ID"),Hybris_TestRail_Common_Properties.idTestPlan,Hybris_TestRail_Common_Properties.idBuild,result,Hybris_TestRail_Common_Properties.assignedTo,notes);
			if(result == "f")
				assertTrue(false);
			}	
			
			public void create_Job_Multiple_Categories(boolean selectItem,int num,String ProjectName,String SubmissionName,boolean lang,String Lang1,String Lang2,boolean submit_onlyNew,
		  			boolean trans_prod_feature,int items) throws Exception {
				//Translate Icon
		  		if(selectItem) {
		  			Verify.action().verifyElementPresent(Hybris_dashboard.exts().select_search_firstrow,5);
		  			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_search_firstrow).click();
		  			Thread.sleep(1000);
		  			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().translate_icon).click();
		  			Thread.sleep(1000);
		  		}
		  		else {
		  			List<WebElement> els = BrowserFactory.SystemEngine().findElements(Hybris_dashboard.exts().select_Multiple_Products);
		  			Thread.sleep(1000);
		  			for(int i=0;i<num;i++)
		  			{
		  				els.get(i).click();
		  				Thread.sleep(500);
		  			}
		  			Thread.sleep(1000);
		  			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().WithoutSelectItem_translate_icon).click();
		  			Thread.sleep(1000);
		  		}
				//Select Project
				common.action().SelectProject_CreateJob(ProjectName);
				//Select Profile
				common.action().select_Profile_CreateJob(Hybris_Common_Properties_Cred.profileName_Default);
				Thread.sleep(1000);
				Verify.action().verifyElementPresent(Hybris_dashboard.exts().gl_Job_ID,5);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Job_ID).clear();
				Thread.sleep(500);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Job_ID).sendKeys(SubmissionName);
				Thread.sleep(2000);
				//Select Language
				if(lang) {
					BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_Language(Lang1)).click();
					Thread.sleep(1000);
					BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_Language(Lang2)).click();
					Thread.sleep(1000);
				}
				else {
					//Select Multiple Language
					Verify.action().verifyElementPresent(Hybris_dashboard.exts().clickOn_Lang_CheckBox,5);
					BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().clickOn_Lang_CheckBox).click();
					Thread.sleep(1000);
				}
				//Select check box for Submit Only new changed content and translate prod feature
				if(submit_onlyNew){
					if(BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().checkbox_submitonly_newcontent).isSelected()==true)
					{
						BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().checkbox_submitonly_newcontent_uncheck).click();
						Thread.sleep(2000);
						System.out.println("--------->Submit Only new changed content");
					}
				}
				if(trans_prod_feature){
					if(BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().checkbox_translate_prod_feature).isSelected()==true)
					{
						BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().checkbox_translate_prod_feature_uncheck).click();
						Thread.sleep(2000);
						System.out.println("--------->translate prod feature");
					}
				}
					
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Job_Nxt_Btn).click();
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Job_Create_Btn).click();
				Thread.sleep(5000);
				
				Verify.action().verifyTextPresent(items+"item(s) queued for translation.", 5);
				System.out.println("Queued Job Values---------->"+items);
				
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Job_Done_Btn).click();
				Thread.sleep(500);
			
			} 

}
