package catalog_ChangeDetectionCategoryProducts;

import static org.testng.Assert.assertTrue;

import java.util.LinkedHashMap;
import java.util.List;

import org.gs4tr.qa.testrail.framework.TestRailClient;
import org.gs4tr.qa.utility.BrowserFactory;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import actions.General;
import actions.Hybris_Common_Properties_Cred;
import actions.Hybris_TestRail_Common_Properties;
import actions.Pmuser_client;
import actions.Verify;
import actions.common;
import locators.Hybris_dashboard;
import locators.PD4_main_client_dashboard_Locators;

/*****************************************************************************************************************************************************************
* Author        : Praveen
*
* Test case     : Verify that a product with changed content can be sent for translation – (Submit only new and changed content box ticked)
* Test Id       : 2626677
* Summary   	: 
* Precondition  : GlobalLink adaptor should be deployed.
				  GlobalLink Config should be done.
******************************************************************************************************************************************************************/
public class Hyb_2626677 {
	
	Boolean assertion = true;
	LinkedHashMap<String, String> dataSet = new LinkedHashMap<String, String>();
	
	String SubmissionName = "Hyb_2626677";
	String articleNum = "2626677";
	String articleNum_edit = "2626677_edit";
	
	@BeforeMethod	
	public void setUp() throws Exception{
		BrowserFactory.SystemEngine().startApplication();
		// Write you Test case description here
			dataSet.put("TL_test_case_description","Hyb_2626677: Verify that a product with changed content can be sent for translation – (Submit only new and changed content box ticked)");
				dataSet.put("TL_internal_testCase_ID", "2626677");
	}	
		public void testcase(LinkedHashMap<String, String> dataSet) throws Exception{
			try{	
				General.action().login();
				Thread.sleep(2000);
				//CREATE PRODUCT
				General.action().createItem(Hybris_Common_Properties_Cred.Title_catalog, Hybris_Common_Properties_Cred.Subtitle_products, articleNum, 
						Hybris_Common_Properties_Cred.Catalog_categries_Electronics_Product_Catalog_Staged, Hybris_Common_Properties_Cred.newPage_ApprovalStatus);
				Thread.sleep(2000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(Hybris_Common_Properties_Cred.Title_catalog)).click();
				Thread.sleep(1000);
				common.action().search_Multiple_Products(Hybris_Common_Properties_Cred.Title_catalog, Hybris_Common_Properties_Cred.Subtitle_products,true,
						articleNum,true, Hybris_Common_Properties_Cred.Catalog_categries_Electronics_Product_Catalog_Staged);
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_search_firstrow).click();
				Thread.sleep(2000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().product_ArticleNumber_Input).clear();
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().product_ArticleNumber_Input).sendKeys(articleNum_edit);
				Thread.sleep(2000);
	  			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().editItem_Save_Button).click();
	  			Thread.sleep(3000);
				common.action().create_Job_Multiple_Categories(true,2,Hybris_Common_Properties_Cred.ProjectName_Hybris, SubmissionName,false,
						Hybris_Common_Properties_Cred.TargetLanguage_German,false,false,1);
				Thread.sleep(500);
				
				common.action().search_Job(Hybris_Common_Properties_Cred.Title_globalLink,Hybris_Common_Properties_Cred.subtitle_jobs,SubmissionName);
				Thread.sleep(2000);
				assertion=common.action().verify_Job_Status("","", Hybris_Common_Properties_Cred.Job_Status_Queued, SubmissionName);
				if(assertion == false){
					report("f"," assertion is failed while verifying  that Job should be queued.");
				}
				Thread.sleep(2000);
				assertion=common.action().verify_Job_Status(Hybris_Common_Properties_Cred.subtitle_SubmitQuedJobs,"", Hybris_Common_Properties_Cred.Job_Status_inProgress, SubmissionName);
				if(assertion == false){
					report("f"," assertion is failed while verifying  that Job status inprogress");
				}
				Thread.sleep(2000);
				
				//STEP 07 PD LOGIN
				common.action().open_New_Instance(Hybris_Common_Properties_Cred.PD_URL);
				
				common.action().PDlogIn(Hybris_Common_Properties_Cred.PD_Config_UserName, Hybris_Common_Properties_Cred.PD_Config_Password);
				Thread.sleep(5000);
				Pmuser_client.action().filterAndWaitForStatus(SubmissionName, "Completed", "Completed", 5);
				Thread.sleep(2000);
				BrowserFactory.SystemEngine().findElement(PD4_main_client_dashboard_Locators.exts().firstDataRowName).click();
				Thread.sleep(1000);
				assertion =Verify.action().verifyElementPresent(PD4_main_client_dashboard_Locators.exts().previewMain_expandLevelLanguage("en-us⇒de-de"), 5);
				if(assertion == false){
				   report("f","Assertion failed while verifying the PD should contain a single job in single submission for all target languages");
				}
				
				}catch(Throwable e){
				report("f", "Execution level error was encountered.\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				}
			}

		@Test
		public void execute() throws Exception{
			
			testcase(dataSet);
			assertion();
			
		}
			
		@AfterMethod
		public void tearDown() throws Exception{
		try {
			BrowserFactory.quitBrowser();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
					
		public void assertion() throws Exception{
		try{
			assertion =Verify.action().verifyElementPresent(PD4_main_client_dashboard_Locators.exts().previewMain_expandLevelLanguage("en-us⇒fr-fr"), 5);
			if(assertion == false){
			   report("f","Assertion failed while verifying the PD should contain a single job in single submission for all target languages");
			}  else{
				report("p","Passed on "+Hybris_Common_Properties_Cred.Hybris_Version_TestRail_comment);
			}	
				}catch (Throwable e) {
					report("f", "Execution level error was encountered in assertion() .\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				
				}
		}
		
		
			public void report(String result, String notes) throws Exception
			{
			TestRailClient.testRailReportByID_production(dataSet.get("TL_internal_testCase_ID"),Hybris_TestRail_Common_Properties.idTestPlan,Hybris_TestRail_Common_Properties.idBuild,result,Hybris_TestRail_Common_Properties.assignedTo,notes);
			if(result == "f")
				assertTrue(false);
			}	

}
