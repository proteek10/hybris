package catalog_ChangeDetectionCategoryProducts;

import static org.testng.Assert.assertTrue;

import java.util.LinkedHashMap;

import org.gs4tr.qa.testrail.framework.TestRailClient;
import org.gs4tr.qa.utility.BrowserFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import actions.General;
import actions.Hybris_Common_Properties_Cred;
import actions.Hybris_TestRail_Common_Properties;
import actions.Pmuser_client;
import actions.Verify;
import actions.common;
import locators.Hybris_dashboard;

/*****************************************************************************************************************************************************************
* Author        : Proteek
*
* Test case     : Verify that a product with already translated content (in all target languages), shall not be sent to PD for translation 
* 				  if user selects multiple target languages
* Test Id       : 2626679
* Summary   	: 
* Precondition  : User should be logged in backoffice
				  GlobalLink adaptor should be configured
				  Multiple target languages should be configured say e.g. German, French and Spanish
******************************************************************************************************************************************************************/
public class Hyb_2626679 {
	
	Boolean assertion = true;
	LinkedHashMap<String, String> dataSet = new LinkedHashMap<String, String>();
	
	String SubmissionName1 = "Hyb_2626679_X1";
	String SubmissionName2 = "Hyb_2626679_X2";
	
	@BeforeMethod	
	public void setUp() throws Exception{
		BrowserFactory.SystemEngine().startApplication();
		// Write you Test case description here
			dataSet.put("TL_test_case_description","Hyb_2626679: Verify that a product with already translated content (in all target languages), shall not be sent to PD for translation if user selects multiple target languages");
				dataSet.put("TL_internal_testCase_ID", "2626679");
	}	
		public void testcase(LinkedHashMap<String, String> dataSet) throws Exception{
			try{
					
				General.action().login();
				Thread.sleep(2000);
				
				common.action().search_Multiple_Products(Hybris_Common_Properties_Cred.Title_catalog, Hybris_Common_Properties_Cred.Subtitle_products,true,
						Hybris_Common_Properties_Cred.Product_Article_Num_2,true, Hybris_Common_Properties_Cred.Catalog_categries_Electronics_Product_Catalog_Staged);
				Thread.sleep(1000);
				common.action().create_Job_Multiple_Categories(true,1,Hybris_Common_Properties_Cred.ProjectName_Hybris, SubmissionName1,false,
						Hybris_Common_Properties_Cred.TargetLanguage_German,true,true,1);
				Thread.sleep(2000);
				common.action().search_Job(Hybris_Common_Properties_Cred.Title_globalLink,Hybris_Common_Properties_Cred.subtitle_jobs,SubmissionName1);
				Thread.sleep(2000);
				assertion=common.action().verify_Job_Status("","", Hybris_Common_Properties_Cred.Job_Status_Queued, SubmissionName1);
				if(assertion == false){
					report("f"," assertion is failed while verifying  that Job should be queued.");
				}
				Thread.sleep(2000);
				assertion=common.action().verify_Job_Status(Hybris_Common_Properties_Cred.subtitle_SubmitQuedJobs,"", Hybris_Common_Properties_Cred.Job_Status_inProgress, SubmissionName1);
				if(assertion == false){
					report("f"," assertion is failed while verifying  that Job status inprogress");
				}
				Thread.sleep(2000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(Hybris_Common_Properties_Cred.Subtitle_products)).click();
				Thread.sleep(2000);
				
				//Submission 02
				
				common.action().create_Job_Multiple_Categories(true,1,Hybris_Common_Properties_Cred.ProjectName_Hybris, SubmissionName2,false,
						Hybris_Common_Properties_Cred.TargetLanguage_German,false,false,1);
				Thread.sleep(500);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(Hybris_Common_Properties_Cred.Title_globalLink)).click();
				Thread.sleep(1000);
				common.action().search_Job(Hybris_Common_Properties_Cred.Title_globalLink,Hybris_Common_Properties_Cred.subtitle_jobs,SubmissionName2);
				Thread.sleep(2000);
				assertion=common.action().verify_Job_Status("","", Hybris_Common_Properties_Cred.Job_Status_Queued, SubmissionName2);
				if(assertion == false){
					report("f"," assertion is failed while verifying  that Job should be queued.");
				}
				Thread.sleep(2000);
				assertion=common.action().verify_Job_Status(Hybris_Common_Properties_Cred.subtitle_SubmitQuedJobs,"", Hybris_Common_Properties_Cred.Job_Status_inProgress, SubmissionName2);
				if(assertion == false){
					report("f"," assertion is failed while verifying  that Job status inprogress");
				}
				Thread.sleep(1000);
			
				assertion=common.action().Verify_WaitUntil_JobStatus(SubmissionName2,Hybris_Common_Properties_Cred.Job_Status_inProgress);
				if(assertion == false){
					report("f"," assertion is failed while verifying  that Job should be queued.");
				}
				
				common.action().open_New_Instance(Hybris_Common_Properties_Cred.PD_URL);
				Thread.sleep(1000);
				common.action().PDlogIn(Hybris_Common_Properties_Cred.PD_Config_UserName, Hybris_Common_Properties_Cred.PD_Config_Password);
				Thread.sleep(2000);
				
				}catch(Throwable e){
				report("f", "Execution level error was encountered.\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				}
			}

		@Test
		public void execute() throws Exception{
			
			testcase(dataSet);
			assertion();
			
		}
			
		@AfterMethod
		public void tearDown() throws Exception{
		try {
			BrowserFactory.quitBrowser();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
					
		public void assertion() throws Exception{
		try{
			assertion=Pmuser_client.action().filterAndWaitForStatus(SubmissionName2, "Completed", "Completed", 5);
			if(assertion == false){
				report("f"," assertion is failed while verifying  that Job should be queued.");
			}else{
				report("p","Passed on "+Hybris_Common_Properties_Cred.Hybris_Version_TestRail_comment);
			}	
				}catch (Throwable e) {
					report("f", "Execution level error was encountered in assertion() .\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				
				}
		}
		
		
			public void report(String result, String notes) throws Exception
			{
			TestRailClient.testRailReportByID_production(dataSet.get("TL_internal_testCase_ID"),Hybris_TestRail_Common_Properties.idTestPlan,Hybris_TestRail_Common_Properties.idBuild,result,Hybris_TestRail_Common_Properties.assignedTo,notes);
			if(result == "f")
				assertTrue(false);
			}	

}
