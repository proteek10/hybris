package configuration_Feature_TranslationTab_Backoffice;

import static org.testng.Assert.assertTrue;

import java.util.LinkedHashMap;

import org.gs4tr.qa.testrail.framework.TestRailClient;
import org.gs4tr.qa.utility.BrowserFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import actions.General;
import actions.Hybris_Common_Properties_Cred;
import actions.Hybris_TestRail_Common_Properties;
import actions.Verify;
import actions.common;
import locators.Hybris_dashboard;

/*****************************************************************************************************************************************************************
* Author        : Proteek
*
* Test case     : Verify that no category features are saved when user selects certain categories for assignments and then clicks on “x” icon
* Test Id       : 2626856
* Summary   	: 
* Precondition  : User should be logged in backoffice
				  GlobalLink Adaptor should be configured
******************************************************************************************************************************************************************/
public class Hyb_2626856 {
	
	Boolean assertion = true;
	LinkedHashMap<String, String> dataSet = new LinkedHashMap<String, String>();
	
	String Value = "Environmental conditions [1196]";
	
	String SubmissionName = "Hyb_2626856";
	
	@BeforeMethod	
	public void setUp() throws Exception{
		BrowserFactory.SystemEngine().startApplication();
		// Write you Test case description here
			dataSet.put("TL_test_case_description","Hyb_2626856: Verify that no category features are saved when user selects certain categories for assignments and then clicks on “x” icon");
				dataSet.put("TL_internal_testCase_ID", "2626856");
	}	
		public void testcase(LinkedHashMap<String, String> dataSet) throws Exception{
			try{
					
				General.action().login();
				Thread.sleep(2000);
				common.action().Go_toFeatureTab_Search(Hybris_Common_Properties_Cred.Title_globalLink, Hybris_Common_Properties_Cred.subtitle_Config,true, false,
				Hybris_Common_Properties_Cred.Classifying_Category, Hybris_Common_Properties_Cred.Page_Comparator_Value_Equals, Hybris_Common_Properties_Cred.Environmentalconditions_1196);
				Thread.sleep(2000);
				//Click on Tree view Button		
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Config_Treeview_Btn).click();
				Thread.sleep(1000);
				
				Verify.action().verifyElementPresent(Hybris_dashboard.exts().gl_Config_treeview_verifyCategory(Value),  5);
				if(assertion == false){
					report("f"," assertion is failed while verifying All the search results should be listed in \"Tree view\" and the results should be displayed");
				}
				
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Config_treeview_SelectCategory(Value)).click();
				Thread.sleep(1000);
				
				Verify.action().verifyElementPresent(Hybris_dashboard.exts().gl_Config_view_PlusIcon,  5);
				if(assertion == false){
					report("f"," assertion is failed while verifying All the search results should be listed in \"Tree view\" and the results should be displayed");
				}
				
				//Click on Close Assignment Window
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Config_CloseAssignmentWindow_X).click();
				Thread.sleep(2000);
				
				Verify.action().verifyElementNotPresent(Hybris_dashboard.exts().gl_Config_AddNewAssignment,  5);
				if(assertion == false){
					report("f"," assertion is failed while verifying Add assignment wizard should close and there should be no \"Feature\" displayed on main Feature translation tab of GlobalLink configuration page");
				}
				
			}
				catch(Throwable e){
				report("f", "Execution level error was encountered.\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				}
			}

		@Test
		public void execute() throws Exception{
			
			testcase(dataSet);
			assertion();
			
		}
			
		@AfterMethod
		public void tearDown() throws Exception{
		try {
			BrowserFactory.quitBrowser();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
					
		public void assertion() throws Exception{
		try{
			Verify.action().verifyElementNotPresent(Hybris_dashboard.exts().gl_Config_treeview_verifyCategory(Value),  5);
			if(assertion == false){
				report("f"," assertion is failed while verifying Add assignment wizard should close and there should be no \"Feature\" displayed on main Feature translation tab of GlobalLink configuration page");
			}else{
				report("p","Passed on "+Hybris_Common_Properties_Cred.Hybris_Version_TestRail_comment);
			}	
				}catch (Throwable e) {
					report("f", "Execution level error was encountered in assertion() .\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				
				}
		}
		
		
			public void report(String result, String notes) throws Exception
			{
			TestRailClient.testRailReportByID_production(dataSet.get("TL_internal_testCase_ID"),Hybris_TestRail_Common_Properties.idTestPlan,Hybris_TestRail_Common_Properties.idBuild,result,Hybris_TestRail_Common_Properties.assignedTo,notes);
			if(result == "f")
				assertTrue(false);
			}	

}

