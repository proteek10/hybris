package configuration_Feature_TranslationTab_Backoffice;

import static org.testng.Assert.assertTrue;

import java.util.LinkedHashMap;

import org.gs4tr.qa.testrail.framework.TestRailClient;
import org.gs4tr.qa.utility.BrowserFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import actions.General;
import actions.Hybris_Common_Properties_Cred;
import actions.Hybris_TestRail_Common_Properties;
import actions.Verify;
import actions.common;
import locators.Hybris_dashboard;

/*****************************************************************************************************************************************************************
* Author        : Praveen
*
* Test case     : Verify if user is able to add and save category features with exclude only listed features selected under Feature Translation tab 
* Test Id       : 2626858
* Summary   	: 
* Precondition  : GlobalLink adaptor should be deployed.
				  GlobalLink Config should be done.
******************************************************************************************************************************************************************/
public class Hyb_2626858 {
	
	Boolean assertion = true;
	LinkedHashMap<String, String> dataSet = new LinkedHashMap<String, String>();
	String value = "3674";
	String features = "";
	String expected = "[Shutter priority AE, 3674]";
	
	@BeforeMethod	
	public void setUp() throws Exception{
		BrowserFactory.SystemEngine().startApplication();
		// Write you Test case description here
			dataSet.put("TL_test_case_description","Hyb_2626858: Verify if user is able to add and save category features with exclude only listed features selected under Feature Translation tab  ");
				dataSet.put("TL_internal_testCase_ID", "2626858");
	}	
		public void testcase(LinkedHashMap<String, String> dataSet) throws Exception{
			try{
					
				General.action().login();
				Thread.sleep(2000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(Hybris_Common_Properties_Cred.Title_globalLink)).click();
		  		Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(Hybris_Common_Properties_Cred.subtitle_Config)).click();
				Thread.sleep(3000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().pd_URL).click();
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Config_Labels(Hybris_Common_Properties_Cred.feature_Translation_Tab)).click();
				Thread.sleep(500);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Config_FeatureTrans_ExcludeFeatures).click();
				Thread.sleep(500);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Config_Add_Button).click();
				Thread.sleep(500);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().AddNewAssignment_Feature_Descriptor).sendKeys(value);
				Thread.sleep(500);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_CatalogVersion(Hybris_Common_Properties_Cred.addNewAssignment_FeatureDescriptor_ShutterPriority)).click();
				Thread.sleep(500);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().addNewAssignment_SearchBtn).click();
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().add_NewAssignment_SelectBox).click();
				Thread.sleep(500);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().add_NewAssignment_AddIcon).click();
				Thread.sleep(2000);
				features = BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Config_FeatureTrans_Features).getText();
				Thread.sleep(500);
				if(features.contains(expected)) {
					System.out.println("Features are added successfully");
				}
				Thread.sleep(500);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().save_Password).click();
				Thread.sleep(2000);
			}
				catch(Throwable e){
				report("f", "Execution level error was encountered.\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				}
			}

		@Test
		public void execute() throws Exception{
			
			testcase(dataSet);
			assertion();
			
		}
			
		@AfterMethod
		public void tearDown() throws Exception{
		try {
			BrowserFactory.quitBrowser();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
					
		public void assertion() throws Exception{
		try{
			assertion=Verify.action().verifyTextPresent(expected, 5);
			if(assertion == false){
				report("f"," assertion is failed while verifying Features");
			}else{
				report("p","Passed on "+Hybris_Common_Properties_Cred.Hybris_Version_TestRail_comment);
			}	
				}catch (Throwable e) {
					report("f", "Execution level error was encountered in assertion() .\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				
				}
		}
		
		
			public void report(String result, String notes) throws Exception
			{
			TestRailClient.testRailReportByID_production(dataSet.get("TL_internal_testCase_ID"),Hybris_TestRail_Common_Properties.idTestPlan,Hybris_TestRail_Common_Properties.idBuild,result,Hybris_TestRail_Common_Properties.assignedTo,notes);
			if(result == "f")
				assertTrue(false);
			}	

}

