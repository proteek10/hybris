package configuration_Feature_TranslationTab_Backoffice;

import static org.testng.Assert.assertTrue;

import java.util.LinkedHashMap;

import org.gs4tr.qa.testrail.framework.TestRailClient;
import org.gs4tr.qa.utility.BrowserFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import actions.Download;
import actions.General;
import actions.Hybris_Common_Properties_Cred;
import actions.Hybris_TestRail_Common_Properties;
import actions.Pmuser_client;
import actions.Verify;
import actions.common;
import locators.Hybris_dashboard;
import locators.PD4_main_client_dashboard_Locators;

/*****************************************************************************************************************************************************************
* Author        : Praveen
*
* Test case     : Verify that GlobalLink adaptor can allow configuration of a list of product features which should not be sent for translation
* Test Id       : 2626863
* Summary   	: 
* Precondition  : User should be logged in backoffice
				  GlobalLink Adaptor should be configured
******************************************************************************************************************************************************************/
public class Hyb_2626863 {
	
	Boolean assertion = true;
	LinkedHashMap<String, String> dataSet = new LinkedHashMap<String, String>();
	String features = "";
	String value = "622";
	String expected = "[Print technology, 78]";
	String Product_feature_label1  = "Print technology";
	String SubmissionName = "HYB_2626863";
	String SubmissionName_01 = "HYB_2626863_01";
	
	@BeforeMethod	
	public void setUp() throws Exception{
		BrowserFactory.SystemEngine().startApplication();
		// Write you Test case description here
			dataSet.put("TL_test_case_description","Hyb_2626863: Verify that GlobalLink adaptor can allow configuration of a list of product features which should not be sent for translation");
				dataSet.put("TL_internal_testCase_ID", "2626863");
	}	
		public void testcase(LinkedHashMap<String, String> dataSet) throws Exception{
			try{
			//	Download.action().deleteFiles_From_Download();
				Thread.sleep(1000);
				BrowserFactory.SystemEngine();
				String originalHandle = BrowserFactory.driver.getWindowHandle();
					
				General.action().login();
				Thread.sleep(2000);
				
				//Select Include option and send submission
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(Hybris_Common_Properties_Cred.Title_globalLink)).click();
		  		Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(Hybris_Common_Properties_Cred.subtitle_Config)).click();
				Thread.sleep(3000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().pd_URL).click();
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Config_Labels(Hybris_Common_Properties_Cred.feature_Translation_Tab)).click();
				Thread.sleep(500);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Config_FeatureTrans_IncludeFeatures).click();
				Thread.sleep(500);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Config_Add_Button).click();
				Thread.sleep(500);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().AddNewAssignment_Classifying_Category).sendKeys(value);
				Thread.sleep(500);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_CatalogVersion(Hybris_Common_Properties_Cred.addNewAssignment_ClassifyngCat)).click();
				Thread.sleep(500);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().addNewAssignment_SearchBtn).click();
				Thread.sleep(2000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().add_NewAssignment_SelectBox).click();
				Thread.sleep(500);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().add_NewAssignment_AddIcon).click();
				Thread.sleep(2000);
				features = BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Config_FeatureTrans_Features).getText();
				Thread.sleep(500);
				if(features.contains(expected)) {
					System.out.println("Features are added successfully");
				}
				Thread.sleep(500);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().save_Password).click();
				Thread.sleep(6000);
				
				//Create Submission
				common.action().search_Multiple_Products(Hybris_Common_Properties_Cred.Title_catalog, Hybris_Common_Properties_Cred.Subtitle_products,true,
						Hybris_Common_Properties_Cred.Product_Article_Num_6,true, Hybris_Common_Properties_Cred.Catalog_categries_Electronics_Product_Catalog_Staged);
				Thread.sleep(1000);
				common.action().create_Job_Multiple_Categories(true,1,Hybris_Common_Properties_Cred.ProjectName_Hybris, SubmissionName,true,
						Hybris_Common_Properties_Cred.TargetLanguage_German,true,false,1);
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(Hybris_Common_Properties_Cred.Title_globalLink)).click();
				Thread.sleep(1000);
				common.action().search_Job(Hybris_Common_Properties_Cred.Title_globalLink,Hybris_Common_Properties_Cred.subtitle_jobs,SubmissionName);
				Thread.sleep(1000);
				assertion=common.action().verify_Job_Status("","", Hybris_Common_Properties_Cred.Job_Status_Queued, SubmissionName);
				if(assertion == false){
					report("f"," assertion is failed while verifying  that Job should be queued.");
				}
				Thread.sleep(2000);
				assertion=common.action().verify_Job_Status(Hybris_Common_Properties_Cred.subtitle_SubmitQuedJobs,"",Hybris_Common_Properties_Cred.Job_Status_inProgress, SubmissionName);
				if(assertion == false){
					report("f"," assertion is failed while verifying  that Status of the GlobalLink job should change to \"IN_PROGRESS\"");
				}
				Thread.sleep(2000);
				
				//PD Login
				common.action().open_New_Instance(Hybris_Common_Properties_Cred.PD_URL);
				Thread.sleep(1000);
				common.action().PDlogIn(Hybris_Common_Properties_Cred.PD_Config_UserName, Hybris_Common_Properties_Cred.PD_Config_Password);
				Thread.sleep(2000);
				Pmuser_client.action().filterAndWaitForStatus(SubmissionName, "Completed", "Completed", 5);
				
				assertion =Verify.action().verifyElementPresent(PD4_main_client_dashboard_Locators.exts().previewMain_expandLevelLanguage("en-us⇒de-de"), 5);
				if(assertion == false){
				   report("f","Assertion failed while verifying the PD should contain a single job in single submission for all target languages");
				}
				Thread.sleep(500);
				BrowserFactory.SystemEngine().findElement(PD4_main_client_dashboard_Locators.exts().manageTab).click();
				Thread.sleep(500);
				BrowserFactory.SystemEngine().findElement(PD4_main_client_dashboard_Locators.exts().firstDataRowName).click();
				Thread.sleep(500);
				BrowserFactory.SystemEngine().findElement(PD4_main_client_dashboard_Locators.exts().download_SourceFile).click();
				Thread.sleep(10000);
				BrowserFactory.SystemEngine().findElement(PD4_main_client_dashboard_Locators.exts().download_Link).click();
				Thread.sleep(20000); 	
				assertion=Download.action().verify_XML_Node_Data(Product_feature_label1,Product_feature_label1);
				if(assertion == false){
					report("f"," assertion is failed while verifying Product Feature");
				}
				Thread.sleep(6000);
				common.action().Close_ActiveTab(originalHandle);  
				Thread.sleep(2000);
				common.action().switchTo_CurrentWindow();
				Thread.sleep(1000);
				//Clean download file form directory
				Download.action().deleteFiles_From_Download();
				
				//Select Exclude option and send submission
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(Hybris_Common_Properties_Cred.Title_globalLink)).click();
		  		Thread.sleep(1000);
		  		BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(Hybris_Common_Properties_Cred.Title_globalLink)).click();
		  		Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(Hybris_Common_Properties_Cred.subtitle_Config)).click();
				Thread.sleep(3000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().pd_URL).click();
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Config_Labels(Hybris_Common_Properties_Cred.feature_Translation_Tab)).click();
				Thread.sleep(500);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Config_FeatureTrans_ExcludeFeatures).click();
				Thread.sleep(500);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Config_Add_Button).click();
				Thread.sleep(500);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().AddNewAssignment_Classifying_Category).sendKeys(value);
				Thread.sleep(500);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_CatalogVersion(Hybris_Common_Properties_Cred.addNewAssignment_ClassifyngCat)).click();
				Thread.sleep(500);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().addNewAssignment_SearchBtn).click();
				Thread.sleep(2000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().add_NewAssignment_SelectBox).click();
				Thread.sleep(500);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().add_NewAssignment_AddIcon).click();
				Thread.sleep(2000);
				features = BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().gl_Config_FeatureTrans_Features).getText();
				Thread.sleep(500);
				if(features.contains(expected)) {
					System.out.println("Features are added successfully");
					}
				Thread.sleep(500);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().save_Password).click();
				Thread.sleep(10000);
				
				//Create Submission again
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(Hybris_Common_Properties_Cred.Subtitle_products)).click();
				Thread.sleep(1000);
				common.action().create_Job_Multiple_Categories(true,1,Hybris_Common_Properties_Cred.ProjectName_Hybris, SubmissionName_01,true,
						Hybris_Common_Properties_Cred.TargetLanguage_German,true,false,1);
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(Hybris_Common_Properties_Cred.Title_globalLink)).click();
				Thread.sleep(1000);
				common.action().search_Job(Hybris_Common_Properties_Cred.Title_globalLink,Hybris_Common_Properties_Cred.subtitle_jobs,SubmissionName_01);
				Thread.sleep(1000);
				assertion=common.action().verify_Job_Status("","", Hybris_Common_Properties_Cred.Job_Status_Queued, SubmissionName_01);
				if(assertion == false){
					report("f"," assertion is failed while verifying  that Job should be queued.");
				}
				Thread.sleep(2000);
				assertion=common.action().verify_Job_Status(Hybris_Common_Properties_Cred.subtitle_SubmitQuedJobs,"",Hybris_Common_Properties_Cred.Job_Status_inProgress, SubmissionName);
				if(assertion == false){
					report("f"," assertion is failed while verifying  that Status of the GlobalLink job should change to \"IN_PROGRESS\"");
				}
				
				//PD Login again
				common.action().open_New_Instance(Hybris_Common_Properties_Cred.PD_URL);
				Thread.sleep(10000);
				Pmuser_client.action().filterAndWaitForStatus(SubmissionName_01, "Completed", "Completed", 5);
				
				assertion =Verify.action().verifyElementPresent(PD4_main_client_dashboard_Locators.exts().previewMain_expandLevelLanguage("en-us⇒de-de"), 5);
				if(assertion == false){
				   report("f","Assertion failed while verifying the PD should contain a single job in single submission for all target languages");
				}
				Thread.sleep(500);
				BrowserFactory.SystemEngine().findElement(PD4_main_client_dashboard_Locators.exts().manageTab).click();
				Thread.sleep(500);
				BrowserFactory.SystemEngine().findElement(PD4_main_client_dashboard_Locators.exts().firstDataRowName).click();
				Thread.sleep(500);
				BrowserFactory.SystemEngine().findElement(PD4_main_client_dashboard_Locators.exts().download_SourceFile).click();
				Thread.sleep(10000);
				BrowserFactory.SystemEngine().findElement(PD4_main_client_dashboard_Locators.exts().download_Link).click();
				Thread.sleep(8000);
				
				if(assertion == false){
					report("f"," assertion is failed while verifying Add assignment wizard should close and there should be no \"Feature\" displayed on main Feature translation tab of GlobalLink configuration page");
				}
				
			}
				catch(Throwable e){
				report("f", "Execution level error was encountered.\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				}
			}

		@Test
		public void execute() throws Exception{
			
			testcase(dataSet);
			assertion();
			
		}
			
		@AfterMethod
		public void tearDown() throws Exception{
		try {
			BrowserFactory.quitBrowser();
			Thread.sleep(2000);
			Download.action().deleteFiles_From_Download();
			Thread.sleep(1000);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
					
		public void assertion() throws Exception{
		try{
			assertion=Download.action().verify_XML_Node_Data_Not_Exist(Product_feature_label1);
			if(assertion == false){
				report("f"," assertion failed while verifying Product Feature not present");
			}else{
				report("p","Passed on "+Hybris_Common_Properties_Cred.Hybris_Version_TestRail_comment);
			}	
				}catch (Throwable e) {
					report("f", "Execution level error was encountered in assertion() .\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				
				}
		}
		
		
			public void report(String result, String notes) throws Exception
			{
			TestRailClient.testRailReportByID_production(dataSet.get("TL_internal_testCase_ID"),Hybris_TestRail_Common_Properties.idTestPlan,Hybris_TestRail_Common_Properties.idBuild,result,Hybris_TestRail_Common_Properties.assignedTo,notes);
			if(result == "f")
				assertTrue(false);
			}	

}

