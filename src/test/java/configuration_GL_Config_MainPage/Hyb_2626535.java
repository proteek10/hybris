package configuration_GL_Config_MainPage;

import static org.testng.Assert.assertTrue;

import java.util.LinkedHashMap;

import org.gs4tr.qa.testrail.framework.TestRailClient;
import org.gs4tr.qa.utility.BrowserFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import actions.General;
import actions.Hybris_Common_Properties_Cred;
import actions.Hybris_TestRail_Common_Properties;
import actions.Verify;
import actions.common;
import locators.Hybris_dashboard;

/******************************************************************************************************************************************************************************
* Author        : Proteek
*
* Test case     : Verify that validation messages are displayed while creating new GlobalLink job configuration when there are no valid source-target language pairs configured
* Test Id       : 2626535
* Summary   	: 
* Precondition  : GlobalLink adaptor should be deployed.
				  GlobalLink PD configuration should be present.
				  There should be no Language Mappings present in GlobalLink PD configuration.

*******************************************************************************************************************************************************************************/
public class Hyb_2626535 {
	
	Boolean assertion = true;
	LinkedHashMap<String, String> dataSet = new LinkedHashMap<String, String>();
	
	
	@BeforeMethod	
	public void setUp() throws Exception{
		BrowserFactory.SystemEngine().startApplication();
		// Write you Test case description here
			dataSet.put("TL_test_case_description","Hyb_2626535: Verify that validation messages are displayed while creating new GlobalLink job configuration when there are no valid source-target language pairs configured");
				dataSet.put("TL_internal_testCase_ID", "2626535");
	}	
		public void testcase(LinkedHashMap<String, String> dataSet) throws Exception{
			try{
					
				General.action().login();
				Thread.sleep(2000);
				common.action().search_Product(Hybris_Common_Properties_Cred.Title_catalog, Hybris_Common_Properties_Cred.Subtitle_products, "29533", 
				Hybris_Common_Properties_Cred.Catalog_categories_Apparel_Product_Catalog_Staged);
				
				Verify.action().verifyElementPresent(Hybris_dashboard.exts().select_search_firstrow,5);
	  			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_search_firstrow).click();
	  			Thread.sleep(3000);
	  			
				
				}catch(Throwable e){
				report("f", "Execution level error was encountered.\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				}
			}

		@Test
		public void execute() throws Exception{
			
			testcase(dataSet);
			assertion();
			
		}
			
		@AfterMethod
		public void tearDown() throws Exception{
		try {
			BrowserFactory.quitBrowser();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
					
		public void assertion() throws Exception{
		try{
			assertion=Verify.action().verifyElementNotPresent(Hybris_dashboard.exts().translate_icon,5);
			if(assertion == false){
				report("f"," assertion is failed while verifying  that Translate icon should be disabled..");
			}else{
				report("p","Passed on "+Hybris_Common_Properties_Cred.Hybris_Version_TestRail_comment);
			}	
				}catch (Throwable e) {
					report("f", "Execution level error was encountered in assertion() .\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				
				}
		}
		
		
			public void report(String result, String notes) throws Exception
			{
			TestRailClient.testRailReportByID_production(dataSet.get("TL_internal_testCase_ID"),Hybris_TestRail_Common_Properties.idTestPlan,Hybris_TestRail_Common_Properties.idBuild,result,Hybris_TestRail_Common_Properties.assignedTo,notes);
			if(result == "f")
				assertTrue(false);
			}	

}