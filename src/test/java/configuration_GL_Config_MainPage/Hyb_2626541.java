package configuration_GL_Config_MainPage;
import static org.testng.Assert.assertTrue;

import java.io.FileReader;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import org.gs4tr.qa.testrail.framework.TestRailClient;
import org.gs4tr.qa.utility.BrowserFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.opencsv.CSVReader;

import actions.General;
import actions.Hybris_Common_Properties_Cred;
import actions.Hybris_TestRail_Common_Properties;
import actions.Verify;
import actions.common;
import locators.Hybris_dashboard;

/********************************************************************************************************
* Author        : Praveen
*
* Test case     : Verify that the configuration page is exported successfully in csv format
* Test Id       : 2626541
* Summary   	: 
* Precondition  : GlobalLink adaptor should be deployed.
**********************************************************************************************************/
public class Hyb_2626541 {
	
	Boolean assertion = true;
	LinkedHashMap<String, String> dataSet = new LinkedHashMap<String, String>();
	String filePath = "";
	
	@BeforeMethod	
	public void setUp() throws Exception{
		BrowserFactory.SystemEngine().startApplication();
		// Write you Test case description here
				dataSet.put("TL_test_case_description","Hyb_2626541:Verify that the configuration page is exported successfully in csv format");
				dataSet.put("TL_internal_testCase_ID", "2626541");
	}	
		public void testcase(LinkedHashMap<String, String> dataSet) throws Exception{
			try{
		//		Download.action().deleteFiles_From_Download();
				Thread.sleep(2000);
				General.action().login();
				Thread.sleep(2000);
			//	common.action().switchTo_Cockpit_Mode(Hybris_Common_Properties_Cred.subtitle_adaptive_Search);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(Hybris_Common_Properties_Cred.Title_globalLink)).click();
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().Dashboard_side_menus(Hybris_Common_Properties_Cred.subtitle_Config)).click();
				Thread.sleep(2000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().search_Button).click();
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().csv_Icon).click();
				Thread.sleep(2000);
				
				}catch(Throwable e){
				report("f", "Execution level error was encountered.\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				}
			}

		@Test
		public void execute() throws Exception{
			
			testcase(dataSet);
			assertion();
			
		}
			
		@AfterMethod
		public void tearDown() throws Exception{
		try {
			BrowserFactory.quitBrowser();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
					
		public void assertion() throws Exception{
		try{
			assertion=verifyCSVFile(Hybris_Common_Properties_Cred.PD_Config_URL);
			if(assertion == false){
				report("f"," assertion failed while verifying PD_Config_URL present in csv file");
			}else{
				report("p","Passed on "+Hybris_Common_Properties_Cred.Hybris_Version_TestRail_comment);
			}	
				}catch (Throwable e) {
					report("f", "Execution level error was encountered in assertion() .\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				
				}
		}
		
		
			public void report(String result, String notes) throws Exception
			{
			TestRailClient.testRailReportByID_production(dataSet.get("TL_internal_testCase_ID"),Hybris_TestRail_Common_Properties.idTestPlan,Hybris_TestRail_Common_Properties.idBuild,result,Hybris_TestRail_Common_Properties.assignedTo,notes);
			if(result == "f")
				assertTrue(false);
			}	
			
			public boolean verifyCSVFile(String pdConfigURL) throws Exception{
				boolean result = false;
				filePath = common.action().getFile_Name("list");
				CSVReader reader = new CSVReader(new FileReader(filePath));

				  List<String[]> list=reader.readAll();
				  System.out.println("Total rows which we have is "+list.size());
				            
				 // create Iterator reference
				  Iterator<String[]>iterator= list.iterator();
				    
				 // Iterate all values 
				 while(iterator.hasNext()){
				     
				 String[] str=iterator.next();
				   
				 System.out.print(" Values are ");

				 for(int i=0;i<str.length;i++)
				{

				   System.out.print(" "+str[i]);
				   Thread.sleep(500);
				   if(str[i].contains(pdConfigURL)) {
					   result = true;
				   }

				}
				   System.out.println("   ");
				 }
				 reader.close();
				 Thread.sleep(1000);
				 return result;
			}

}
