package execute_ATLAST;

import static org.testng.Assert.assertTrue;

import java.util.LinkedHashMap;

import org.gs4tr.qa.testrail.framework.TestRailClient;
import org.gs4tr.qa.utility.BrowserFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import actions.Download;
import actions.General;
import actions.Hybris_Common_Properties_Cred;
import actions.Hybris_TestRail_Common_Properties;
import actions.Verify;
import actions.common;

/*****************************************************************************************************************************************************************
* Author        : Proteek
*
* Test case     : Verify that when user clicks on “GlobalLink” link in left hand explorer section, all the GlobalLink jobs created earlier are listed

* Test Id       : 2626734
* Summary   	: 
* Precondition  : GlobalLink adaptor should be deployed.
				  CMS Manager should be added to "GlobalLink User Group [globallinkusergroup]" group.
				  Some GlobalLink jobs should be created earlier
******************************************************************************************************************************************************************/
public class Hyb_2626734 {
	
	Boolean assertion = true;
	LinkedHashMap<String, String> dataSet = new LinkedHashMap<String, String>();

	
	@BeforeMethod	
	public void setUp() throws Exception{
		BrowserFactory.SystemEngine().startApplication();
		// Write you Test case description here
			dataSet.put("TL_test_case_description","Hyb_2626734: Verify that if the GlobalLink job is created from GlobalProduct Cockpit, then the requestor name displayed is “Product Manager [productmanager]");
				dataSet.put("TL_internal_testCase_ID", "2626734");
	}	
		public void testcase(LinkedHashMap<String, String> dataSet) throws Exception{
			try{
				Thread.sleep(1000);
				BrowserFactory.SystemEngine();		
				General.action().login();
				Thread.sleep(2000);
				
		
				common.action().search_Job(Hybris_Common_Properties_Cred.Title_globalLink,Hybris_Common_Properties_Cred.subtitle_jobs,"");
				Thread.sleep(1000);
				
			}
				catch(Throwable e){
				report("f", "Execution level error was encountered.\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				}
			}

		@Test
		public void execute() throws Exception{
			
			testcase(dataSet);
			assertion();
			
		}
			
		@AfterMethod
		public void tearDown() throws Exception{
		try {
			BrowserFactory.quitBrowser();
			Thread.sleep(2000);
			Download.action().deleteFiles_From_Download();
			Thread.sleep(1000);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
					
		public void assertion() throws Exception{
		try{
			assertion= Verify.action().verifyTextPresent("productmanager", 5);
			if(assertion == false){
				report("f"," assertion failed while verifying All the GlobalLink jobs created earlier should be listed on this page.");
			}else{
				report("p","Passed on "+Hybris_Common_Properties_Cred.Hybris_Version_TestRail_comment);
			}	
				}catch (Throwable e) {
					report("f", "Execution level error was encountered in assertion() .\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				
				}
		}
		
		
			public void report(String result, String notes) throws Exception
			{
			TestRailClient.testRailReportByID_production(dataSet.get("TL_internal_testCase_ID"),Hybris_TestRail_Common_Properties.idTestPlan,Hybris_TestRail_Common_Properties.idBuild,result,Hybris_TestRail_Common_Properties.assignedTo,notes);
			if(result == "f")
				assertTrue(false);
			}	

}

