package functionalTesting_GL_CMS_Cockpit;
import static org.testng.Assert.assertTrue;

import java.util.LinkedHashMap;

import org.gs4tr.qa.testrail.framework.TestRailClient;
import org.gs4tr.qa.utility.BrowserFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import actions.General;
import actions.GlobalProductCockpit_Common_Prop_Cred;
import actions.Hybris_Common_Properties_Cred;
import actions.Hybris_TestRail_Common_Properties;
import actions.Verify;
import actions.globalProductCockpit;
import locators.Hybris_GlobalProductCockpit;

/*********************************************************************************************************************************************
* Author        : Praveen
*
* Test case     : Verify that user is able to create new GlobalLink job by clicking on translation icon on selected page

* Test Id       : 2626794
* Summary   	: 
* Precondition  : GlobalLink adaptor should be deployed.
				  CMS Manager should be added to "GlobalLink User Group [globallinkusergroup]" group.
***********************************************************************************************************************************************/
public class Hyb_2626794 {
	
	Boolean assertion = true;
	LinkedHashMap<String, String> dataSet = new LinkedHashMap<String, String>();
	String SubmissionName = "Hyb_2626794";

	@BeforeMethod	
	public void setUp() throws Exception{
		BrowserFactory.SystemEngine().startApplication();
		// Write you Test case description here
				dataSet.put("TL_test_case_description","Hyb_2626794: Verify that user is able to create new GlobalLink job by clicking on translation icon on selected page");
				dataSet.put("TL_internal_testCase_ID", "2626794");
	}	
	
		public void testcase(LinkedHashMap<String, String> dataSet) throws Exception{
			try{
					
				General.action().login();
				globalProductCockpit.action().open_New_Instance_globalProduct_Cockpit(GlobalProductCockpit_Common_Prop_Cred.gpc_cms_URL);
				
				globalProductCockpit.action().globalCMS_Cockpit_login();

				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().selectCMS_SideMenu(GlobalProductCockpit_Common_Prop_Cred.csm_Apparel_Site_UK)).click();	
				Thread.sleep(2000);
				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().selectCMS_SideMenu(GlobalProductCockpit_Common_Prop_Cred.cms_Apparel_UK_Content_Catalog_Staged)).click();
				Thread.sleep(2000);
				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().searchPage_Input).sendKeys(GlobalProductCockpit_Common_Prop_Cred.page_HomePage);
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().searchProduct_Icon).click();
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().searchProduct_Icon).click();
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().clickOn_Product(GlobalProductCockpit_Common_Prop_Cred.page_HomePage)).click();
				Thread.sleep(3000);
				globalProductCockpit.action().create_Job_From_Product(true, 3, GlobalProductCockpit_Common_Prop_Cred.ProjectName_Hybris, GlobalProductCockpit_Common_Prop_Cred.profileName_Default,
						SubmissionName, true, GlobalProductCockpit_Common_Prop_Cred.German, false, false, 1);
				Thread.sleep(2000);
				globalProductCockpit.action().search_Created_Job(GlobalProductCockpit_Common_Prop_Cred.menu_GlobalLink, SubmissionName);
				
				}catch(Throwable e){
				report("f", "Execution level error was encountered.\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				}
			}

		@Test
		public void execute() throws Exception{
			
			testcase(dataSet);
			assertion();
			
		}
			
		@AfterMethod
		public void tearDown() throws Exception{
		try {
			BrowserFactory.quitBrowser();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
					
		public void assertion() throws Exception{
		try{
			assertion= globalProductCockpit.action().verify_Job_Status(GlobalProductCockpit_Common_Prop_Cred.Job_Status_Queued);
			if(assertion == false){
				report("f","assertion failed while verifying job status queued");
			}else{
				report("p","Passed on "+Hybris_Common_Properties_Cred.Hybris_Version_TestRail_comment);
			}	
				}catch (Throwable e) {
					report("f", "Execution level error was encountered in assertion() .\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				
				}
		}
		
		
			public void report(String result, String notes) throws Exception
			{
			TestRailClient.testRailReportByID_production(dataSet.get("TL_internal_testCase_ID"),Hybris_TestRail_Common_Properties.idTestPlan,Hybris_TestRail_Common_Properties.idBuild,result,Hybris_TestRail_Common_Properties.assignedTo,notes);
			if(result == "f")
				assertTrue(false);
			}	

}