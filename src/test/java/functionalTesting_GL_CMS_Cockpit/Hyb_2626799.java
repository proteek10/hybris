package functionalTesting_GL_CMS_Cockpit;

import static org.testng.Assert.assertTrue;

import java.util.LinkedHashMap;

import org.gs4tr.qa.testrail.framework.TestRailClient;
import org.gs4tr.qa.utility.BrowserFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import actions.General;
import actions.GlobalProductCockpit_Common_Prop_Cred;
import actions.Hybris_Common_Properties_Cred;
import actions.Hybris_TestRail_Common_Properties;
import actions.Verify;
import actions.globalProductCockpit;
import locators.Hybris_GlobalProductCockpit;

/************************************************************************************************************************************************************
* Author        : Proteek
*
* Test case     : Verify that user is able to create a GlobalLink job for navigation nodes
* Test Id       : 2626799
* Summary   	: 
* Precondition  : GlobalLink adaptor should be deployed.
				  GlobalLink Config should be done.
				  User should be logged into globalcms cockpit (https://10.10.222.12:9002/globalcmscockpit/index.zul)
***************************************************************************************************************************************************************/
public class Hyb_2626799 {
	
	Boolean assertion = true;
	LinkedHashMap<String, String> dataSet = new LinkedHashMap<String, String>();
	
	
	@BeforeMethod	
	public void setUp() throws Exception{
		BrowserFactory.SystemEngine().startApplication();
		// Write you Test case description here
				dataSet.put("TL_test_case_description","Hyb_2626799:Verify that user is able to create a GlobalLink job for navigation nodes");
				dataSet.put("TL_internal_testCase_ID", "2626799");
	}	
		public void testcase(LinkedHashMap<String, String> dataSet) throws Exception{
			try{
					
				General.action().login();
				globalProductCockpit.action().open_New_Instance_globalProduct_Cockpit(GlobalProductCockpit_Common_Prop_Cred.gpc_cms_URL);
				
				globalProductCockpit.action().globalCMS_Cockpit_login();
				
				
				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().selectMenu("Navigation")).click();
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().selectCMS_SideMenu_Siteroot("Apparel Site UK")).click();
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().selectCMS_SideMenu("Apparel UK Content Catalog / Staged")).click();
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().Select_SiteRoot).click();
				Thread.sleep(1000);	
				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().Open_Select_SiteRoot).click();
				Thread.sleep(1000);			
				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().Select_Open_Apparel_UK_Site).click();
				Thread.sleep(1000);		
				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().Select_MyAccount).click();
				Thread.sleep(1000);		
				globalProductCockpit.action().create_Job_SiteRoot_ApparelUKSite("My Account",false, 1, GlobalProductCockpit_Common_Prop_Cred.ProjectName_Hybris, GlobalProductCockpit_Common_Prop_Cred.profileName_Default,
						 false, "", false, false, 1,GlobalProductCockpit_Common_Prop_Cred.menu_GlobalLink);
				Thread.sleep(2000);
				
				}catch(Throwable e){
				report("f", "Execution level error was encountered.\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				}
			}

		@Test
		public void execute() throws Exception{
			
			testcase(dataSet);
			assertion();
			
		}
			
		@AfterMethod
		public void tearDown() throws Exception{
		try {
			BrowserFactory.quitBrowser();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
					
		public void assertion() throws Exception{
		try{
			assertion= globalProductCockpit.action().verify_Job_Status(GlobalProductCockpit_Common_Prop_Cred.Job_Status_Queued);
			if(assertion == false){
				report("f","assertio failed while verifying Created GlobalLink job should be listed on this page with status as \"QUEUED\" automatically without having user to click on search.");
			}else{
				report("p","Passed on "+Hybris_Common_Properties_Cred.Hybris_Version_TestRail_comment);
			}	
				}catch (Throwable e) {
					report("f", "Execution level error was encountered in assertion() .\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				
				}
		}
		
		
			public void report(String result, String notes) throws Exception
			{
			TestRailClient.testRailReportByID_production(dataSet.get("TL_internal_testCase_ID"),Hybris_TestRail_Common_Properties.idTestPlan,Hybris_TestRail_Common_Properties.idBuild,result,Hybris_TestRail_Common_Properties.assignedTo,notes);
			if(result == "f")
				assertTrue(false);
			}	

}

