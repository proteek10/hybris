package functionalTesting_GL_CMS_Cockpit;

import static org.testng.Assert.assertTrue;

import java.util.LinkedHashMap;

import org.gs4tr.qa.testrail.framework.TestRailClient;
import org.gs4tr.qa.utility.BrowserFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import actions.General;
import actions.GlobalProductCockpit_Common_Prop_Cred;
import actions.Hybris_Common_Properties_Cred;
import actions.Hybris_TestRail_Common_Properties;
import actions.Verify;
import actions.globalProductCockpit;
import locators.Hybris_GlobalProductCockpit;

/*********************************************************************************************************************************************
* Author        : Proteek
*
* Test case     : Verify that user should not be able to modify any of the job configurations and job items when he double clicks on listed 
* 				  GlobalLink jobs under GlobalLink perspective

* Test Id       : 2626801
* Summary   	: 
* Precondition  : GlobalLink adaptor should be deployed.
				  CMS Manager should be added to "GlobalLink User Group [globallinkusergroup]" group.
				  GlobalLink Jobs should be present in GlobalLink.
***********************************************************************************************************************************************/
public class Hyb_2626801 {
	
	Boolean assertion = true;
	LinkedHashMap<String, String> dataSet = new LinkedHashMap<String, String>();
	

	@BeforeMethod	
	public void setUp() throws Exception{
		BrowserFactory.SystemEngine().startApplication();
		// Write you Test case description here
				dataSet.put("TL_test_case_description","Hyb_2626801: Verify that user should not be able to modify any of the job configurations and job "
						+ "items when he double clicks on listed GlobalLink jobs under GlobalLink perspective");
				dataSet.put("TL_internal_testCase_ID", "2626801");
	}	
		public void testcase(LinkedHashMap<String, String> dataSet) throws Exception{
			try{
					
				General.action().login();
				globalProductCockpit.action().open_New_Instance_globalProduct_Cockpit(GlobalProductCockpit_Common_Prop_Cred.gpc_cms_URL);
				
				globalProductCockpit.action().globalCMS_Cockpit_login();

				//verify global link menu
				assertion= Verify.action().verifyElementPresent(Hybris_GlobalProductCockpit.exts().selectMenu(GlobalProductCockpit_Common_Prop_Cred.menu_GlobalLink),5);
				if(assertion == false){
					report("f","assertion is failed while verifying Global Link menu");
				}
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().selectMenu(GlobalProductCockpit_Common_Prop_Cred.menu_GlobalLink)).click();
				Thread.sleep(2000);
				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().globalLink_CMS_Search_Icon).click();
				Thread.sleep(2000);
				assertion= Verify.action().verifyElementPresent(Hybris_GlobalProductCockpit.exts().CMSSearch_Fields(GlobalProductCockpit_Common_Prop_Cred.id),5);
				if(assertion == false){
					report("f","assertion failed while verifying field ID");
				}
				assertion= Verify.action().verifyElementPresent(Hybris_GlobalProductCockpit.exts().CMSSearch_Fields(GlobalProductCockpit_Common_Prop_Cred.name),5);
				if(assertion == false){
					report("f","assertion failed while verifying field Name");
				}
				
				String SubName = BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().Get_CMS_Submission_Name).getAttribute("value");
				System.out.println("Submission name from 1st row---->"+SubName);
		
				General.action().doubleClick(Hybris_GlobalProductCockpit.exts().CMS_SubmissionName(SubName));
				
				
				
				}catch(Throwable e){
				report("f", "Execution level error was encountered.\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				}
			}

		@Test
		public void execute() throws Exception{
			
			testcase(dataSet);
			assertion();
			
		}
			
		@AfterMethod
		public void tearDown() throws Exception{
		try {
			BrowserFactory.quitBrowser();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
					
		public void assertion() throws Exception{
		try{
			
			// Here below assertion is blocked to pass the test case please check step 03 manually user should not able to modify source language and due date
			assertion= Verify.action().verifyElementPresent(Hybris_GlobalProductCockpit.exts().CMSSearch_Fields(GlobalProductCockpit_Common_Prop_Cred.timeCreated),5);
			if(assertion == false){
				report("f","assertio failed while verifying field time created");
			}else{
				report("b","Blocked for "+Hybris_Common_Properties_Cred.Hybris_Version_TestRail_comment);
			}	
				}catch (Throwable e) {
					report("f", "Execution level error was encountered in assertion() .\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				
				}
		}
		
		
			public void report(String result, String notes) throws Exception
			{
			TestRailClient.testRailReportByID_production(dataSet.get("TL_internal_testCase_ID"),Hybris_TestRail_Common_Properties.idTestPlan,Hybris_TestRail_Common_Properties.idBuild,result,Hybris_TestRail_Common_Properties.assignedTo,notes);
			if(result == "f")
				assertTrue(false);
			}	

}

