package functionalTesting_GL_CMS_Cockpit;

import static org.testng.Assert.assertTrue;

import java.util.LinkedHashMap;

import org.gs4tr.qa.testrail.framework.TestRailClient;
import org.gs4tr.qa.utility.BrowserFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import actions.General;
import actions.GlobalProductCockpit_Common_Prop_Cred;
import actions.Hybris_Common_Properties_Cred;
import actions.Hybris_TestRail_Common_Properties;
import actions.Verify;
import actions.common;
import actions.globalProductCockpit;
import locators.Hybris_GlobalProductCockpit;

/************************************************************************************************************************************************************
* Author        : Proteek
*
* Test case     : Verify that child navigation nodes exported for translation are imported back successfully
* Test Id       : 2626803
* Summary   	: 
* Precondition  : GlobalLink adaptor should be deployed.
				  GlobalLink Config should be done.
				  User should be logged into globalcms cockpit (https://10.10.222.12:9002/globalcmscockpit/index.zul)
***************************************************************************************************************************************************************/
public class Hyb_2626803 {
	
	Boolean assertion = true;
	LinkedHashMap<String, String> dataSet = new LinkedHashMap<String, String>();
	String SubmissionName = "HYB000008_20210518075644";
	
	@BeforeMethod	
	public void setUp() throws Exception{
		BrowserFactory.SystemEngine().startApplication();
		// Write you Test case description here
				dataSet.put("TL_test_case_description","Hyb_2626803:Verify that child navigation nodes exported for translation are imported back successfully");
				dataSet.put("TL_internal_testCase_ID", "2626803");
	}	
		public void testcase(LinkedHashMap<String, String> dataSet) throws Exception{
			try{
				String originalHandle = BrowserFactory.driver.getWindowHandle();	
				General.action().login();
				globalProductCockpit.action().open_New_Instance_globalProduct_Cockpit(GlobalProductCockpit_Common_Prop_Cred.gpc_cms_URL);
				
				globalProductCockpit.action().globalCMS_Cockpit_login();
				
				
				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().selectMenu("Navigation")).click();
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().selectCMS_SideMenu_Siteroot("Apparel Site UK")).click();
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().selectCMS_SideMenu("Apparel UK Content Catalog / Staged")).click();
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().Select_SiteRoot).click();
				Thread.sleep(1000);	
				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().Open_Select_SiteRoot).click();
				Thread.sleep(1000);			
				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().Select_Open_Apparel_UK_Site).click();
				Thread.sleep(1000);		
				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().Select_SiteRootNode("Categories")).click();
				Thread.sleep(1000);		
				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().Select_Open_Cateogories).click();
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().Select_SiteRootNode("Accessories Menu")).click();
				Thread.sleep(1000);		
								
				SubmissionName =globalProductCockpit.action().create_Job_SiteRoot_ApparelUKSite("Accessories Menu",false, 1, GlobalProductCockpit_Common_Prop_Cred.ProjectName_Hybris, GlobalProductCockpit_Common_Prop_Cred.profileName_Default,
						 false, "", false, false, 1,GlobalProductCockpit_Common_Prop_Cred.menu_GlobalLink);
				Thread.sleep(2000);
				
				common.action().Close_ActiveTab(originalHandle);  
				Thread.sleep(2000);
				common.action().switchTo_CurrentWindow();
				Thread.sleep(1000);
				
				common.action().search_Job(Hybris_Common_Properties_Cred.Title_globalLink,Hybris_Common_Properties_Cred.subtitle_jobs,SubmissionName);
				
				common.action().verify_Job_Status("","", Hybris_Common_Properties_Cred.Job_Status_Queued, SubmissionName);
				
				assertion=common.action().verify_Job_Status(Hybris_Common_Properties_Cred.subtitle_SubmitQuedJobs,"",Hybris_Common_Properties_Cred.Job_Status_inProgress, SubmissionName);
				if(assertion == false){
					report("f"," assertion is failed while verifying  that Status of the GlobalLink job should change to \"IN_PROGRESS\"");
				}
				
				common.action().verify_Job_Status("",Hybris_Common_Properties_Cred.subtitle_FetchTranslations,Hybris_Common_Properties_Cred.Job_Status_Complete, SubmissionName);
				
				}catch(Throwable e){
				report("f", "Execution level error was encountered.\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				}
			}

		@Test
		public void execute() throws Exception{
			
			testcase(dataSet);
			assertion();
			
		}
			
		@AfterMethod
		public void tearDown() throws Exception{
		try {
			BrowserFactory.quitBrowser();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
					
		public void assertion() throws Exception{
		try{
			assertion=common.action().verify_Translated_Content(Hybris_Common_Properties_Cred.subtitle_Job_Items,SubmissionName,Hybris_Common_Properties_Cred.editItem_Page_Title);
			if(assertion == false){
				report("f","assertion is failed while verifying  that All the navigation nodes including children navigation nodes needs to be imported successfully");
			}else{
				report("p","Passed on "+Hybris_Common_Properties_Cred.Hybris_Version_TestRail_comment);
			}	
				}catch (Throwable e) {
					report("f", "Execution level error was encountered in assertion() .\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				
				}
		}
		
		
			public void report(String result, String notes) throws Exception
			{
			TestRailClient.testRailReportByID_production(dataSet.get("TL_internal_testCase_ID"),Hybris_TestRail_Common_Properties.idTestPlan,Hybris_TestRail_Common_Properties.idBuild,result,Hybris_TestRail_Common_Properties.assignedTo,notes);
			if(result == "f")
				assertTrue(false);
			}	

}

