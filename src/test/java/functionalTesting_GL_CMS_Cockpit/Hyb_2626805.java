package functionalTesting_GL_CMS_Cockpit;
import static org.testng.Assert.assertTrue;

import java.util.LinkedHashMap;
import java.util.List;

import org.gs4tr.qa.testrail.framework.TestRailClient;
import org.gs4tr.qa.utility.BrowserFactory;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import actions.Download;
import actions.General;
import actions.GlobalProductCockpit_Common_Prop_Cred;
import actions.Hybris_Common_Properties_Cred;
import actions.Hybris_TestRail_Common_Properties;
import actions.Pmuser_client;
import actions.Verify;
import actions.common;
import actions.globalProductCockpit;
import locators.Hybris_GlobalProductCockpit;
import locators.PD4_main_client_dashboard_Locators;

/*********************************************************************************************************************************************
* Author        : Praveen
*
* Test case     : Verify that adaptor will send child nodes for translation

* Test Id       : 2626805
* Summary   	: 
* Precondition  : GlobalLink adaptor should be deployed.
				  CMS Manager should be added to "GlobalLink User Group [globallinkusergroup]" group.
***********************************************************************************************************************************************/
public class Hyb_2626805 {
	
	Boolean assertion = true;
	LinkedHashMap<String, String> dataSet = new LinkedHashMap<String, String>();
	String SubmissionName = "Hyb_2626805_A10";

	@BeforeMethod	
	public void setUp() throws Exception{
		BrowserFactory.SystemEngine().startApplication();
		// Write you Test case description here
				dataSet.put("TL_test_case_description","Hyb_2626805: Verify that adaptor will send child nodes for translation");
				dataSet.put("TL_internal_testCase_ID", "2626805");
	}	
	
		public void testcase(LinkedHashMap<String, String> dataSet) throws Exception{
			try{
				Download.action().deleteFiles_From_Download();
				Thread.sleep(1000);
				BrowserFactory.SystemEngine();
				String originalHandle = BrowserFactory.driver.getWindowHandle();	
				
				General.action().login();
				globalProductCockpit.action().open_New_Instance_globalProduct_Cockpit(GlobalProductCockpit_Common_Prop_Cred.gpc_cms_URL);
				
				globalProductCockpit.action().globalCMS_Cockpit_login();
				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().selectMenu(GlobalProductCockpit_Common_Prop_Cred.menu_Navigation)).click();
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().selectCMS_SideMenu_Siteroot(GlobalProductCockpit_Common_Prop_Cred.csm_Apparel_Site_UK)).click();	
				Thread.sleep(2000);
				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().selectCMS_SideMenu_Siteroot(GlobalProductCockpit_Common_Prop_Cred.cms_Apparel_UK_Content_Catalog_Staged)).click();
				Thread.sleep(2000);
				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().siteRootNode_Expand_Arrow).click();
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().apparelUKSite_Expand_Plus_Icon).click();
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().categories_Expand_Plus_Icon).click();
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().categories_SubMenu_AllBrands).click();
				Thread.sleep(2000);
				create_Job_From_Product(true, 3, GlobalProductCockpit_Common_Prop_Cred.ProjectName_Hybris, GlobalProductCockpit_Common_Prop_Cred.profileName_Default,
						SubmissionName, true, GlobalProductCockpit_Common_Prop_Cred.German, true, false, 1);
				Thread.sleep(3000);
				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().SiteRoot_selectMenu(GlobalProductCockpit_Common_Prop_Cred.menu_GlobalLink)).click();
				Thread.sleep(3000);
				
				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().globalCMS_search_Job_Input).sendKeys(SubmissionName);
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().globalCMS_search_Job_Icon).click();
				Thread.sleep(3000);
				
				assertion= globalProductCockpit.action().verify_Job_Status(GlobalProductCockpit_Common_Prop_Cred.Job_Status_Queued);
				if(assertion == false){
					report("f","assertion failed while verifying job status is queued");
				}
				globalProductCockpit.action().Close_ActiveTab(originalHandle);
				
				//Switch to backoffice
				Thread.sleep(6000);
				common.action().search_Job(Hybris_Common_Properties_Cred.Title_globalLink, Hybris_Common_Properties_Cred.subtitle_jobs, SubmissionName);
				Thread.sleep(2000);
				common.action().verify_Job_Status(Hybris_Common_Properties_Cred.subtitle_SubmitQuedJobs, "", Hybris_Common_Properties_Cred.Job_Status_inProgress, SubmissionName);
				Thread.sleep(5000);   
				
				//STEP 07 PD
				common.action().open_New_Instance(Hybris_Common_Properties_Cred.PD_URL);
				
				common.action().PDlogIn(Hybris_Common_Properties_Cred.PD_Config_UserName, Hybris_Common_Properties_Cred.PD_Config_Password);
				Thread.sleep(2000);
				Pmuser_client.action().filterAndWaitForStatus(SubmissionName, "Completed", "Completed", 5);
				
				assertion =Verify.action().verifyElementPresent(PD4_main_client_dashboard_Locators.exts().previewMain_expandLevelLanguage("en-us⇒de-de"), 5);
				if(assertion == false){
				   report("f","Assertion failed while verifying the PD should contain a single job in single submission for all target languages");
				}    
				Thread.sleep(500);
				BrowserFactory.SystemEngine().findElement(PD4_main_client_dashboard_Locators.exts().manageTab).click();
				Thread.sleep(500);
				BrowserFactory.SystemEngine().findElement(PD4_main_client_dashboard_Locators.exts().firstDataRowName).click();
				Thread.sleep(500);
				BrowserFactory.SystemEngine().findElement(PD4_main_client_dashboard_Locators.exts().download_SourceFile).click();
				Thread.sleep(10000);
				BrowserFactory.SystemEngine().findElement(PD4_main_client_dashboard_Locators.exts().download_Link).click();
				Thread.sleep(5000);
				common.action().Close_ActiveTab(originalHandle);  
				
				}catch(Throwable e){
				report("f", "Execution level error was encountered.\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				}
			}
		
		public void create_Job_From_Product(boolean singleProduct,int num,String projectName,String profile,String SubmissionName,boolean lang,String TargetLanguageName,
				boolean submit_onlyNew, boolean translate_Product, int items)
				throws Exception {
			
			//Click on Translate Icon
			if(singleProduct) {
			Verify.action().verifyElementPresent(Hybris_GlobalProductCockpit.exts().siteRootNode_ApparelUK_TranslateIcon,5);
			BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().siteRootNode_ApparelUK_TranslateIcon).click();
			Thread.sleep(2000);
			}else {
				List<WebElement> els = BrowserFactory.SystemEngine().findElements(Hybris_GlobalProductCockpit.exts().selectMulti_Products);
	  			Thread.sleep(1000);
	  			Actions act=new Actions(BrowserFactory.driver);
	  			for(int i=0;i<num;i++)
	  			{
	  				act.keyDown(Keys.CONTROL).click(els.get(i)).build().perform();
	  				Thread.sleep(500);
	  			}
	  			Thread.sleep(1000);
	  			BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().translate_Icon).click();
	  			Thread.sleep(1000);
			}
			//Select Project
			globalProductCockpit.action().SelectProject_CreateJob(projectName);
			//Select Profile
			globalProductCockpit.action().select_Profile_CreateJob(profile);
			Thread.sleep(3000);
			//Enter JOB NAME
			BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().SiteRoot_JobName_Input).clear();
			Thread.sleep(2000);
			BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().SiteRoot_JobName_Input).sendKeys(SubmissionName);
			Thread.sleep(2000);
			 Thread.sleep(2000);
			//Select Language
			if(lang) 
				{
					BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().selectTarget_Lang(TargetLanguageName)).click();
					Thread.sleep(2000);
					System.out.println(TargetLanguageName+"--------->Target Language Selected");
				}
			else {
				//Select Multiple Language
				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().selectTarget_Lang(GlobalProductCockpit_Common_Prop_Cred.selectALL)).click();
				Thread.sleep(2000);
			}
			//Select check box for Submit Only new changed content and translate prod feature
			if(submit_onlyNew)
				{
					BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().checkbox_submitonly_newcontent).click();
					Thread.sleep(2000);
					System.out.println("--------->Submit Only new changed content");
				}
			if(translate_Product)
			{
				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().checkbox_translate_prod_feature).click();
				Thread.sleep(2000);
				System.out.println("--------->Translate Product Features");
			}
				
			BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().click_On_Button(GlobalProductCockpit_Common_Prop_Cred.translate_Btn)).click();
			Thread.sleep(3000);
			Verify.action().verifyTextPresent(items+" item(s) queued for translation.", 5);
			System.out.println("Queued Job Values---------->"+items);
			Thread.sleep(2000);
			BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().click_On_Button(GlobalProductCockpit_Common_Prop_Cred.close_Btn)).click();
			Thread.sleep(3000);
		} 

		@Test
		public void execute() throws Exception{
			
			testcase(dataSet);
			assertion();
			
		}
			
		@AfterMethod
		public void tearDown() throws Exception{
		try {
			BrowserFactory.quitBrowser();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
					
		public void assertion() throws Exception{
		try{
			assertion= Download.action().verify_XML_Data("All Brands");
			if(assertion == false){
				report("f","assertion failed while verifying tranlate icon is displayed");
			}else{
				report("p","Passed on "+Hybris_Common_Properties_Cred.Hybris_Version_TestRail_comment);
			}	
				}catch (Throwable e) {
					report("f", "Execution level error was encountered in assertion() .\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				
				}
		}
		
		
			public void report(String result, String notes) throws Exception
			{
			TestRailClient.testRailReportByID_production(dataSet.get("TL_internal_testCase_ID"),Hybris_TestRail_Common_Properties.idTestPlan,Hybris_TestRail_Common_Properties.idBuild,result,Hybris_TestRail_Common_Properties.assignedTo,notes);
			if(result == "f")
				assertTrue(false);
			}	

}