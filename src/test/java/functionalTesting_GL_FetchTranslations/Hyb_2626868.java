package functionalTesting_GL_FetchTranslations;

import static org.testng.Assert.assertTrue;

import java.util.LinkedHashMap;

import org.gs4tr.qa.testrail.framework.TestRailClient;
import org.gs4tr.qa.utility.BrowserFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import actions.General;
import actions.Hybris_Common_Properties_Cred;
import actions.Hybris_TestRail_Common_Properties;
import actions.Pmuser_client;
import actions.Verify;
import actions.common;
import locators.Hybris_dashboard;
import locators.PD4_main_client_dashboard_Locators;

/********************************************************************************************************
* Author        : Praveen
*
* Test case     : Verify that translated content for page is displayed properly under relevant target language
* Test Id       : 2626868
* Summary   	: 
* Precondition  : GlobalLink adaptor should be deployed.
**********************************************************************************************************/
public class Hyb_2626868 {
	
	Boolean assertion = true;
	LinkedHashMap<String, String> dataSet = new LinkedHashMap<String, String>();
	String s1 = "This page will be sent for Translation";
	
	String SubmissionName = "Hyb_2626868";
	
	@BeforeMethod	
	public void setUp() throws Exception{
		BrowserFactory.SystemEngine().startApplication();
		// Write you Test case description here
				dataSet.put("TL_test_case_description","Hyb_2626868:Verify that translated content for page is displayed properly under relevant target language");
				dataSet.put("TL_internal_testCase_ID", "2626868");
	}	
		public void testcase(LinkedHashMap<String, String> dataSet) throws Exception{
			try{
					
				General.action().login();
				Thread.sleep(2000);
				common.action().serach_Multi_Pages(Hybris_Common_Properties_Cred.Title_wcms, Hybris_Common_Properties_Cred.Subtitle_page,true,
				Hybris_Common_Properties_Cred.Page_Attribute_Catalog_Version, Hybris_Common_Properties_Cred.Page_ApparelUK_Content_Cat_Staged, true,
				Hybris_Common_Properties_Cred.Page_Attribute_PageName, Hybris_Common_Properties_Cred.Page_Attribute_PageName_FAQ);
				Thread.sleep(1000);
				Verify.action().verifyElementPresent(Hybris_dashboard.exts().select_search_firstrow,5);
	  			BrowserFactory.SystemEngine().findElement(Hybris_dashboard.exts().select_search_firstrow).click();
	  			Thread.sleep(1000);
				common.action().verify_Globe_Icon_Fields(false,1,true,s1);
				Thread.sleep(1000);
				common.action().create_Job_Multiple_Categories(true,2,Hybris_Common_Properties_Cred.ProjectName_Hybris, SubmissionName,true,
						Hybris_Common_Properties_Cred.TargetLanguage_German,true,false,1);
				common.action().search_Job(Hybris_Common_Properties_Cred.Title_globalLink,Hybris_Common_Properties_Cred.subtitle_jobs,SubmissionName);
				Thread.sleep(1000);
				common.action().verify_Job_Status("","", Hybris_Common_Properties_Cred.Job_Status_Queued, SubmissionName);
				Thread.sleep(2000);
				assertion=common.action().verify_Job_Status(Hybris_Common_Properties_Cred.subtitle_SubmitQuedJobs,"",Hybris_Common_Properties_Cred.Job_Status_inProgress, SubmissionName);
				if(assertion == false){
					report("f"," assertion is failed while verifying  that Status of the GlobalLink job should change to \"IN_PROGRESS\"");
				}
				Thread.sleep(2000);
				assertion=common.action().verify_Job_Status("",Hybris_Common_Properties_Cred.subtitle_FetchTranslations,Hybris_Common_Properties_Cred.Job_Status_Complete, SubmissionName);
				if(assertion == false){
					report("f"," assertion is failed while verifying  that Status of the GlobalLink job should change to \"IN_PROGRESS\"");
				}
					
				}catch(Throwable e){
				report("f", "Execution level error was encountered.\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				}
			
			Thread.sleep(2000);
			}

		@Test
		public void execute() throws Exception{
			
			testcase(dataSet);
			assertion();
			
		}
			
		@AfterMethod
		public void tearDown() throws Exception{
		try {
			BrowserFactory.quitBrowser();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
					
		public void assertion() throws Exception{
		try{
			assertion=common.action().verify_Content_In_Page(true,Hybris_Common_Properties_Cred.Subtitle_page,false);
			if(assertion == false){
				report("f"," assertion is failed while verifying  Translated content");
			}else{
				report("p","Passed on "+Hybris_Common_Properties_Cred.Hybris_Version_TestRail_comment);
			}	
				}catch (Throwable e) {
					report("f", "Execution level error was encountered in assertion() .\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				
				}
		}
		
		
			public void report(String result, String notes) throws Exception
			{
			TestRailClient.testRailReportByID_production(dataSet.get("TL_internal_testCase_ID"),Hybris_TestRail_Common_Properties.idTestPlan,Hybris_TestRail_Common_Properties.idBuild,result,Hybris_TestRail_Common_Properties.assignedTo,notes);
			if(result == "f")
				assertTrue(false);
			}	

}
