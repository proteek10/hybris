package functionalTesting_GL_Product_Cockpit;

import static org.testng.Assert.assertTrue;

import java.util.LinkedHashMap;

import org.gs4tr.qa.testrail.framework.TestRailClient;
import org.gs4tr.qa.utility.BrowserFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import actions.General;
import actions.GlobalProductCockpit_Common_Prop_Cred;
import actions.Hybris_Common_Properties_Cred;
import actions.Hybris_TestRail_Common_Properties;
import actions.Verify;
import actions.globalProductCockpit;
import locators.Hybris_GlobalProductCockpit;

/*****************************************************************************************************************************************************
* Author        : Proteek
*
* Test case     : Verify that when user clicks on “GlobalLink” link in left hand explorer section, all the GlobalLink jobs created earlier are listed
* Test Id       : 2626807
* Summary   	: 
* Precondition  : GlobalLink adaptor should be deployed.
				  Product Manager should be added to "GlobalLink User Group [globallinkusergroup]" group.

******************************************************************************************************************************************************/
public class Hyb_2626806 {
	
	Boolean assertion = true;
	LinkedHashMap<String, String> dataSet = new LinkedHashMap<String, String>();
		
	@BeforeMethod	
	public void setUp() throws Exception{
		BrowserFactory.SystemEngine().startApplication();
		// Write you Test case description here
				dataSet.put("TL_test_case_description","Hyb_2626807:Verify that when user clicks on “GlobalLink” link in left hand explorer section, all the GlobalLink jobs created earlier are listed");
				dataSet.put("TL_internal_testCase_ID", "2626807");
	}	
		public void testcase(LinkedHashMap<String, String> dataSet) throws Exception{
			try{
					
				General.action().login();
				globalProductCockpit.action().open_New_Instance_globalProduct_Cockpit(GlobalProductCockpit_Common_Prop_Cred.gpc_URL);
				
				globalProductCockpit.action().globalProduct_Cockpit_login();
				
				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().selectMenu(GlobalProductCockpit_Common_Prop_Cred.menu_GlobalLink)).click();
				Thread.sleep(1000);
				
				assertion= 	Verify.action().verifyElementPresent(Hybris_GlobalProductCockpit.exts().GlobalLink_activeTab,5);
				if(assertion == false){
					report("f","assertion failed while verifying GlobalLink module is present besides Catalog under Menu label.");
				}
				
				
				}catch(Throwable e){
				report("f", "Execution level error was encountered.\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				}
			}

		@Test
		public void execute() throws Exception{
			
			testcase(dataSet);
			assertion();
			
		}
			
		@AfterMethod
		public void tearDown() throws Exception{
		try {
			BrowserFactory.quitBrowser();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
					
		public void assertion() throws Exception{
		try{
			assertion= Verify.action().verifyElementPresent(Hybris_GlobalProductCockpit.exts().GlobalLink_Submission_Div,5);
			if(assertion == false){
				report("f","assertion failed while verifying All the GlobalLink jobs created by user should be listed.");
			}else{
				report("p","Passed on "+Hybris_Common_Properties_Cred.Hybris_Version_TestRail_comment);
			}	
				}catch (Throwable e) {
					report("f", "Execution level error was encountered in assertion() .\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				
				}
		}
		
		
			public void report(String result, String notes) throws Exception
			{
			TestRailClient.testRailReportByID_production(dataSet.get("TL_internal_testCase_ID"),Hybris_TestRail_Common_Properties.idTestPlan,Hybris_TestRail_Common_Properties.idBuild,result,Hybris_TestRail_Common_Properties.assignedTo,notes);
			if(result == "f")
				assertTrue(false);
			}	

}

