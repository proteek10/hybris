package functionalTesting_GL_Product_Cockpit;

import static org.testng.Assert.assertTrue;

import java.util.LinkedHashMap;

import org.gs4tr.qa.testrail.framework.TestRailClient;
import org.gs4tr.qa.utility.BrowserFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import actions.General;
import actions.GlobalProductCockpit_Common_Prop_Cred;
import actions.Hybris_Common_Properties_Cred;
import actions.Hybris_TestRail_Common_Properties;
import actions.Verify;
import actions.common;
import actions.globalProductCockpit;
import locators.Hybris_GlobalProductCockpit;

/*********************************************************************************************************************************************
* Author        : Praveen
*
* Test case     : Verify that status of GlobalLink jobs changes with the execution of relevant scheduled/manual cronjobs
* Test Id       : 2626817
* Summary   	: 
* Precondition  :User should be logged in backoffice
				  GlobalLink adaptor should be configured
***********************************************************************************************************************************************/
public class Hyb_2626817 {
	
	Boolean assertion = true;
	LinkedHashMap<String, String> dataSet = new LinkedHashMap<String, String>();
	
	String SubmissionName = "Hyb_2626817_Z1";
	
	@BeforeMethod	
	public void setUp() throws Exception{
		BrowserFactory.SystemEngine().startApplication();
		// Write you Test case description here
				dataSet.put("TL_test_case_description","Hyb_2626817: Verify that status of GlobalLink jobs changes with the execution of relevant scheduled/manual cronjobs");
				dataSet.put("TL_internal_testCase_ID", "2626817");
	}	
		public void testcase(LinkedHashMap<String, String> dataSet) throws Exception{
			try{
				BrowserFactory.SystemEngine();
				String originalHandle = BrowserFactory.driver.getWindowHandle();	
				General.action().login();
				globalProductCockpit.action().open_New_Instance_globalProduct_Cockpit(GlobalProductCockpit_Common_Prop_Cred.gpc_URL);
				
				globalProductCockpit.action().globalProduct_Cockpit_login();
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().selectProduct_Type_From_Catalog(GlobalProductCockpit_Common_Prop_Cred.apparel_Prod_Catalog_Staged)).click();
				globalProductCockpit.action().searchProduct(GlobalProductCockpit_Common_Prop_Cred.product_Helmet);
				Thread.sleep(2000);
				globalProductCockpit.action().create_Job_From_Product(true, 1, GlobalProductCockpit_Common_Prop_Cred.ProjectName_Hybris, GlobalProductCockpit_Common_Prop_Cred.profileName_Default,
						SubmissionName, true, GlobalProductCockpit_Common_Prop_Cred.German, false, false, 1);
				Thread.sleep(2000);
				globalProductCockpit.action().search_Created_Job(GlobalProductCockpit_Common_Prop_Cred.menu_GlobalLink, SubmissionName);
				Thread.sleep(1000);
				assertion= globalProductCockpit.action().verify_Job_Status(GlobalProductCockpit_Common_Prop_Cred.Job_Status_Queued);
				if(assertion == false){
					report("f","assertion failed while verifying job status is queued");
				}
				globalProductCockpit.action().Close_ActiveTab(originalHandle);
				
				//Switch to backoffice
				Thread.sleep(6000);
				common.action().search_Job(Hybris_Common_Properties_Cred.Title_globalLink, Hybris_Common_Properties_Cred.subtitle_jobs, SubmissionName);
				Thread.sleep(2000);
				common.action().verify_Job_Status(Hybris_Common_Properties_Cred.subtitle_SubmitQuedJobs, "", Hybris_Common_Properties_Cred.Job_Status_inProgress, SubmissionName);
				Thread.sleep(2000);
				common.action().verify_Job_Status("", Hybris_Common_Properties_Cred.subtitle_FetchTranslations, Hybris_Common_Properties_Cred.Job_Status_Complete, SubmissionName);
				Thread.sleep(5000);
				
				//Switch to product cockpit
				globalProductCockpit.action().open_New_Instance_globalProduct_Cockpit(GlobalProductCockpit_Common_Prop_Cred.gpc_URL);
			//	globalProductCockpit.action().search_Created_Job(GlobalProductCockpit_Common_Prop_Cred.menu_GlobalLink, SubmissionName);
				Thread.sleep(2000);
				
				}catch(Throwable e){
				report("f", "Execution level error was encountered.\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				}
			}

		@Test
		public void execute() throws Exception{
			
			testcase(dataSet);
			assertion();
			
		}
			
		@AfterMethod
		public void tearDown() throws Exception{
		try {
			BrowserFactory.quitBrowser();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
					
		public void assertion() throws Exception{
		try{
			assertion= globalProductCockpit.action().verify_Job_Status(GlobalProductCockpit_Common_Prop_Cred.Job_Status_Complete);
			if(assertion == false){
				report("f","assertion failed while verifying job status is complete");
			}else{
				report("p","Passed on "+Hybris_Common_Properties_Cred.Hybris_Version_TestRail_comment);
			}	
				}catch (Throwable e) {
					report("f", "Execution level error was encountered in assertion() .\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				
				}
		}
		
		
			public void report(String result, String notes) throws Exception
			{
			TestRailClient.testRailReportByID_production(dataSet.get("TL_internal_testCase_ID"),Hybris_TestRail_Common_Properties.idTestPlan,Hybris_TestRail_Common_Properties.idBuild,result,Hybris_TestRail_Common_Properties.assignedTo,notes);
			if(result == "f")
				assertTrue(false);
			}	

}

