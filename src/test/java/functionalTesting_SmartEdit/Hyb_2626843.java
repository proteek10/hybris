package functionalTesting_SmartEdit;

import static org.testng.Assert.assertTrue;

import java.util.LinkedHashMap;

import org.gs4tr.qa.testrail.framework.TestRailClient;
import org.gs4tr.qa.utility.BrowserFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import actions.General;
import actions.Hybris_Common_Properties_Cred;
import actions.Hybris_TestRail_Common_Properties;
import actions.SE_Common_Prop_Cred;
import actions.Verify;
import actions.common;
import actions.smartedit;
import locators.Hybris_dashboard;
import locators.Hybris_smartedit;

/***************************************************************************************************************
* Author        : Praveen
*
* Test case     : Verify Translate option for Page in basic & advance edit mode for smartedit module
* Test Id       : 2626843
* Summary   	:
* Precondition  : User should be logged in backoffice
				  GlobalLink adaptor should be configured
****************************************************************************************************************/
public class Hyb_2626843 {
	
	Boolean assertion = true;
	LinkedHashMap<String, String> dataSet = new LinkedHashMap<String, String>();
	
	@BeforeMethod	
	public void setUp() throws Exception{
		BrowserFactory.SystemEngine().startApplication();
		// Write you Test case description here
				dataSet.put("TL_test_case_description","Hyb_2626843: Verify Translate option for Page in basic & advance edit mode for smartedit module");
				dataSet.put("TL_internal_testCase_ID", "2626843");
	}	
		public void testcase(LinkedHashMap<String, String> dataSet) throws Exception{
			try{
					
				General.action().login();
				smartedit.action().open_New_Instance_SmartEdit(SE_Common_Prop_Cred.SmartEdit_URL);
				Thread.sleep(6000);
				smartedit.action().SmartEdit_login();

				smartedit.action().select_yourSite(SE_Common_Prop_Cred.Apparel_Site_UK);
				
				//Select Catalog
				BrowserFactory.SystemEngine().findElement(Hybris_smartedit.exts().select_apparel_uk_content_Catalog(SE_Common_Prop_Cred.catalog_staged, SE_Common_Prop_Cred.catalog_page_pages)).click();
				
				//Select Page
				BrowserFactory.SystemEngine().findElement(Hybris_smartedit.exts().select_staged_page(SE_Common_Prop_Cred.catalog_page_addedit_Address_page)).click();
				Thread.sleep(5000);
				
				//Select Basic Edit if not selected
				smartedit.action().select_Basic_EditMode(SE_Common_Prop_Cred.basic_Edit_Mode);
				
				//Verify Translate button enabled 
				assertion= BrowserFactory.SystemEngine().findElement(Hybris_smartedit.exts().translate_Icon).isEnabled();
				if(assertion == false){
					report("f","assertion is failed while verifying Translate button is enabled in Basic Edit");
				}
				System.out.println("Translate button is Enabled in Basic Edit");
				Thread.sleep(2000);
				
				BrowserFactory.SystemEngine().findElement(Hybris_smartedit.exts().down_Arrow_Icon).click();
				Thread.sleep(2000);
				BrowserFactory.SystemEngine().findElement(Hybris_smartedit.exts().selectEditMode(SE_Common_Prop_Cred.Advanced_Edit_Mode)).click();
				Thread.sleep(2000);
				
				}catch(Throwable e){
				report("f", "Execution level error was encountered.\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				}
			}

		@Test
		public void execute() throws Exception{
			
			testcase(dataSet);
			assertion();
			
		}
			
		@AfterMethod
		public void tearDown() throws Exception{
		try {
			BrowserFactory.quitBrowser();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
					
		public void assertion() throws Exception{
		try{
			assertion= BrowserFactory.SystemEngine().findElement(Hybris_smartedit.exts().translate_Icon).isEnabled();
			if(assertion == false){
				report("f","assertion is failed while verifying Translate button is enabled in Advanced Edit");
			}else{
				report("p","Passed on "+Hybris_Common_Properties_Cred.Hybris_Version_TestRail_comment);
			}	
				}catch (Throwable e) {
					report("f", "Execution level error was encountered in assertion() .\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				
				}
		}
		
		
			public void report(String result, String notes) throws Exception
			{
			TestRailClient.testRailReportByID_production(dataSet.get("TL_internal_testCase_ID"),Hybris_TestRail_Common_Properties.idTestPlan,Hybris_TestRail_Common_Properties.idBuild,result,Hybris_TestRail_Common_Properties.assignedTo,notes);
			if(result == "f")
				assertTrue(false);
			}	

}
