package sanityTesting_GlobalCMS_Cockpit;

import static org.testng.Assert.assertTrue;

import java.util.LinkedHashMap;

import org.gs4tr.qa.testrail.framework.TestRailClient;
import org.gs4tr.qa.utility.BrowserFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import actions.General;
import actions.GlobalProductCockpit_Common_Prop_Cred;
import actions.Hybris_Common_Properties_Cred;
import actions.Hybris_TestRail_Common_Properties;
import actions.Verify;
import actions.globalProductCockpit;

/*********************************************************************************************************************************************
* Author        : Proteek
*
* Test case     : Verify that from GlobalLink CMS cockpit, WCMS page view user is able to create GlobalLink job configuration for multiple pages
* Test Id       : 2626618
* Summary   	: 
* Precondition  : GlobalLink adaptor should be deployed.
				  GlobalLink Config should be done.
				  User should be logged into globalcms cockpit (https://10.10.222.12:9002/globalcmscockpit/index.zul)
***********************************************************************************************************************************************/
public class Hyb_2626618 {
	
	Boolean assertion = true;
	LinkedHashMap<String, String> dataSet = new LinkedHashMap<String, String>();
	
	String SubmissionName = "Hyb_2626618_A1";
	
	@BeforeMethod	
	public void setUp() throws Exception{
		BrowserFactory.SystemEngine().startApplication();
		// Write you Test case description here
				dataSet.put("TL_test_case_description","Hyb_2626618:Verify that from GlobalLink CMS cockpit, WCMS page view user is able to create GlobalLink job configuration for multiple pages");
				dataSet.put("TL_internal_testCase_ID", "2626618");
	}	
		public void testcase(LinkedHashMap<String, String> dataSet) throws Exception{
			try{
					
				General.action().login();
				globalProductCockpit.action().open_New_Instance_globalProduct_Cockpit(GlobalProductCockpit_Common_Prop_Cred.gpc_cms_URL);
				
				globalProductCockpit.action().globalCMS_Cockpit_login();
				
				globalProductCockpit.action().searchCMSProduct(GlobalProductCockpit_Common_Prop_Cred.csm_menu_WCMS,GlobalProductCockpit_Common_Prop_Cred.csm_Electronic_Site,
															   GlobalProductCockpit_Common_Prop_Cred.cms_menu_Catalog_Staged, GlobalProductCockpit_Common_Prop_Cred.product_cms_Terms_Conditions);
				
				globalProductCockpit.action().create_Job_From_Product(false, 10, GlobalProductCockpit_Common_Prop_Cred.ProjectName_Hybris, GlobalProductCockpit_Common_Prop_Cred.profileName_Default,
						SubmissionName, false, "", false, false, 9);
				Thread.sleep(2000);
				globalProductCockpit.action().search_Created_Job(GlobalProductCockpit_Common_Prop_Cred.menu_GlobalLink, SubmissionName);
				
				}catch(Throwable e){
				report("f", "Execution level error was encountered.\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				}
			}

		@Test
		public void execute() throws Exception{
			
			testcase(dataSet);
			assertion();
			
		}
			
		@AfterMethod
		public void tearDown() throws Exception{
		try {
			BrowserFactory.quitBrowser();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
					
		public void assertion() throws Exception{
		try{
			assertion= globalProductCockpit.action().verify_Job_Status(GlobalProductCockpit_Common_Prop_Cred.Job_Status_Queued);
			if(assertion == false){
				report("f","assertio failed while verifying Created GlobalLink job should be listed on this page with status as \"QUEUED\" automatically without having user to click on search.");
			}else{
				report("p","Passed on "+Hybris_Common_Properties_Cred.Hybris_Version_TestRail_comment);
			}	
				}catch (Throwable e) {
					report("f", "Execution level error was encountered in assertion() .\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				
				}
		}
		
		
			public void report(String result, String notes) throws Exception
			{
			TestRailClient.testRailReportByID_production(dataSet.get("TL_internal_testCase_ID"),Hybris_TestRail_Common_Properties.idTestPlan,Hybris_TestRail_Common_Properties.idBuild,result,Hybris_TestRail_Common_Properties.assignedTo,notes);
			if(result == "f")
				assertTrue(false);
			}	

}

