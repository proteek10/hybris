package sanityTesting_GlobalCMS_Cockpit;

import static org.testng.Assert.assertTrue;

import java.util.LinkedHashMap;

import org.gs4tr.qa.testrail.framework.TestRailClient;
import org.gs4tr.qa.utility.BrowserFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import actions.General;
import actions.GlobalProductCockpit_Common_Prop_Cred;
import actions.Hybris_Common_Properties_Cred;
import actions.Hybris_TestRail_Common_Properties;
import actions.Verify;
import actions.common;
import actions.globalProductCockpit;
import locators.Hybris_GlobalProductCockpit;
import locators.Hybris_dashboard;

/*********************************************************************************************************************************************
* Author        : Praveen
*
* Test case     : Verify that from GlobalLink CMS cockpit, on successful execution of "Fetch translations" cron job, status of jobs in GlobalLink CMS cockpit also changes to in complete
* Test Id       : 2626621
* Summary   	: 
* Precondition  : GlobalLink adaptor should be deployed.
				  GlobalLink Config should be done.
				  User should be logged into globalcms cockpit (https://10.10.222.12:9002/globalcmscockpit/index.zul)
***********************************************************************************************************************************************/
public class Hyb_2626621 {
	
	Boolean assertion = true;
	LinkedHashMap<String, String> dataSet = new LinkedHashMap<String, String>();
	
	String SubmissionName = "";
	
	@BeforeMethod	
	public void setUp() throws Exception{
		BrowserFactory.SystemEngine().startApplication();
		// Write you Test case description here
				dataSet.put("TL_test_case_description","Hyb_2626621: Verify that from GlobalLink CMS cockpit, on successful execution of \"Fetch translations\" cron job, status of jobs in GlobalLink CMS cockpit also changes to in complete");
				dataSet.put("TL_internal_testCase_ID", "2626621");
	}	
		public void testcase(LinkedHashMap<String, String> dataSet) throws Exception{
			try{
				String originalHandle = BrowserFactory.driver.getWindowHandle();	
				General.action().login();
				globalProductCockpit.action().open_New_Instance_globalProduct_Cockpit(GlobalProductCockpit_Common_Prop_Cred.gpc_cms_URL);
				
				globalProductCockpit.action().globalCMS_Cockpit_login();
				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().selectCMS_SideMenu(GlobalProductCockpit_Common_Prop_Cred.csm_Electronic_Site)).click();	
				Thread.sleep(2000);
				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().selectCMS_SideMenu(GlobalProductCockpit_Common_Prop_Cred.cms_menu_Catalog_Staged)).click();
				Thread.sleep(2000);
				SubmissionName=globalProductCockpit.action().create_Job_MultipleProd_From_Product(false, 3, GlobalProductCockpit_Common_Prop_Cred.ProjectName_Hybris, GlobalProductCockpit_Common_Prop_Cred.profileName_Default,
						true, GlobalProductCockpit_Common_Prop_Cred.German, false, false, 3, Hybris_Common_Properties_Cred.Title_globalLink);
				
				Thread.sleep(3000);
				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().search_Job_Input).sendKeys(SubmissionName);
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().search_Job_Icon).click();
				Thread.sleep(3000);
		
				globalProductCockpit.action().Close_ActiveTab(originalHandle);
				
				//Switch to backoffice
				Thread.sleep(6000);
				common.action().search_Job(Hybris_Common_Properties_Cred.Title_globalLink, Hybris_Common_Properties_Cred.subtitle_jobs, SubmissionName);
				Thread.sleep(2000);
				common.action().verify_Job_Status(Hybris_Common_Properties_Cred.subtitle_SubmitQuedJobs, "", Hybris_Common_Properties_Cred.Job_Status_inProgress, SubmissionName);
				Thread.sleep(2000);
				common.action().verify_Job_Status("", Hybris_Common_Properties_Cred.subtitle_FetchTranslations, Hybris_Common_Properties_Cred.Job_Status_Complete, SubmissionName);
				Thread.sleep(2000);
				
				//Switch to product cockpit
				globalProductCockpit.action().open_New_Instance_globalProduct_Cockpit(GlobalProductCockpit_Common_Prop_Cred.gpc_cms_URL);
				
				}catch(Throwable e){
				report("f", "Execution level error was encountered.\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				}
			}

		@Test
		public void execute() throws Exception{
			
			testcase(dataSet);
			assertion();
			
		}
			
		@AfterMethod
		public void tearDown() throws Exception{
		try {
			BrowserFactory.quitBrowser();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
					
		public void assertion() throws Exception{
		try{
			assertion= globalProductCockpit.action().verify_Job_Status(GlobalProductCockpit_Common_Prop_Cred.Job_Status_Complete);
			if(assertion == false){
				report("f","assertion failed while verifying job status changes to Complete.");
			}else{
				report("p","Passed on "+Hybris_Common_Properties_Cred.Hybris_Version_TestRail_comment);
			}	
				}catch (Throwable e) {
					report("f", "Execution level error was encountered in assertion() .\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				
				}
		}
		
		
			public void report(String result, String notes) throws Exception
			{
			TestRailClient.testRailReportByID_production(dataSet.get("TL_internal_testCase_ID"),Hybris_TestRail_Common_Properties.idTestPlan,Hybris_TestRail_Common_Properties.idBuild,result,Hybris_TestRail_Common_Properties.assignedTo,notes);
			if(result == "f")
				assertTrue(false);
			}	

}

