package sanityTesting_GlobalProduct_Cockpit;

import static org.testng.Assert.assertTrue;

import java.util.LinkedHashMap;

import org.gs4tr.qa.testrail.framework.TestRailClient;
import org.gs4tr.qa.utility.BrowserFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import actions.General;
import actions.GlobalProductCockpit_Common_Prop_Cred;
import actions.Hybris_Common_Properties_Cred;
import actions.Hybris_TestRail_Common_Properties;
import actions.SE_Common_Prop_Cred;
import actions.Verify;
import actions.globalProductCockpit;
import actions.smartedit;
import locators.Hybris_GlobalProductCockpit;
import locators.Hybris_smartedit;

/*********************************************************************************************************************************************
* Author        : Praveen
*
* Test case     : Verify that GlobalLink module is present in Explorer section of Global Product Cockpit
* Test Id       : 2626609
* Summary   	: 
* Precondition  :User should be logged in backoffice
				  GlobalLink adaptor should be configured
***********************************************************************************************************************************************/
public class Hyb_2626609 {
	
	Boolean assertion = true;
	LinkedHashMap<String, String> dataSet = new LinkedHashMap<String, String>();
	
//	String SubmissionName = "Hyb_2626609";
	
	@BeforeMethod	
	public void setUp() throws Exception{
		BrowserFactory.SystemEngine().startApplication();
		// Write you Test case description here
				dataSet.put("TL_test_case_description","Hyb_2626609: Verify that Translation wizard opens up on clicking upon translation icon from page in smartedit mode");
				dataSet.put("TL_internal_testCase_ID", "2626609");
	}	
		public void testcase(LinkedHashMap<String, String> dataSet) throws Exception{
			try{
					
				General.action().login();
				globalProductCockpit.action().open_New_Instance_globalProduct_Cockpit(GlobalProductCockpit_Common_Prop_Cred.gpc_URL);
				
				globalProductCockpit.action().globalProduct_Cockpit_login();

				//verify global link menu
				assertion= Verify.action().verifyElementPresent(Hybris_GlobalProductCockpit.exts().selectMenu(GlobalProductCockpit_Common_Prop_Cred.menu_GlobalLink),5);
				if(assertion == false){
					report("f","assertion is failed while verifying Global Link menu");
				}
				Thread.sleep(1000);
				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().selectMenu(GlobalProductCockpit_Common_Prop_Cred.menu_GlobalLink)).click();
				Thread.sleep(2000);
				BrowserFactory.SystemEngine().findElement(Hybris_GlobalProductCockpit.exts().globalLink_AdvancedSearch_Icon).click();
				Thread.sleep(2000);
				assertion= Verify.action().verifyElementPresent(Hybris_GlobalProductCockpit.exts().advancedSearch_Fields(GlobalProductCockpit_Common_Prop_Cred.id),5);
				if(assertion == false){
					report("f","assertion failed while verifying field ID");
				}
				assertion= Verify.action().verifyElementPresent(Hybris_GlobalProductCockpit.exts().advancedSearch_Fields(GlobalProductCockpit_Common_Prop_Cred.name),5);
				if(assertion == false){
					report("f","assertion failed while verifying field Name");
				}
				
				}catch(Throwable e){
				report("f", "Execution level error was encountered.\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				}
			}

		@Test
		public void execute() throws Exception{
			
			testcase(dataSet);
			assertion();
			
		}
			
		@AfterMethod
		public void tearDown() throws Exception{
		try {
			BrowserFactory.quitBrowser();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
					
		public void assertion() throws Exception{
		try{
			assertion= Verify.action().verifyElementPresent(Hybris_GlobalProductCockpit.exts().advancedSearch_Fields(GlobalProductCockpit_Common_Prop_Cred.timeCreated),5);
			if(assertion == false){
				report("f","assertio failed while verifying field time created");
			}else{
				report("p","Passed on "+Hybris_Common_Properties_Cred.Hybris_Version_TestRail_comment);
			}	
				}catch (Throwable e) {
					report("f", "Execution level error was encountered in assertion() .\n\nError log:\n\n"+ Verify.action().getErrorBuffer(e));
				
				}
		}
		
		
			public void report(String result, String notes) throws Exception
			{
			TestRailClient.testRailReportByID_production(dataSet.get("TL_internal_testCase_ID"),Hybris_TestRail_Common_Properties.idTestPlan,Hybris_TestRail_Common_Properties.idBuild,result,Hybris_TestRail_Common_Properties.assignedTo,notes);
			if(result == "f")
				assertTrue(false);
			}	

}

